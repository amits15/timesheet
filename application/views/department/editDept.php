<?php echo $this->load->view("common/header", $title); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Department
        </h1>
        <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Master</li>
            <li><a href="#"></i> Department</a></li>          
            <li><a href="#"></i> Edit</a></li>          
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                        <a href="javascript:history.back()" class="btn btn-link pull-right"><i class="fa fa-arrow-left" ></i> Back</a>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="<?php echo base_url("department/edit"); ?>" method="POST" name="frmDept" id="frmDept">
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="inputEmail3">Department Name</label>
                                <div class="col-sm-4">
                                    <input type="hidden" id="txtDeptId" name="txtDeptId" class="form-control" value="<?php echo $dept['id'] ?>" >
                                    <input type="text" placeholder="Department Name" id="txtDeptName" name="txtDeptName" class="form-control" value="<?php echo $dept['dept_name'] ?>" >
                                    <?php echo form_error('txtDeptName', '<div class="form-error">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="inputPassword3">Status</label>
                                <div class="col-sm-3">
                                    <select class="form-control col-sm-2" name="optDeptStatus" id="optDeptStatus">
                                        <option value=1 <?php if($dept['status'] == 1) echo "Selected"; ?> >Active</option>
                                        <option value="0" <?php if($dept['status'] == 0) echo "Selected"; ?> >Inactive</option>
                                    </select>
                                    <?php echo form_error('optDeptStatus', '<div class="form-error">', '</div>'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div><!-- /.box -->

            </div><!--/.col (left) -->

        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php echo $this->load->view("common/footer"); ?>
<script src="<?php echo base_url(); ?>assets/plugins/jValidate/formValidations.js"></script>