<?php echo $this->load->view("common/header", $title); ?>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/minimal/_all.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Reset Password
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Reset Password</li>  
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#settings" data-toggle="tab">Reset Password</a></li>
                    </ul>
                    <div class="tab-content">
                        <span class="text-aqua">
                            <?php
                            if ($this->session->flashdata("pwd_msg")) {
                                echo $this->session->flashdata("pwd_msg");
                            }
                            ?>
                        </span>
                        <div class="active tab-pane" id="settings">
                            <form class="form-horizontal" name="frmEmpPassword" id="frmEmpPassword" method="POSt" action="<?php echo base_url("employee/changeEmpPassword"); ?>">
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Employee</label>
                                    <div class="col-sm-4">
                                        <select class="form-control col-sm-2" id="optEmp" name="optEmp">
                                            <option value="">Select Employee</option>
                                            <?php foreach ($employees as $key => $emp) { ?>
                                                <option value="<?php echo $emp['id'] ?>" ><?php echo $emp['first_name']. " ". $emp['last_name'] ?></option>
                                            <?php } ?>
                                        </select>
                                        <?php echo form_error('optEmp', '<div class="form-error">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">New Password</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="txtEmpPwd" id="txtEmpPwd" class="form-control" value="">
                                        <?php echo form_error('txtEmpPwd', '<div class="form-error">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Confirm Password</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="txtEmpConfirmPwd" id="txtEmpConfirmPwd" class="form-control" value="">
                                        <?php echo form_error('txtEmpConfirmPwd', '<div class="form-error">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" id="PwdSub" name="PwdSub" class="btn btn-danger">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div><!-- /.tab-pane -->
                    </div><!-- /.tab-content -->
                </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php echo $this->load->view("common/footer"); ?>
<script>
    var baseURL = "<?php echo base_url() ?>";
</script>
<script src="<?php echo base_url(); ?>assets/plugins/jValidate/formValidations.js"></script>
<script>
$("#optEmp").chosen();
</script>