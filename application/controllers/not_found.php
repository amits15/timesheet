<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Not_Found extends CI_Controller {

	public function __construct() 
    {
        parent::__construct(); 
        $this->load->library('session');
    } 


    

    public function index() 
    { 
    	//creating header title for header view page
	    $data = array(
	        "title" => "404 Not Found"
	    );

        $this->load->view('404', $data);//loading in my template 
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */