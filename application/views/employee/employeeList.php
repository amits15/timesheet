<?php echo $this->load->view("common/header", $title); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/datatables/dataTables.bootstrap.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Employee Master
        </h1>
        <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Master</li>      
            <li><a href="#"><i class="fa fa-dashboard"></i> Employee</a></li>            
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <form action="<?php echo base_url("employee/delete") ?>" method="POST" id="frmEmpList" >
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Employee List</h3>
                        </div><!-- /.box-header -->
                        &nbsp;&nbsp;&nbsp;<a class="btn btn-primary" href="<?php echo base_url("employee/add") ?>"><i class="fa fa-plus"></i> Add</a>
                        &nbsp;&nbsp;&nbsp;<a class="btn btn-danger" data-toggle="modal" id="multiDelete" href="javascript:void(0)"><i class="fa fa-trash"></i> Delete</a>
                        <?php if($this->session->userdata("employee_type") == "super_admin" ){ ?>
                            &nbsp;&nbsp;&nbsp;<a class="btn btn-warning" id="empPwd" href="<?php echo base_url("employee/changeEmpPassword"); ?>"><i class="fa fa-key"></i> Reset Password</a>
                            <?php } ?>
                        <a class="btn btn-success pull-right" data-toggle="modal" style="margin-right: 10px;" href="<?php echo base_url("employee/exportEmployees") ?>"><i class="fa fa-file-excel-o"></i> Export to Excel</a>
                        <div class="box-body">
                            <table id="code_tbl" class="table table-bordered table-striped dt-responsive nowrap">
                                <thead>
                                    <tr>
                                        <th style="width: 1%" ><input type="checkbox" class="grp_check" name="check_all" /></th>
                                        <!--<th >Sr No.</th>-->
                                        <th>Emp Code</th>
                                        <th>Emp Name</th>
                                        <th>Email ID</th>
                                        <th>Department</th>
                                        <th>Designation</th>
                                        <!-- <th>Phone</th> -->
                                        <th>Location</th>
                                        <th>Emp type</th>
                                        <th>Status</th>
                                        <th style="width: 10%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    /*if ($emp_results) {
                                        foreach ($emp_results as $emp) {
                                            ?>
                                            <tr>
                                                <td><input type="checkbox" class="multi_check" name="check_emps[]" value="<?php echo $emp['id'] ?>" /></td>
                                                
                                                <td><?php echo $emp['emp_code'] ?></td>
                                                <td><?php echo $emp['first_name'] . " " . $emp["last_name"] ?></td>
                                                <td><?php echo $emp['email_id'] ?></td>
                                                <td><?php echo $emp['dept_name'] ?></td>
                                                <td><?php echo $emp['desig_name'] ?></td>
                                                <td><?php echo $emp['phone'] ?></td>
                                                <td><?php echo $emp['location_name'] ?></td>
                                                <td><?php echo $emp['employee_type'] ?></td>
                                                <td>
                                                    <?php
                                                    if ($emp['status'] == 1) {
                                                        echo "Active";
                                                    } else {
                                                        echo "Inactive";
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <a class="" href="<?php echo base_url("employee/edit/$emp[id]") ?>"><i class="fa fa-edit fa-lg"></i></a>&nbsp;&nbsp;
                                                    <a class="" href="#" data-href="<?php echo base_url("employee/delete/$emp[id]") ?>" data-toggle="modal" data-target="#deleteModal" ><i class="fa fa-trash fa-lg"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }*/
                                    ?>
                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->

                    <!-- Modal -->
                    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Delete Employee</h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure you want to delete Employee?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                    <a class="btn btn-danger">Yes</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="multiDeleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Delete Employee</h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure you want to delete selected Employee(s)?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                    <button type="submit" class="btn btn-danger">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php echo $this->load->view("common/footer"); ?>
<!-- DataTables -->
<script src="<?php echo base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>
    $(function () {

        $('#code_tbl').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            "aoColumns": [
                { "bSortable": false, "searchable": false },
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                { "bSortable": false, "searchable": false },
                ],
            "serverSide": true,
            "processing": true,
            "language": {
                "processing": "Loading data..." //add a loading image,simply putting <img src="loader.gif" /> tag.
            },
            "ajax": {
                url: '<?php echo base_url(); ?>employee/datatable',
                type: 'POST'
            }
        });
    });

</script>