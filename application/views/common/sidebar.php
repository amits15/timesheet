<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <?php if ($this->session->userdata("profile_pic")) { ?>
                    <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url() ?>uploads/profile/<?php echo $this->session->userdata("profile_pic") ?>" alt="User profile picture">
                <?php } else { ?>
                    <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url() ?>assets/dist/img/blank_profile.png" alt="User profile picture">
                <?php } ?>
            </div>
            <div class="pull-left info">
                <p><span class="text-capitalize" ><?php echo $this->session->userdata("first_name"); ?></span> <span class="text-capitalize" ><?php echo $this->session->userdata("last_name"); ?></span></p>
                <a href="#" class="text-capitalize"><i class="fa fa-laptop"></i> <?php echo $this->session->userdata("designation"); ?></a>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="active"><a href="<?php echo base_url("dashboard"); ?>"><i class="fa fa-calendar"></i> <span>Dashboard</span></a></li>
            <!-- <li><a href="<?php echo base_url("holidays/showHolidays"); ?>"><i class="fa fa-list"></i> Holidays</a></li> -->
            <!-- <li><a href="<?php echo base_url("leave"); ?>"><i class="fa fa-calendar"></i> <span>Leaves</span></a></li> -->
            <?php if($this->session->userdata("employee_type") != "employee" ){ ?>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-book"></i> <span>Master</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url("client"); ?>"><i class="fa fa-circle-o"></i> Client Master</a></li>
                    <li><a href="<?php echo base_url("code"); ?>"><i class="fa fa-circle-o"></i> Code Master</a></li>
                    <li><a href="<?php echo base_url("employee"); ?>"><i class="fa fa-circle-o"></i> Employee Master</a></li>
                    <li><a href="<?php echo base_url("department"); ?>"><i class="fa fa-circle-o"></i> Department Master</a></li>
                    <li><a href="<?php echo base_url("designation"); ?>"><i class="fa fa-circle-o"></i> Designation Master</a></li>
                    <li><a href="<?php echo base_url("holidays"); ?>"><i class="fa fa-circle-o"></i> Holidays Master</a></li>
                    <li><a href="<?php echo base_url("location"); ?>"><i class="fa fa-circle-o"></i> Location Master</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list"></i> <span>Reports</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url("reports"); ?>"><i class="fa fa-circle-o"></i>Report with Original Data</a></li>
                    <li><a href="<?php echo base_url("reports/cappedReport"); ?>"><i class="fa fa-circle-o"></i> Report with Capped Data</a></li>
                    <li><a href="<?php echo base_url("reports/withoutCorpReport"); ?>"><i class="fa fa-circle-o"></i> Report without Corp & Bench</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list"></i> <span>Profitability</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url("profitability/profitEstimation"); ?>"><i class="fa fa-circle-o"></i>Project Estimation</a></li>
                    <!-- <li><a href="<?php echo base_url("profitability/actualProfit"); ?>"><i class="fa fa-circle-o"></i> Report with Capped Data</a></li> -->
                    <!-- <li><a href="<?php //echo base_url("reports/withoutCorpReport"); ?>"><i class="fa fa-circle-o"></i> Report without Corp & Bench</a></li> -->
                </ul>
            </li>
            <?php } ?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>