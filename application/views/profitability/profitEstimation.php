<?php echo $this->load->view("common/header", $title); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Profit Estimation
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="">Profitability</li>
            <li class="active">Profit Estimation</li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
             <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                        <a href="javascript:history.back()" class="btn btn-link pull-right"><i class="fa fa-arrow-left" ></i> Back</a>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="<?php echo base_url("profitability/checkEstimation"); ?>" method="POST" id="frmProfitEstimation">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Client <span class="text-red">*</span></label>
                                        <div class="col-sm-6">
                                            <select class="form-control col-sm-2" id="optProfitEstClient" name="optProfitEstClient">
                                                <option value="">Select Client</option>
                                                <?php foreach ($clients as $key => $client) { ?>
                                                    <option value="<?php echo $client['id'] ?>" ><?php echo $client['client_name'] ?></option>
                                                <?php } ?>
                                            </select>
                                            <?php echo form_error('optProfitEstClient', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Code <span class="text-red">*</span> <img id="loader" class="pull-right" style="display:none;" src="<?php echo base_url("assets/dist/img/spin.svg") ?>" /> </span></label>
                                        <div class="col-sm-6">
                                            <select class="form-control col-sm-2" id="optProfitEstCode" name="optProfitEstCode">
                                                <option value="">Select Code</option>
                                            </select>
                                            <?php echo form_error('optProfitEstCode', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Sales <span class="text-red">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" name="txtProfitSalesCost" id="txtProfitSalesCost" class="form-control">
                                            <?php echo form_error('txtProfitSalesCost', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Direct Expenses <span class="text-red">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" name="txtProfitDirectCost" id="txtProfitDirectCost" class="form-control">
                                            <a data-toggle="modal" data-target="#directCostModal" id="addDirectCost" href="javascript:void(0)" >Add Costs</a>
                                            <?php echo form_error('txtProfitDirectCost', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Employee Allocation <span class="text-red">*</span></label>
                                        <div class="col-sm-8">
                                            <table class="table table-bordered">
                                                <tr>
                                                    <th>Department</th>
                                                    <th>Designation</th>
                                                    <th>No. Of Emp</th>
                                                    <th>No. of Hrs</th>
                                                </tr>
                                                <tr id="row_1" class="tblRow">
                                                    <td> 
                                                        <select class="form-control col-sm-2 empDept" id="optDept" name="optDept[]">
                                                            <option value="">Select Department</option>
                                                            <?php if(isset($departments)){
                                                            foreach ($departments as $key => $dept) { ?>
                                                                <option value="<?php echo $dept['id'] ?>" ><?php echo $dept['dept_name'] ?></option>
                                                            <?php   }
                                                            } ?>  
                                                        </select>
                                                    </td>
                                                    <td> 
                                                        <select class="form-control col-sm-2 empDesig" id="optDesig" name="optDesig[]">
                                                            <option value="">Select Designation</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control col-sm-2 empNum" placeholder="Emp" name="txtEmpNo[]" id="txtEmpNo"  />
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control col-sm-2 empHrs" placeholder="Hrs" name="txtEmpHrs[]" id="txtEmpHrs"  />
                                                    </td>
                                                    <td>
                                                        <a href="javascript:void(0);" class="trDelete" >X</a>
                                                    </td>
                                                </tr>
                                                <tr id="add_new_row">
                                                    <td colspan="5">
                                                        <a id="add_new" title="Add New" class="pull-right" href="javascript:void(0)"><i class="fa fa-plus"></i></a>
                                                    </td>
                                                </tr>                                          
                                            </table>

                                            <!-- <input type="text" name="txtProfitEmployee" id="txtProfitEmployee" class="form-control"> -->
                                            <?php //echo form_error('txtProfitEmployee', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-sm-7 control-label">Sales</label>
                                        <div class="col-sm-5">
                                            <label id="lblSalesCost"></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-7 control-label">Direct Expenses</label>
                                        <div class="col-sm-5">
                                            <label id="lblDirectExpense"></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-7 control-label">Gross Profit</label>
                                        <div class="col-sm-5">
                                            <label id="lblGrossProfit"></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-7 control-label">Gross Profit (%)</label>
                                        <div class="col-sm-5">
                                            <label id="lblGrossProfitPercent"></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-7 control-label">Employee Allocation</label>
                                        <div class="col-sm-5">
                                            <label id="lblEmpAllocation"></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-7 control-label">Loading Cost</label>
                                        <div class="col-sm-5">
                                            <label id="lblLoadingCost"></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-7 control-label">Total Indirect Cost</label>
                                        <div class="col-sm-5">
                                            <label id="lblTotalIndirectCost" ></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-7 control-label">Earning Margin</label>
                                        <div class="col-sm-5">
                                            <label id="lblEarningMargin" ></label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary" id="btnSubmit" >Submit</button>
                            <button type="button" class="btn btn-success pull-right" id="btnSubmit" >Save</button>
                        </div>
                    </form>
                </div><!-- /.box -->

                <!-- Modal -->
                    <div class="modal fade" id="directCostModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form class="form-horizontal" name="frmEditUserTimesheet" id="frmEditUserTimesheet" method="POST" action="<?php echo base_url("dashboard/updateTask") ?>" >
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Add Direct Cost Factors</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <table class="table table-bordered">
                                                        <tr>
                                                            <th>Title</th>
                                                            <th>Amount</th>
                                                        </tr>
                                                        <tr id="DCRow_1" class="tblDCRow">
                                                            <td class="col-sm-9">
                                                                <input type="text" class="form-control col-sm-6 " name="txtFactorTitle[]" id="txtFactorTitle"  />
                                                            </td>
                                                            <td>
                                                                <input type="text" class="form-control col-sm-2 " name="txtFactorAmount[]" id="txtFactorAmount"  />
                                                            </td>
                                                        </tr>
                                                        <tr id="add_new_factor_row">
                                                            <td colspan="5">
                                                                <a id="add_new_factor" title="Add New" class="pull-right" href="javascript:void(0)"><i class="fa fa-plus"></i></a>
                                                            </td>
                                                        </tr>                                          
                                                    </table>

                                                    <!-- <input type="text" name="txtProfitEmployee" id="txtProfitEmployee" class="form-control"> -->
                                                    <?php //echo form_error('txtProfitEmployee', '<div class="form-error">', '</div>'); ?>
                                                </div>
                                            </div>

                                    </div>
                                    <div class="modal-footer">
                                        <span> &nbsp;&nbsp;&nbsp; <img id="editTaskLoader" style="display:none;" src="<?php echo base_url("assets/dist/img/spin.svg") ?>" /> </span>
                                        <button type="submit" id="taskUpdate" class="btn btn-primary">Save</button>
                                        <button type="button" id="taskDelete" data-id="" class="btn btn-danger">Delete</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

            </div><!--/.col (left) -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<?php echo $this->load->view("common/footer"); ?>

<script src="<?php echo base_url(); ?>assets/plugins/jValidate/formValidations.js"></script>


<script>

    $("#add_new").click(function () { 

        var rowId = $(this).parents('tr').prev('tr').attr('id');
        var rowArr = rowId.split("_");
        rowArr[1]++;
        rowId = rowArr[0]+ "_" +rowArr[1];
        var content = "<tr id='"+ rowId +"' class='tblRow' >";
        content += $(this).parents('tr').prev('tr').html();
        content += "</td>";

        $(content).insertBefore('#add_new_row');
    });
    $("#add_new_factor").click(function () { 

        var rowId = $(this).parents('tr').prev('tr').attr('id');
        var rowArr = rowId.split("_");
        rowArr[1]++;
        rowId = rowArr[0]+ "_" +rowArr[1];
        var content = "<tr id='"+ rowId +"' class='tblDCRow' >";
        content += $(this).parents('tr').prev('tr').html();
        content += "</td>";

        $(content).insertBefore('#add_new_factor_row');
    });


    $(document).on('click', '.trDelete', function () { 

        var numItems = $('.tblRow').length;

        if(numItems == 1){
            alert("not allowed");
        }else{
            var rowId = $(this).parents('tr').attr('id');
            $("#"+rowId).remove();
        }
    });


    $("#optProfitEstClient").chosen();
    $("#optProfitEstCode").chosen();
    // $("#optEmpAllocation").chosen();

    var grossProfit, grossProfitPercent, totalIndirectCost, earningMargin = 0;

    $("#optProfitEstClient").change(function () {
        $.ajax({
            url: '<?php echo base_url('dashboard/getClientCodes'); ?>',
            type: "POST",
            dataType: "JSON",
            data: {"clientId": $("#optProfitEstClient").val()},
            beforeSend: function() { 
                $('#loader').css("display", "block");
            },
            success: function (response) {
                $('#loader').css("display", "none");
                $("#optProfitEstCode").empty();
                $("#optProfitEstCode").append("<option value=''>Select Code</option>");
                if (response) {


                    $.each(response, function (index, itemData) {

                        $("#optProfitEstCode").append("<option value='" + itemData.id + "'>" + itemData.pCode + "</option>");

                    });

                }
                $('#optProfitEstCode').trigger("chosen:updated");

            }

        });
    });


    $("#btnSubmit").click(function () {
        
        $.ajax({
            url: '<?php echo base_url('profitability/checkEstimation'); ?>',
            type: "POST",
            dataType: "JSON",
            data: $("#frmProfitEstimation").serialize(),
            success: function (response) {
                if (response) {
                    $("#lblSalesCost").text(response.salesCost);
                    $("#lblDirectExpense").text(response.directCost);
                    $("#lblGrossProfit").text(response.gpCost);
                    $("#lblGrossProfitPercent").text(response.gpPercent);
                    $("#lblEmpAllocation").text(response.empCost);
                    $("#lblLoadingCost").text(response.loadingCost);
                    // $("#lblIndirectCost").text(response.indirectCost);
                    $("#lblTotalIndirectCost").text(response.totalIndirectCost);
                    $("#lblEarningMargin").text(response.ProfitMargin);
                }
            }
        });
        return false;
    });


    $(document).on('change', '.empDept', function () {

        var rowId = $(this).parents('tr').attr('id');
        // alert(rowId);

        $.ajax({
            url: '<?php echo base_url('department/getDeptDesignations'); ?>',
            type: "POST",
            dataType: "JSON",
            data: {"deptId": $(this).val()},
            beforeSend: function() { 
                $('#loader').css("display", "block");
            },
            success: function (response) {
                $('#loader').css("display", "none");
                $("#"+rowId).find('.empDesig').empty();
                if (response) {

                    $.each(response, function (index, itemData) {

                        $("#"+rowId).find('.empDesig').append("<option value='" + itemData.id + "'>" + itemData.desig_name + "</option>");

                    });
                }

            }

        });
    });


        

    /*
        $("#frmProfitEstimation").validate({
            errorClass: "form-error",
            errorElement: "span",
            rules: {
                optProfitEstClient: {
                    required: true
                },
                optProfitEstCode: {
                    required: true
                },
                txtProfitSalesCost: {
                    required: true,
                    number:true
                },
                txtProfitDirectCost: {
                    required: true,
                    number:true
                },
                txtProfitLoadingCost: {
                    required: true,
                    number:true
                },
                txtProfitIndirectCost: {
                    required: true,
                    number:true
                },
                'optDept[]':{
                    required: true
                }
            },
            messages: {
                optProfitEstClient: {
                    required: true
                },
                optProfitEstCode: {
                    required: true
                },
                txtProfitSalesCost: {
                    required: true,
                    number:true
                },
                txtProfitDirectCost: {
                    required: true,
                    number:true
                },
                txtProfitLoadingCost: {
                    required: true,
                    number:true
                },
                txtProfitIndirectCost: {
                    required: true,
                    number:true
                }
            },
            errorPlacement: function (error, element) {
                if (element.attr("name") == "txtHolidayDate") {
                    error.insertAfter($(".holidayDate"));
                } else {
                    error.insertAfter(element);
                }
            }
        });

    $('select.empDept').each(function () {
            $(this).rules('add', {
                required: true
            });
        });*/
        
   


</script>