<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Holidays extends CI_Controller {
    /*
     * function name :__construct
     *  Counstructor for Holidays controller 
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('datatables');
        $this->load->library('form_validation');
        $this->load->model('holidays_model');
    }

    /*
     * function name :index
     *  Shows the holidays listing page.
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    public function index() {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Holidays"
            );

            $data['holidays_results'] = $this->holidays_model->getHolidaysData();

            $this->load->view('holidays/holidayList', $data);
        } else {
            redirect("login");
        }
    }

    public function datatable() {

        $column = '<a class="" href="'. base_url("holidays/edit/$1") .'"><i class="fa fa-edit fa-lg"></i></a>&nbsp;&nbsp;
         <a data-href="'. base_url("holidays/delete/$1") .'" data-toggle="modal" data-target="#deleteModal" href="#"><i class="fa fa-trash fa-lg"></i></a>';

        $this->datatables->select("id, DATE_FORMAT(holiday_date, '%D %b, %Y') as date, DATE_FORMAT(holiday_date, '%W') as day , holiday_desc", FALSE)
                ->from('holidays_data')
                ->edit_column('id', '<input type="checkbox"  name="check_holidays[]" value="$1"  class="multi_check" />', 'id')
                ->add_column('edit', "$column", 'id');

                echo $this->datatables->generate();
    }

    /*
     * function name :showHolidays
     *  Shows the holidays listing for all users.
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : string $year
     * @return : void
     */

    public function showHolidays($year = NULL) {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Holidays"
            );
            $data['selectedYear'] = $year;
            $data['holidays'] = $this->holidays_model->getYearlyHolidays($year);
            $data['years'] = $this->holidays_model->getAllHolidayYears();

            $this->load->view('holidays/showHolidays', $data);
        } else {
            redirect("login");
        }
    }


    /*
     * function name :add
     *  Add Holiday Details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    public function add() {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Add Holiday"
            );

            if ($_POST) {

                $this->form_validation->set_rules('txtHolidayDate', 'Holiday Date', 'required');
                $this->form_validation->set_rules('txtHolidayDesc', 'Holiday Description', 'required');

                if ($this->form_validation->run() == true) {

                    $insertData = array(
                        "holiday_date" => $this->input->post("txtHolidayDate"),
                        "holiday_desc" => $this->input->post("txtHolidayDesc"),
                        "added_date" => date("Y-m-d H:i:s")
                    );
                    $result = $this->holidays_model->addHoliday($insertData);

                    if ($result) {
                        $this->session->set_flashdata("success_msg", "Holiday Saved Successfully.");
                        redirect("holidays");
                    } else {

                        redirect("holidays/add");
                    }
                }
            }

            $this->load->view('holidays/addHoliday', $data);
        } else {
            redirect("login");
        }
    }

    /*
     * function name : edit
     *  Update Holiday details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : Int $deptId
     * @return : void
     */

    public function edit($holidayId = NULL) {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Edit Holiday"
            );

            if ($_POST) {
                /*echo "<pre>";
                print_r($_POST);
                exit;*/
                $this->form_validation->set_rules('txtHolidayDate', 'Holiday Date', 'required');
                $this->form_validation->set_rules('txtHolidayDesc', 'Holiday Description', 'required');

                if ($this->form_validation->run() == true) {


                    $holidayId = $this->input->post("txtHolidayId");

                    $updateData = array(
                        "holiday_date" => $this->input->post("txtHolidayDate"),
                        "holiday_desc" => $this->input->post("txtHolidayDesc")
                    );
                    $result = $this->holidays_model->editHoliday($updateData, $holidayId);

                    if ($result) {
                        $this->session->set_flashdata("success_msg", "Holiday Updated Successfully.");
                        redirect("holidays");
                    } else {
                        redirect("holidays/edit/$holidayId");
                    }
                }
            }

            $data['holiday'] = $this->holidays_model->getHolidayDetails($holidayId);

            $this->load->view('holidays/editHoliday', $data);
        } else {
            redirect("login");
        }
    }

    /*
     * function name : delete
     *  Delete Holidays details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : Int $holidayId
     * @return : void
     */

    public function delete($holidayId = NULL) {

        if ($this->session->userdata('userid')) {

            if ($holidayId) {

                $this->holidays_model->deleteHoliday($holidayId);
            } else if ($_POST) {

                $holidayIds = $this->input->post("check_holidays");

                foreach ($holidayIds as $key => $value) {
                    $this->holidays_model->deleteHoliday($value);
                }
            }
            $this->session->set_flashdata("success_msg", "Department Deleted Successfully.");
            redirect("holidays");
        } else {
            redirect("login");
        }
    }

}

/* End of file holidays.php */
/* Location: ./application/controllers/holidays.php */