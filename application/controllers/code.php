<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Code extends CI_Controller {
    /*
     * function name :__construct
     *  Counstructor for Code controller 
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->library('datatables');
        $this->load->model('code_model');
        $this->load->model('client_model');
        $this->load->model('location_model');
        $this->load->library('excel');
    }

    /*
     * function name :index
     *  Shows the code listing page.
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */
    
    public function index() {

        if ($this->session->userdata('userid')) {
            
            //creating header title for header view page
            $data = array(
                "title" => "Code"
            );
            
            //$data['code_results'] = $this->code_model->getCodeData();

            $this->load->view('code/codeList', $data);
        } else {
            redirect("login");
        }
    }

    public function datatable() {

        $column = '<a class="" href="'. base_url("code/edit/$1") .'"><i class="fa fa-edit fa-lg"></i></a>&nbsp;&nbsp;
         <a data-href="'. base_url("code/delete/$1") .'" data-toggle="modal" data-target="#deleteModal" href="#"><i class="fa fa-trash fa-lg"></i></a>';

        $this->datatables->select("cd.id, cl.client_name, l.location_name, cd.type, cd.subtype, cd.suffix, cd.product_code, DATE_FORMAT(cd.created, '%d %b, %Y'), case cd.status  when '1' then 'Active'  when '0' then 'Inactive' end as status", FALSE)
                ->from('code_data as cd')
                ->join('client_data as cl', 'cl.id = cd.client_id', 'LEFT')
                ->join('location_data as l', 'l.id = cd.location', 'LEFT')
                ->edit_column('id', '<input type="checkbox"  name="check_codes[]" value="$1"  class="multi_check" />', 'id')
                ->add_column('edit', "$column", 'id');

                echo $this->datatables->generate();
    }


    /*
     * function name :checkProductCode
     * Checks product code is exsist or not
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : 
     * @return : bool true|false
     */

    public function checkProductCode() {

        if ($this->session->userdata('userid')) {

            
            $productCode = $this->input->post("txtProductCode");

            $result = $this->code_model->checkProductCode($productCode);

            if ($result) {
                echo "true";
            } else {
                echo "false";
            }
            exit;
        } else {
            redirect("login");
        }
    }


    /*
     * function name :checkJobCode
     * Checks job code is exsist or not
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : 
     * @return : bool true|false
     */

    public function checkJobCode() {

        if ($this->session->userdata('userid')) {

            
            $jobCode = $this->input->post("txtJobCode");
            $codeId = $this->input->post("codeId");

            $result = $this->code_model->checkJobCode($jobCode, $codeId);

            if ($result) {
                echo "true";
            } else {
                echo "false";
            }
            exit;
        } else {
            redirect("login");
        }
    }


    /*
     * function name :add
     *  Add Code Details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */
    
    public function add() {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Add Code"
            );

            if ($_POST) {

                $this->form_validation->set_rules('txtJobCode', 'Job Code', 'required|is_unique[code_data.job_code]');
                $this->form_validation->set_rules('optCodeClientName', 'Client Name', 'required');
                $this->form_validation->set_rules('optCodeClientType', 'Type', 'required');
                $this->form_validation->set_rules('optCodeClientSubType', 'Sub Type', 'required');
                $this->form_validation->set_rules('txtCodeSuffix', 'Suffix', 'required');
                $this->form_validation->set_rules('optCodeLocation', 'Location', 'required');
                $this->form_validation->set_rules('txtProductCode', 'Product Code', 'is_unique[code_data.product_code]');
                
                

                if ($this->form_validation->run() == true) {
                    
                    $codeStatus = $this->input->post("chkClientStatus");
                    
                    if(empty($codeStatus)){
                        $codeStatus = 0;
                    }

                    $insertData = array(
                        "client_id" => $this->input->post("optCodeClientName"),
                        "job_code" => strtoupper($this->input->post("txtJobCode")),
                        "type" => $this->input->post("optCodeClientType"),
                        "subtype" => $this->input->post("optCodeClientSubType"),
                        "suffix" => $this->input->post("txtCodeSuffix"),
                        "product_code" => $this->input->post("txtProductCode"),
                        "location" => $this->input->post("optCodeLocation"),
                        "status" => $codeStatus,
                        "created" => date("Y-m-d H:i:s")
                    );
                    $startDate = $this->input->post("txtCodeStartDate");
                    if($startDate){
                        $insertData['start_date'] = $startDate;
                    }
                    $endDate = $this->input->post("txtCodeEndDate");
                    if($endDate){
                        $insertData['end_date'] = $endDate;
                    }

                    $result = $this->code_model->addCode($insertData);

                    if ($result) {
                        redirect("code");
                    } else {
//                        $this->session->set_flashdata("login_msg", "Invalid Username/Password!!!");
                        redirect("code/add");
                    }
                }
            }
            
            $data['clients'] = $this->client_model->getActiveClientData();
            $data['locations'] = $this->location_model->getActiveLocationData();

            $this->load->view('code/addCode', $data);
        } else {
            redirect("login");
        }
    }
    
    
    /*
     * function name : edit
     *  Update Code details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : Int $codeId
     * @return : void
     */

    public function edit($codeId = NULL) {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Edit Code"
            );

            if ($_POST) {

                $this->form_validation->set_rules('txtJobCode', 'Job Code', 'required|is_unique[code_data.job_code]');
                $this->form_validation->set_rules('optCodeClientName', 'Client Name', 'required');
                $this->form_validation->set_rules('optCodeClientType', 'Type', 'required');
                $this->form_validation->set_rules('optCodeClientSubType', 'Sub Type', 'required');
                $this->form_validation->set_rules('txtCodeSuffix', 'Suffix', 'required');
                $this->form_validation->set_rules('optCodeLocation', 'Location', 'required');
                $this->form_validation->set_rules('txtProductCode', 'Product Code', 'is_unique[code_data.product_code]');

                if ($this->form_validation->run() == true) {


                    $codeId = $this->input->post("txtCodeId");
                    
                    $codeStatus = $this->input->post("chkClientStatus");
                    
                    if(empty($codeStatus)){
                        $codeStatus = 0;
                    }
                    
//                    echo "$clientId";exit;

                    $updateData = array(
                        "client_id" => $this->input->post("optCodeClientName"),
                        "job_code" => strtoupper($this->input->post("txtJobCode")),
                        "type" => $this->input->post("optCodeClientType"),
                        "subtype" => $this->input->post("optCodeClientSubType"),
                        "suffix" => $this->input->post("txtCodeSuffix"),
                        "product_code" => $this->input->post("txtProductCode"),
                        "location" => $this->input->post("optCodeLocation"),
                        "status" => $codeStatus
                    );
                    $startDate = $this->input->post("txtCodeStartDate");
                    if($startDate){
                        $updateData['start_date'] = $startDate;
                    }
                    $endDate = $this->input->post("txtCodeEndDate");
                    if($endDate){
                        $updateData['end_date'] = $endDate;
                    }

                    $result = $this->code_model->editCode($updateData, $codeId);

                    if ($result) {
                        redirect("code");
                    } else {
                        redirect("code/edit/$codeId");
                    }
                }
            }

            $data['clients'] = $this->client_model->getActiveClientData();
            $data['locations'] = $this->location_model->getActiveLocationData();
            
            $data['code'] = $this->code_model->getCodeDetails($codeId);
//            print_r($data);exit;

            $this->load->view('code/editCode', $data);
        } else {
            redirect("login");
        }
    }
    
    /*
     * function name : delete
     *  Delete Code details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : Int $codeId
     * @return : void
     */

    public function delete($codeId = NULL) {

        if ($this->session->userdata('userid')) {

            if ($codeId) {

                $this->code_model->deleteCode($codeId);
            } else if ($_POST) {

                $codeIds = $this->input->post("check_codes");

                foreach ($codeIds as $key => $value) {
                    $this->code_model->deleteCode($value);
                }
            }
            redirect("code");
        } else {
            redirect("login");
        }
    }


    /*
     * function name : exportCodes
     *  Export Codes details to Excel Sheet
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : 
     * @return : void
     */

    public function exportCodes() {

        if ($this->session->userdata('userid')) {

            $data['code_results'] = $this->code_model->getCodeData();

            //Set active worksheet
            $this->excel->setActiveSheetIndex(0);

            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('Glitch Codes');

            //Set font size and bold effect
            $this->excel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(8);
            $this->excel->getActiveSheet()->getStyle("A1:I1")->getFont()->setBold(true);    
            $this->excel->getActiveSheet()->getStyle("A1:I1")->getFont()->setSize(10);    

            $this->excel->getActiveSheet()->setCellValueByColumnAndRow('0', 1, "Client");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow('1', 1, "Location");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow('2', 1, "Cateogry");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow('3', 1, "Sub Category");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow('4', 1, "Suffix");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow('5', 1, "Product Code");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow('6', 1, "Start Date");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow('7', 1, "End Date");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow('8', 1, "Status");


            $col = 0;
            $row = 2;
            foreach ($data['code_results'] as $key => $code) {

                $codeStartDate = "";
                $codeEndDate = "";
                $codeStatus = "";

                if($code['start_date'] != NULL && $code['start_date'] != '0000-00-00'){
                    $codeStartDate = $code['start_date'];
                }

                if($code['end_date'] != NULL && $code['end_date'] != '0000-00-00'){
                    $codeEndDate = $code['end_date'];
                }


                if($code['status'] == 1){
                    $codeStatus = "Active";
                }else{
                    $codeStatus = "Inactive";
                }

                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $code['client_name']);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+1, $row, $code['location_name'] );
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+2, $row, $code['type']);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row, $code['subtype']);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+4, $row, $code['suffix']);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+5, $row, $code['product_code']);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+6, $row, $codeStartDate);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+7, $row, $codeEndDate);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+8, $row, $codeStatus);
                
                $row++;
            }

            $filename = 'Codes.xlsx'; //save our workbook as this file name
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache

            $objWriter = new PHPExcel_Writer_Excel2007($this->excel);
            $objWriter->setOffice2003Compatibility(true);

            $objWriter->save('php://output');
            exit;

        } else {
            redirect("login");
        }
    }

}

/* End of file code.php */
/* Location: ./application/controllers/code.php */