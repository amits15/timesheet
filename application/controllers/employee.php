<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Employee extends CI_Controller {
    /*
     * function name :__construct
     *  Counstructor for Employee controller 
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('datatables');
        $this->load->library('form_validation');
        $this->load->model('emp_model');
        $this->load->model('location_model');
        $this->load->model('dept_model');
        $this->load->model('desig_model');
        $this->load->library('excel');
    }
    
    /*
     * function name :index
     *  Shows the Employee listing page.
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    public function index() {

        if ($this->session->userdata('userid')) {
            
            //creating header title for header view page
            $data = array(
                "title" => "Employee"
            );
            
            // $data['emp_results'] = $this->emp_model->getEmployeeData();

            $this->load->view('employee/employeeList', $data);
        } else {
            redirect("login");
        }
    }

    public function datatable() {

        $column = '<a class="" href="'. base_url("employee/edit/$1") .'"><i class="fa fa-edit fa-lg"></i></a>&nbsp;&nbsp;
         <a data-href="'. base_url("employee/delete/$1") .'" data-toggle="modal" data-target="#deleteModal" href="#"><i class="fa fa-trash fa-lg"></i></a>';

        $this->datatables->select("u.id, CAST(u.emp_code AS DECIMAL) as emp_code,CONCAT(u.first_name, ' ', u.last_name) as name,  u.email_id, dp.dept_name, ds.desig_name, l.location_name, u.employee_type, case u.status  when '1' then 'Active'  when '0' then 'Inactive' end as status", FALSE)
                ->from('user_data as u')
                ->join('dept_data as dp', 'dp.id = u.department', 'LEFT')
                ->join('designation_data as ds', 'ds.id = u.designation', 'LEFT')
                ->join('location_data as l', 'l.id = u.location', 'LEFT')
                ->edit_column('id', '<input type="checkbox"  name="check_emps[]" value="$1"  class="multi_check" />', 'id')
                ->add_column('edit', "$column", 'id');

                echo $this->datatables->generate();
    }

    /*
     * function name :checkEmpCode
     * Checks emp code is exsist or not
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : 
     * @return : bool true|false
     */

    public function checkEmpCode() {

        if ($this->session->userdata('userid')) {

            
            $empCode = $this->input->post("txtEmpCode");
            $empId = $this->input->post("empId");

            $result = $this->emp_model->checkEmpCode($empCode, $empId);

            if ($result) {
                echo "true";
            } else {
                echo "false";
            }
            exit;
        } else {
            redirect("login");
        }
    }
    
    /*
     * function name :add
     *  Add employee Details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */
    
    public function add() {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Add Employee"
            );

            if ($_POST) {

                // $this->form_validation->set_rules('txtEmpCode', 'Emp code', 'required');
                $this->form_validation->set_rules('txtEmpFirstName', 'First Name', 'required');
                $this->form_validation->set_rules('txtEmpLastName', 'Last Name', 'required');
                $this->form_validation->set_rules('txtEmpEmail', 'Email', 'required|email');
                // $this->form_validation->set_rules('txtEmpPassword', 'Password', 'trim|required');
                // $this->form_validation->set_rules('txtEmpPhone', 'Phone', 'required');
                // $this->form_validation->set_rules('txtEmpDOB', 'DOB', 'required');
                $this->form_validation->set_rules('rdoEmpGender', 'Gender', 'required');
                $this->form_validation->set_rules('optEmpDept', 'Department', 'required');
                $this->form_validation->set_rules('optEmpDesignation', 'Designation', 'required');
                // $this->form_validation->set_rules('txtEmpSalary', 'Salary', 'required');
                $this->form_validation->set_rules('optEmpLocation', 'Location', 'required');
                $this->form_validation->set_rules('optEmpType', 'Employee type', 'required');
                $this->form_validation->set_rules('txtEmpDOJ', 'DOJ', 'required');
                
                

                if ($this->form_validation->run() == true) {
                    
                    $empStatus = $this->input->post("chkEmpStatus");
                    
                    if(empty($empStatus)){
                        $empStatus = 0;
                    }
                    
                    $password = md5(strtolower($this->input->post("txtEmpFirstName")) . substr(strtolower($this->input->post("txtEmpLastName")), 0, 1). "123");

                    $insertData = array(
                        "emp_code" => $this->input->post("txtEmpCode"),
                        "first_name" => $this->input->post("txtEmpFirstName"),
                        "last_name" => $this->input->post("txtEmpLastName"),
                        "email_id" => $this->input->post("txtEmpEmail"),
                        "password" => $password,
                        "phone" => $this->input->post("txtEmpPhone"),
                        "gender" => $this->input->post("rdoEmpGender"),
                        "date_of_birth" => $this->input->post("txtEmpDOB"),
                        "department" => $this->input->post("optEmpDept"),
                        "designation" => $this->input->post("optEmpDesignation"),
                        "salary" => $this->input->post("txtEmpSalary"),
                        "location" => $this->input->post("optEmpLocation"),
                        "employee_type" => $this->input->post("optEmpType"),
                        "joining_date" => $this->input->post("txtEmpDOJ"),
                        "status" => $empStatus,
                        "created_on" => date("Y-m-d H:i:s")
                    );
                    
                    $leavingDate = $this->input->post("txtEmpLWD");
                    if($leavingDate){
                        $insertData['leaving_date'] = $leavingDate;
                    }
                    
                    $result = $this->emp_model->addEmp($insertData);

                    if ($result) {
                        redirect("employee");
                    } else {
//                        $this->session->set_flashdata("login_msg", "Invalid Username/Password!!!");
                        redirect("employee/add");
                    }
                }
            }
            
            $data['dept_results'] = $this->dept_model->getActiveDeptData();
            $data['locations'] = $this->location_model->getActiveLocationData();
            
            $this->load->view('employee/addEmployee', $data);
        } else {
            redirect("login");
        }
    }
    
    
    /*
     * function name : edit
     *  Update Employee details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : Int $empId
     * @return : void
     */

    public function edit($empId = NULL) {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Edit Employee"
            );

            if ($_POST) {

                // $this->form_validation->set_rules('txtEmpCode', 'Emp code', 'required');
                $this->form_validation->set_rules('txtEmpFirstName', 'First Name', 'required');
                $this->form_validation->set_rules('txtEmpLastName', 'Last Name', 'required');
                $this->form_validation->set_rules('txtEmpEmail', 'Email', 'required|email');
               // $this->form_validation->set_rules('txtEmpPassword', 'Password', 'trim|required');
                // $this->form_validation->set_rules('txtEmpPhone', 'Phone', 'required');
                // $this->form_validation->set_rules('txtEmpDOB', 'DOB', 'required');
                $this->form_validation->set_rules('rdoEmpGender', 'Gender', 'required');
                $this->form_validation->set_rules('optEmpDept', 'Department', 'required');
                $this->form_validation->set_rules('optEmpDesignation', 'Designation', 'required');
                // $this->form_validation->set_rules('txtEmpSalary', 'Salary', 'required');
                $this->form_validation->set_rules('optEmpLocation', 'Location', 'required');
                $this->form_validation->set_rules('optEmpType', 'Employee type', 'required');
                $this->form_validation->set_rules('txtEmpDOJ', 'DOJ', 'required');

                $empId = $this->input->post("txtEmpId");

                if ($this->form_validation->run() == true) {                    
                    
                    $empStatus = $this->input->post("chkEmpStatus");
                    
                    if(empty($empStatus)){
                        $empStatus = 0;
                    }

                    $updateData = array(
                        "emp_code" => $this->input->post("txtEmpCode"),
                        "first_name" => $this->input->post("txtEmpFirstName"),
                        "last_name" => $this->input->post("txtEmpLastName"),
                        "email_id" => $this->input->post("txtEmpEmail"),
                        "phone" => $this->input->post("txtEmpPhone"),
                        "gender" => $this->input->post("rdoEmpGender"),
                        "date_of_birth" => $this->input->post("txtEmpDOB"),
                        "department" => $this->input->post("optEmpDept"),
                        "designation" => $this->input->post("optEmpDesignation"),
                        "salary" => $this->input->post("txtEmpSalary"),
                        "location" => $this->input->post("optEmpLocation"),
                        "employee_type" => $this->input->post("optEmpType"),
                        "joining_date" => $this->input->post("txtEmpDOJ"),
                        "status" => $empStatus
                    );
                    $leavingDate = $this->input->post("txtEmpLWD");
                    if($leavingDate){
                        $updateData['leaving_date'] = $leavingDate;
                    }
                    
                    $password = md5(strtolower($this->input->post("txtEmpFirstName")) . substr(strtolower($this->input->post("txtEmpLastName")), 0, 1). "123");
                    $updateData['password'] = $password;
                    
                    $result = $this->emp_model->editEmp($updateData, $empId);

                    if ($result) {
                        redirect("employee");
                    } else {
                        redirect("employee/edit/$empId");
                    }
                }
            }

            $data['emp'] = $this->emp_model->getEmpDetails($empId);
            $data['dept_results'] = $this->dept_model->getActiveDeptData();
            $data['desig_results'] = $this->dept_model->getDeptDesignations($data['emp']['department']);
            $data['locations'] = $this->location_model->getActiveLocationData();
            

            $this->load->view('employee/editEmployee', $data);
        } else {
            redirect("login");
        }
    }
    
    /*
     * function name : delete
     *  Delete Employee details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : Int $empId
     * @return : void
     */

    public function delete($empId = NULL) {

        if ($this->session->userdata('userid')) {

            if ($empId) {

                $this->emp_model->deleteEmp($empId);
            } else if ($_POST) {

                $empIds = $this->input->post("check_emps");

                foreach ($empIds as $key => $value) {
                    $this->emp_model->deleteEmp($value);
                }
            }
            redirect("employee");
        } else {
            redirect("login");
        }
    }



    
    /*
     * function name : exportEmployees
     *  Export Employee details to Excel Sheet
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : 
     * @return : void
     */

    public function exportEmployees() {

        if ($this->session->userdata('userid')) {

            $data['emp_results'] = $this->emp_model->getEmployeeData();

            //Set active worksheet
            $this->excel->setActiveSheetIndex(0);

            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('Employees');

            //Set font size and bold effect
            $this->excel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(8);
            $this->excel->getActiveSheet()->getStyle("A1:L1")->getFont()->setBold(true);    

            $this->excel->getActiveSheet()->setCellValueByColumnAndRow('0', 1, "Emp Code");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow('1', 1, "Name");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow('2', 1, "Email");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow('3', 1, "Phone");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow('4', 1, "DOB");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow('5', 1, "Department");
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow('6', 1, "Designation");
            if ($this->session->userdata('employee_type') == 'super_admin') {
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow('7', 1, "Salary");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow('8', 1, "Location");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow('9', 1, "Status");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow('10', 1, "Joining Date");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow('11', 1, "Last Working Date");
            }else{
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow('7', 1, "Location");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow('8', 1, "Status");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow('9', 1, "Joining Date");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow('10', 1, "Last Working Date");
            }
            


            $col = 0;
            $row = 2;
            foreach ($data['emp_results'] as $key => $emp) {

                $empDOB = "";
                $empDOJ = "";
                $empLWD = "";
                $empStatus = "";

                if($emp['date_of_birth'] != NULL && $emp['date_of_birth'] != '0000-00-00'){
                    $empDOB = $emp['date_of_birth'];
                }

                if($emp['joining_date'] != NULL && $emp['joining_date'] != '0000-00-00'){
                    $empDOJ = $emp['joining_date'];
                }

                if($emp['leaving_date'] != NULL && $emp['leaving_date'] != '0000-00-00'){
                    $empLWD = $emp['leaving_date'];
                }

                if($emp['status'] == 1){
                    $empStatus = "Active";
                }else{
                    $empStatus = "Inactive";
                }

                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $emp['emp_code']);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+1, $row, $emp['first_name'] . " " . $emp['last_name'] );
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+2, $row, $emp['email_id']);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+3, $row, $emp['phone']);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+4, $row, $empDOB);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+5, $row, $emp['dept_name']);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+6, $row, $emp['desig_name']);
                if ($this->session->userdata('employee_type') == 'super_admin') {
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+7, $row, $emp['salary']);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+8, $row, $emp['location_name']);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+9, $row, $empStatus);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+10, $row, $empDOJ);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+11, $row, $empLWD);
                }else{
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+7, $row, $emp['location_name']);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+8, $row, $empStatus);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+9, $row, $empDOJ);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col+10, $row, $empLWD);
                }
                
                $row++;
            }

            $filename = 'Employees.xlsx'; //save our workbook as this file name
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache

            $objWriter = new PHPExcel_Writer_Excel2007($this->excel);
            $objWriter->setOffice2003Compatibility(true);

            $objWriter->save('php://output');
            exit;

        } else {
            redirect("login");
        }
    }
    

    /*
     * function name : changeEmpPassword
     *  Change Employee password
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : 
     * @return : void
     */
    public function changeEmpPassword() {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Reset Employee Password"
            );

            if ($_POST) {

                // $this->form_validation->set_rules('txtEmpCode', 'Emp code', 'required');
                $this->form_validation->set_rules('optEmp', 'Employee Name', 'required');
                $this->form_validation->set_rules('txtEmpPwd', 'Password', 'required');
                $this->form_validation->set_rules('txtEmpConfirmPwd', 'Confirm password', 'required|matches[txtEmpPwd]');
               
                if ($this->form_validation->run() == true) {                    
                    
                    $empId = $this->input->post("optEmp");
                    $updateData = array(
                        "password" => md5($this->input->post("txtEmpPwd"))
                    );
                                        
                    $result = $this->emp_model->editEmp($updateData, $empId);

                    if ($result) {
                        $this->session->set_flashdata('pwd_msg', "Password updated successfully...");
                        redirect("employee/changeEmpPassword");
                    } else {
                        redirect("employee/changeEmpPassword");
                    }
                }
            }

            $data['employees'] = $this->emp_model->getEmployeeData();
            

            $this->load->view('login/changeEmpPassword', $data);
            
            
        } else {
            redirect("login");
        }
    }



    public function updatePassword() {

        if ($this->session->userdata('userid')) {

            $emp_results = $this->emp_model->getEmployeeData();


            foreach ($emp_results as $key => $emp_val) {

                $password = md5(strtolower($emp_val['first_name']) . substr(strtolower($emp_val['last_name']), 0, 1). "123");
                // echo $str. "<br/>";
                $this->emp_model->updatePassword($password, $emp_val['id']);
                
            }
            exit;
            
            redirect("employee");
        } else {
            redirect("login");
        }
    }



    public function timesheetReminder() {

        
        // $emp_results = $this->emp_model->getTimesheetEmployees();


        foreach ($emp_results as $key => $emp_val) {

            $password = md5(strtolower($emp_val['first_name']) . substr(strtolower($emp_val['last_name']), 0, 1). "123");
            // echo $str. "<br/>";
            $this->emp_model->updatePassword($password, $emp_val['id']);
            
        }
        exit;
    }

}

/* End of file code.php */
/* Location: ./application/controllers/code.php */