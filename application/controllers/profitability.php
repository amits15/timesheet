<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profitability extends CI_Controller {
    /*
     * function name :__construct
     *  Counstructor for Profitability controller 
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('excel');
        $this->load->library('form_validation');
        $this->load->model('code_model');
        $this->load->model('client_model');
        $this->load->model('emp_model');
        $this->load->model('dept_model');
        $this->load->model('profitability_model');
        $this->load->model('reports_model');
    }

    /*
     * function name :profitEstimation
     *  Shows the profit estimation calculation page.
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    public function profitEstimation() {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Profit Estimation"
            );

            $data['departments'] = $this->dept_model->getActiveDeptData();
            $data['clients'] = $this->client_model->getActiveClientData();

            $this->load->view('profitability/profitEstimation', $data);
        } else {
            redirect("login");
        }
    }

    /*
     * function name : checkEstimation
     *  Checks the project estimation
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : Int $clientId
     * @return : void
     */

    public function checkEstimation($clientId = NULL) {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Edit Client"
            );

            if ($_POST) {
                // echo "<pre>";
//                 print_r($_POST);
// exit;
                $salesCost = $this->input->post('txtProfitSalesCost');
                $directCost = $this->input->post('txtProfitDirectCost');

                $dept = $this->input->post('optDept');
                $desig = $this->input->post('optDesig');
                $noOfEmps = $this->input->post('txtEmpNo');
                $noOfHours = $this->input->post('txtEmpHrs');

                // $loadingCost = $this->input->post('txtProfitLoadingCost');
                $loadingCost = 0;
                $indirectCost = 0;
                // $indirectCost = $this->input->post('txtProfitIndirectCost');

                $arrCount = count($dept);

                $startDay = date('Y-m-01'); // hard-coded '01' for first day
                $endDay  = date('Y-m-t');
                $workingHours = $this->getTotalWorkingHrs($startDay, $endDay);

                $totalEmpCost = 0;
                for($i=0; $i<$arrCount; $i++){
                    $salResult = $this->profitability_model->getEmpSalary($dept[$i], $desig[$i]);
                    // echo "$salResult[maxSal] <br/>";
                    $perHourSal = round(($salResult['maxSal']/$workingHours));
                    $arrPerHourSal[] = $perHourSal;
                    $totalEmpCost += (($perHourSal * $noOfEmps[$i]) * $noOfHours[$i]);

                }

                $grossProfit = $salesCost - $directCost;

                $grossProfitPercent = round((($grossProfit / $salesCost) * 100));

                $totalIndirectCost = $totalEmpCost + $loadingCost + $indirectCost;

                $profitMargin = $grossProfit - $totalIndirectCost;

                $arrResponse = array(
                    "salesCost" => number_format($salesCost),
                    "directCost" => number_format($directCost),
                    "gpCost" => number_format($grossProfit),
                    "gpPercent" => $grossProfitPercent,
                    "empCost" => number_format($totalEmpCost),
                    "loadingCost" => number_format($loadingCost),
                    "indirectCost" => number_format($indirectCost),
                    "totalIndirectCost" => number_format($totalIndirectCost),
                    "ProfitMargin" => number_format($profitMargin)
                    );
                echo json_encode($arrResponse);
                exit;

                // print_r($noOfEmps);
                // print_r($noOfHours);
                // print_r($arrPerHourSal);
                // print_r($totalEmpCost);
                exit;
                
            }

            $this->load->view('client/editClient', $data);
        } else {
            redirect("login");
        }
    }

    /*
     * function name : getTotalWorkingHrs
     *  Fetch the total working hours of current month
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : String $startDate, String $endDate
     * @return : void
     */
    public function getTotalWorkingHrs($startDate, $endDate) {
        $holidayCounts = $this->reports_model->getTotalWorkingDays($startDate, $endDate);

        $start = strtotime($startDate);
        $end = strtotime($endDate . '+1 day');

        $datediff = $end - $start;
        $totalDays = floor($datediff / (60 * 60 * 24));

        $days = $totalDays;

        for ($i = $startDate; $i <= $endDate; $i++) {
            $curr = date("D", strtotime($i));

            // substract if Saturday or Sunday
            if ($curr == 'Sat' || $curr == 'Sun') {
                $days--;
            }
        }
        $days -= $holidayCounts;

        $workinghrs = 8 * $days;

        return $workinghrs;
    }


    /*
     * function name : importEXcel
     *  Import Tally excel data into system.
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : 
     * @return : void
     */

    public function importEXcel(){
        //read file from path
        $objPHPExcel = PHPExcel_IOFactory::load(getcwd(). "/test.xlsx");
        //get only the Cell Collection
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

        foreach ($cell_collection as $cell) {
            $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
         
            //header will/should be in row 1 only. of course this can be modified to suit your need.
            if ($row == 1) {
                $header[$row][$column] = $data_value;
            } else {
                $arr_data[$row][$column] = $data_value;
            }
        }

        echo "<pre>";
        print_r($header);
        print_r($arr_data);exit;
    }

}

/* End of file profitability.php */
/* Location: ./application/controllers/profitability.php */