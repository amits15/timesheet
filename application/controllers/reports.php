<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports extends CI_Controller {
    /*
     * function name :__construct
     *  Counstructor for Reports controller 
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->library('excel');
        $this->load->model('reports_model');
        $this->load->model('location_model');
        $this->load->model('code_model');
        $this->load->model('dept_model');
    }

    /*
     * function name :index
     *  Shows the report filter page.
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    public function index() {

        if ($this->session->userdata('userid')) {

        //creating header title for header view page;
            $data = array(
                "title" => "Report with Original Data"
            );

            $data['locations'] = $this->location_model->getActiveLocationData();
            $data['departments'] = $this->dept_model->getActiveDeptData();

            $this->load->view('reports/userOriginalData', $data);
        } else {
            redirect("login");
        }
    }

    /*
     * function name :userOriginalData
     *  Shows the user Report by percentage or hours.
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    public function userOriginalData() {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Reports"
            );

            if ($_POST) {



                $reportDept = $this->input->post("optReportDepartment");
                $reportCategory = $this->input->post("optReportCategory");
                $reportLocation = $this->input->post("optReportLocation");
                $dateRange = $this->input->post("txtReportDateRange");
                $percentageFlag = $this->input->post("rdoPercentageFlag");
                $dateArr = explode(" - ", $dateRange);

                $startDate = "";
                $endDate = "";
                if (!empty($dateArr)) {
                    $startDate = $dateArr[0];
                    $endDate = $dateArr[1];
                }

                $workingHours = $this->getTotalWorkingHrs($startDate, $endDate);

                $empData = $this->reports_model->getEmployeeData($reportDept);
                $codeData = $this->reports_model->getCodeData($reportCategory, $reportLocation);
                // echo "<pre>";
                // print_r($codeData);exit;
                //Set active worksheet
                $this->excel->setActiveSheetIndex(0);

                //name the worksheet
                $this->excel->getActiveSheet()->setTitle('Detailed Timesheet');

                //Set font size and bold effect
                $this->excel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(8);

                //set row height for header columns
                $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);

                //Freeze Header and 2 Left side columns
                $this->excel->getActiveSheet()->freezePane("ZZ2");
                $this->excel->getActiveSheet()->freezePane("C2");

                $headerRow = 1;

                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $headerRow, "Name");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $headerRow, "Dept");
                $headerCol = 2;
                foreach ($codeData as $code_key => $code) {

                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol, $headerRow, $code['pCode']);

                    $HeaderColumn = PHPExcel_Cell::stringFromColumnIndex($headerCol);
                    $this->excel->getActiveSheet()->getColumnDimension("$HeaderColumn")->setAutoSize(false);
                    $this->excel->getActiveSheet()->getColumnDimension("$HeaderColumn")->setWidth("13");
                    $headerCol++;
                }


                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol, $headerRow, "CORP-\nINTERNAL");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 1, $headerRow, "CORP-\nFREETIME");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 2, $headerRow, "CORP-\nPITCH");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 3, $headerRow, "CORP-\nTRAINING");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 4, $headerRow, "PROJ-\nVIDEO");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 5, $headerRow, "PROJ-\nTECH");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 6, $headerRow, "PROJ-\nMEDIA");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 7, $headerRow, "PROJ-\nSTRAT");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 8, $headerRow, "RETA-\nRETAINER");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 9, $headerRow, "SUPPORT");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 10, $headerRow, "MGMT");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 11, $headerRow, "TOTAL\nTIME");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 12, $headerRow, "BENCH\nTIME");

                $row = 2;
                foreach ($empData as $emp_key => $emp) {

                    //Set font size and bold effect
                    $this->excel->getActiveSheet()->getStyle("A$row:ZZ$row")->getFont()->setSize(8);


                    $techMinutes = $techHours = 0;
                    $videoMinutes = $videoHours = 0;
                    $mediaMinutes = $mediaHours = 0;
                    $stratMinutes = $stratHours = 0;
                    $retMinutes = $retHours = 0;
                    $freetimeMinutes = $freetimeHours = 0;
                    $internalMinutes = $internalHours = 0;
                    $trainingMinutes = $trainingHours = 0;
                    $pitchMinutes = $pitchHours = 0;
                    $corpIntTotal = $corpFreeTotal = $corpPitchTotal = $corpTrainTotal = $projectVidTotal = $projectTechTotal = $projectMedTotal = $projectStratTotal = $retRetTotal = 0;
                    $totalTime = 0;





                    $col = 0;
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $emp['first_name'] . " " . $emp['last_name']);
                    $col++;
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $emp['dept_name']);
                    $col++;
                    //$firstColumn = PHPExcel_Cell::stringFromColumnIndex($col);
                    foreach ($codeData as $cd_key => $codeVal) {

                        $minutes = 0;
                        $hours = 0;
                        $hrs = 0;
                        $finalHrs = 0;

                        $totalHours = 0;
                        $suppTotalTime = $mgmtTotalTime = $workingHours;

                        $reportData = $this->reports_model->getUserTimesheetReport($codeVal['id'], $emp['id'], $startDate, $endDate);

                        if (!empty($reportData)) {

                            foreach ($reportData as $key => $task) {

                                $minutes += $this->explode_min($task['task_duration']);
                                $hours += $this->explode_hrs($task['task_duration']);


                                if ($task['type'] == 'project' && $task['subtype'] == 'tech') {

                                    $techMinutes += $this->explode_min($task['task_duration']);
                                    $techHours += $this->explode_hrs($task['task_duration']);
                                } else if ($task['type'] == 'project' && $task['subtype'] == 'video') {

                                    $videoMinutes += $this->explode_min($task['task_duration']);
                                    $videoHours += $this->explode_hrs($task['task_duration']);
                                } else if ($task['type'] == 'project' && $task['subtype'] == 'mediabuying') {

                                    $mediaMinutes += $this->explode_min($task['task_duration']);
                                    $mediaHours += $this->explode_hrs($task['task_duration']);
                                } else if ($task['type'] == 'project' && $task['subtype'] == 'strategy') {

                                    $stratMinutes += $this->explode_min($task['task_duration']);
                                    $stratHours += $this->explode_hrs($task['task_duration']);
                                } else if ($task['type'] == 'retainer' && $task['subtype'] == 'retainer') {

                                    $retMinutes += $this->explode_min($task['task_duration']);
                                    $retHours += $this->explode_hrs($task['task_duration']);
                                } else if ($task['type'] == 'corporate' && $task['subtype'] == 'freetime') {

                                    $freetimeMinutes += $this->explode_min($task['task_duration']);
                                    $freetimeHours += $this->explode_hrs($task['task_duration']);
                                } else if ($task['type'] == 'corporate' && $task['subtype'] == 'internal') {

                                    $internalMinutes += $this->explode_min($task['task_duration']);
                                    $internalHours += $this->explode_hrs($task['task_duration']);
                                } else if ($task['type'] == 'corporate' && $task['subtype'] == 'training') {

                                    $trainingMinutes += $this->explode_min($task['task_duration']);
                                    $trainingHours += $this->explode_hrs($task['task_duration']);
                                } else if ($task['type'] == 'corporate' && $task['subtype'] == 'pitch') {

                                    $pitchMinutes += $this->explode_min($task['task_duration']);
                                    $pitchHours += $this->explode_hrs($task['task_duration']);
                                }
                            }

                            $totalHours = $this->calculateHrs($minutes, $hours);


                            //Calculate Total hours for each code category
                            $projectTechTotal = round($this->calculateHrs($techMinutes, $techHours), 2);
                            $projectMedTotal = round($this->calculateHrs($mediaMinutes, $mediaHours), 2);
                            $projectVidTotal = round($this->calculateHrs($videoMinutes, $videoHours), 2);
                            $projectStratTotal = round($this->calculateHrs($stratMinutes, $stratHours), 2);

                            $retRetTotal = round($this->calculateHrs($retMinutes, $retHours), 2);

                            $corpFreeTotal = round($this->calculateHrs($freetimeMinutes, $freetimeHours), 2);
                            $corpIntTotal = round($this->calculateHrs($internalMinutes, $internalHours), 2);
                            $corpPitchTotal = round($this->calculateHrs($pitchMinutes, $pitchHours), 2);
                            $corpTrainTotal = round($this->calculateHrs($trainingMinutes, $trainingHours), 2);

                            //calculate totals in percentage
                            if($percentageFlag == 1){
                                $totalHours = round(($totalHours / $workingHours), 2);
                            }
                        }
                        
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $totalHours);
                        $col++;
                    }
                    //calculate totals in percentage
                    if($percentageFlag == 1){

                        $projectTechTotal = round(($projectTechTotal / $workingHours), 2);
                        $projectMedTotal = round(($projectMedTotal / $workingHours), 2);
                        $projectVidTotal = round(($projectVidTotal / $workingHours), 2);
                        $projectStratTotal = round(($projectStratTotal / $workingHours), 2);

                        $retRetTotal = round(($retRetTotal / $workingHours), 2);

                        $corpFreeTotal = round(($corpFreeTotal / $workingHours), 2);
                        $corpIntTotal = round(($corpIntTotal / $workingHours), 2);
                        $corpPitchTotal = round(($corpPitchTotal / $workingHours), 2);
                        $corpTrainTotal = round(($corpTrainTotal / $workingHours), 2);

                        $suppTotalTime = round(($workingHours / $workingHours));
                        $mgmtTotalTime = round(($workingHours / $workingHours));
                    }

                    $firstTotalColumn = PHPExcel_Cell::stringFromColumnIndex($col);
                    
                    //Calculate total hours spend on all tasks.
                    $totalTime = ($projectMedTotal + $projectStratTotal + $projectTechTotal + $projectVidTotal +
                            $retRetTotal + $corpIntTotal + $corpPitchTotal + $corpTrainTotal);
                    // echo "percent   ".$totalTime;exit;
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $corpIntTotal);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 1, $row, $corpFreeTotal);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 2, $row, $corpPitchTotal);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 3, $row, $corpTrainTotal);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 4, $row, $projectVidTotal);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 5, $row, $projectTechTotal);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 6, $row, $projectMedTotal);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 7, $row, $projectStratTotal);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 8, $row, $retRetTotal);
                    
                    if (in_array($emp['dept_id'], array('6','7','8','9','10','11') ) ) {
                        if($percentageFlag != 1){
                            $totalTime = $workingHours;
                        }else{
                            $totalTime = "100";
                        }
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 9, $row, $totalTime);
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 10, $row, 0);
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 11, $row, "$totalTime");
                    } else if ($emp['dept_id'] == '12') {
                        if($percentageFlag != 1){
                            $totalTime = $workingHours;
                        }else{
                            $totalTime = "100";
                        }
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 9, $row, 0);
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 10, $row, $totalTime);
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 11, $row, "$totalTime");
                    } else {
                        // echo $totalTime;exit;
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 9, $row, 0);
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 10, $row, 0);
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 11, $row, $totalTime);
                    }
                    if($percentageFlag != 1){
                        if ($totalTime < $workingHours) {
                            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 12, $row, ($workingHours - $totalTime) + $corpFreeTotal);
                        } else {
                            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 12, $row, "0");
                        }
                    }else{
                        if (($totalTime * 100) < 100) {
                            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 12, $row, ((100 - ($totalTime * 100)) + $corpFreeTotal) / 100 );
                        } else {
                            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 12, $row, "0");
                        }
                    }

                    $lastTotalColumn = PHPExcel_Cell::stringFromColumnIndex($col + 12);


                    $this->excel->getActiveSheet()->getStyle("$firstTotalColumn$row:$lastTotalColumn$row")->getFill()->applyFromArray(array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'startcolor' => array(
                            'rgb' => 'FAC090'
                        )
                    ));
                    //Set Background color for header columns
                    $this->excel->getActiveSheet()->getStyle($firstTotalColumn . "1:" . $lastTotalColumn . "1")->getFill()->applyFromArray(array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'startcolor' => array(
                            'rgb' => 'FAC090'
                        )
                    ));
                    if($percentageFlag == 1){
                        $this->excel->getActiveSheet()->getStyle("C2:$lastTotalColumn$row")->getNumberFormat()->applyFromArray( 
                            array( 
                                'code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE
                            )
                        );
                    }

                    $row++;
                }

                $percentText = (($percentageFlag == 1) ? "IN_Percentage_" : "IN_Hours_");
                $filename = 'ReportWithOriginalData_' . $percentText . "$startDate - $endDate" . '.xlsx'; //save our workbook as this file name
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); //mime type
                header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache

                $objWriter = new PHPExcel_Writer_Excel2007($this->excel);
                $objWriter->setOffice2003Compatibility(true);

                $objWriter->save('php://output');
                exit;
            }
        } else {
            redirect("login");
        }
    }

    /*
     * function name :cappedReport
     *  Shows the Capped report filter page.
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    public function cappedReport() {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Report with Capped Data"
            );

            $data['locations'] = $this->location_model->getActiveLocationData();
            $data['departments'] = $this->dept_model->getActiveDeptData();

            $this->load->view('reports/userCappedData', $data);
        } else {
            redirect("login");
        }
    }

    /*
     * function name :userCappedData
     *  Shows the user capped Report by hours.
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    public function userCappedData() {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Reports"
            );

            if ($_POST) {

                $reportDept = $this->input->post("optReportDepartment");
                $reportCategory = $this->input->post("optReportCategory");
                $reportLocation = $this->input->post("optReportLocation");
                $dateRange = $this->input->post("txtReportDateRange");
                $percentageFlag = $this->input->post("rdoPercentageFlag");
                $dateArr = explode(" - ", $dateRange);

                $startDate = "";
                $endDate = "";
                if (!empty($dateArr)) {
                    $startDate = $dateArr[0];
                    $endDate = $dateArr[1];
                }

                //Fetch total working hours in a month
                $workingHours = $this->getTotalWorkingHrs($startDate, $endDate);

                $empData = $this->reports_model->getEmployeeData($reportDept);
                $codeData = $this->reports_model->getCodeData($reportCategory, $reportLocation);
                //Set active worksheet
                $this->excel->setActiveSheetIndex(0);

                //name the worksheet
                $this->excel->getActiveSheet()->setTitle('Capped Data');

                //set font size for header row
                $this->excel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(8);

                //set height of header row 
                $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);

                //Freeze Header and 2 columns
                $this->excel->getActiveSheet()->freezePane("ZZ2");
                $this->excel->getActiveSheet()->freezePane("C2");

                $headerRow = 1;
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $headerRow, "Name");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $headerRow, "Dept");
                $headerCol = 2;

                foreach ($codeData as $code_key => $code) {

                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol, $headerRow, $code['pCode']);
                    
                    $HeaderColumn = PHPExcel_Cell::stringFromColumnIndex($headerCol);
                    $this->excel->getActiveSheet()->getColumnDimension("$HeaderColumn")->setAutoSize(false);
                    $this->excel->getActiveSheet()->getColumnDimension("$HeaderColumn")->setWidth("13");

                    $headerCol++;
                }

                //Header for capped data
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol, $headerRow, "CORP-\nINTERNAL");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 1, $headerRow, "CORP-\nFREETIME");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 2, $headerRow, "CORP-\nPITCH");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 3, $headerRow, "CORP-\nTRAINING");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 4, $headerRow, "PROJ-\nVIDEO");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 5, $headerRow, "PROJ-\nTECH");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 6, $headerRow, "PROJ-\nMEDIA");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 7, $headerRow, "PROJ-\nSTRAT");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 8, $headerRow, "RETA-\nRETAINER");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 9, $headerRow, "SUPPORT");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 10, $headerRow, "MGMT");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 11, $headerRow, "TOTAL\nTIME");

                $row = 2;
                foreach ($empData as $emp_key => $emp) {

                    $techMinutes = $techHours = 0;
                    $videoMinutes = $videoHours = 0;
                    $mediaMinutes = $mediaHours = 0;
                    $stratMinutes = $stratHours = 0;
                    $retMinutes = $retHours = 0;
                    $freetimeMinutes = $freetimeHours = 0;
                    $internalMinutes = $internalHours = 0;
                    $trainingMinutes = $trainingHours = 0;
                    $pitchMinutes = $pitchHours = 0;
                    $corpIntTotal = $corpFreeTotal = $corpPitchTotal = $corpTrainTotal = $projectVidTotal = $projectTechTotal = $projectMedTotal = $projectStratTotal = $retRetTotal = 0;
                    $hrs = 0;
                    $finalHrs = 0;
                    $totalTime = 0;
                    $empMinutes = 0;
                    $empHours = 0;
                    $totalEmpHours = 0;



                    $col = 0;
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $emp['first_name'] . " " . $emp['last_name']);
                    $col++;
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $emp['dept_name']);
                    $col++;
                    $reportData = $this->reports_model->getUserTimesheetReportByEmp($emp['id'], $startDate, $endDate);

                    if (!empty($reportData)) {

                        //Iterate Loop for calculating total minutes and hours for each Code category.
                        foreach ($reportData as $key => $task) {
                            $empMinutes += $this->explode_min($task['task_duration']);
                            $empHours += $this->explode_hrs($task['task_duration']);
                        }
                        $totalEmpHours = $this->calculateHrs($empMinutes, $empHours);
                    }

                    foreach ($codeData as $cd_key => $codeVal) {

                        $minutes = 0;
                        $hours = 0;
                        $hrs = 0;
                        $finalHrs = 0;

                        $totalHours = 0;
                        $calculatedTime = 0;
                        $suppTotalTime = $mgmtTotalTime = $workingHours;

                        $reportCodeData = $this->reports_model->getUserTimesheetReport($codeVal['id'], $emp['id'], $startDate, $endDate);

                        if (!empty($reportCodeData)) {


                            foreach ($reportCodeData as $key => $task) {

                                $minutes += $this->explode_min($task['task_duration']);
                                $hours += $this->explode_hrs($task['task_duration']);


                                if ($task['type'] == 'project' && $task['subtype'] == 'tech') {

                                    $techMinutes += $this->explode_min($task['task_duration']);
                                    $techHours += $this->explode_hrs($task['task_duration']);
                                } else if ($task['type'] == 'project' && $task['subtype'] == 'video') {

                                    $videoMinutes += $this->explode_min($task['task_duration']);
                                    $videoHours += $this->explode_hrs($task['task_duration']);
                                } else if ($task['type'] == 'project' && $task['subtype'] == 'mediabuying') {

                                    $mediaMinutes += $this->explode_min($task['task_duration']);
                                    $mediaHours += $this->explode_hrs($task['task_duration']);
                                } else if ($task['type'] == 'project' && $task['subtype'] == 'strategy') {

                                    $stratMinutes += $this->explode_min($task['task_duration']);
                                    $stratHours += $this->explode_hrs($task['task_duration']);
                                } else if ($task['type'] == 'retainer' && $task['subtype'] == 'retainer') {

                                    $retMinutes += $this->explode_min($task['task_duration']);
                                    $retHours += $this->explode_hrs($task['task_duration']);
                                } else if ($task['type'] == 'corporate' && $task['subtype'] == 'freetime') {

                                    $freetimeMinutes += $this->explode_min($task['task_duration']);
                                    $freetimeHours += $this->explode_hrs($task['task_duration']);
                                } else if ($task['type'] == 'corporate' && $task['subtype'] == 'internal') {

                                    $internalMinutes += $this->explode_min($task['task_duration']);
                                    $internalHours += $this->explode_hrs($task['task_duration']);
                                } else if ($task['type'] == 'corporate' && $task['subtype'] == 'training') {

                                    $trainingMinutes += $this->explode_min($task['task_duration']);
                                    $trainingHours += $this->explode_hrs($task['task_duration']);
                                } else if ($task['type'] == 'corporate' && $task['subtype'] == 'pitch') {

                                    $pitchMinutes += $this->explode_min($task['task_duration']);
                                    $pitchHours += $this->explode_hrs($task['task_duration']);
                                }
                            }

                            $totalHours = $this->calculateHrs($minutes, $hours);

                            if ($totalEmpHours != 0) {
                                $calculatedTime = round(($totalHours / $totalEmpHours) * $workingHours, 2);
                            } else {
                                $calculatedTime = $totalHours;
                            }



                            //Calculate Total hours for each code category
                            $projectTechTotal = round(($this->calculateHrs($techMinutes, $techHours) / $totalEmpHours * $workingHours), 2);
                            $projectMedTotal = round(($this->calculateHrs($mediaMinutes, $mediaHours) / $totalEmpHours * $workingHours), 2);
                            $projectVidTotal = round(($this->calculateHrs($videoMinutes, $videoHours) / $totalEmpHours * $workingHours), 2);
                            $projectStratTotal = round(($this->calculateHrs($stratMinutes, $stratHours) / $totalEmpHours * $workingHours), 2);

                            $retRetTotal = round(($this->calculateHrs($retMinutes, $retHours) / $totalEmpHours * $workingHours), 2);

                            $corpFreeTotal = round(($this->calculateHrs($freetimeMinutes, $freetimeHours) / $totalEmpHours * $workingHours), 2);
                            $corpIntTotal = round(($this->calculateHrs($internalMinutes, $internalHours) / $totalEmpHours * $workingHours), 2);
                            $corpPitchTotal = round(($this->calculateHrs($pitchMinutes, $pitchHours) / $totalEmpHours * $workingHours), 2);
                            $corpTrainTotal = round(($this->calculateHrs($trainingMinutes, $trainingHours) / $totalEmpHours * $workingHours), 2);
                        
                            //calculate totals in percentage
                            if($percentageFlag == 1){
                                $calculatedTime = round(($calculatedTime / $workingHours), 2);
                            }
                        }
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $calculatedTime);
                        $col++;
                    }

                    //calculate totals in percentage
                    if($percentageFlag == 1){

                        $projectTechTotal = round(($projectTechTotal / $workingHours), 2);
                        $projectMedTotal = round(($projectMedTotal / $workingHours), 2);
                        $projectVidTotal = round(($projectVidTotal / $workingHours), 2);
                        $projectStratTotal = round(($projectStratTotal / $workingHours), 2);

                        $retRetTotal = round(($retRetTotal / $workingHours), 2);

                        $corpFreeTotal = round(($corpFreeTotal / $workingHours), 2);
                        $corpIntTotal = round(($corpIntTotal / $workingHours), 2);
                        $corpPitchTotal = round(($corpPitchTotal / $workingHours), 2);
                        $corpTrainTotal = round(($corpTrainTotal / $workingHours), 2);

                        $suppTotalTime = round(($workingHours / $workingHours));
                        $mgmtTotalTime = round(($workingHours / $workingHours));
                    }

                    $totalTime = ($projectMedTotal + $projectStratTotal + $projectTechTotal + $projectVidTotal +
                            $retRetTotal + $corpFreeTotal + $corpIntTotal + $corpPitchTotal + $corpTrainTotal);


                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $corpIntTotal);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 1, $row, $corpFreeTotal);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 2, $row, $corpPitchTotal);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 3, $row, $corpTrainTotal);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 4, $row, $projectVidTotal);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 5, $row, $projectTechTotal);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 6, $row, $projectMedTotal);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 7, $row, $projectStratTotal);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 8, $row, $retRetTotal);


                    if ($emp['dept_id'] == '6') {
                        if($percentageFlag != 1){
                            $totalTime = $workingHours;
                        }else{
                            $totalTime = "100";
                        }
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 9, $row, $totalTime);
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 10, $row, 0);
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 11, $row, "$totalTime");
                    } else if ($emp['dept_id'] == '12') {
                        if($percentageFlag != 1){
                            $totalTime = $workingHours;
                        }else{
                            $totalTime = "100";
                        }
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 9, $row, 0);
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 10, $row, $totalTime);
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 11, $row, "$totalTime");
                    } else {
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 9, $row, 0);
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 10, $row, 0);
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 11, $row, $totalTime);
                    }

                    $firstTotalColumn = PHPExcel_Cell::stringFromColumnIndex($col);
                    $lastTotalColumn = PHPExcel_Cell::stringFromColumnIndex($col + 11);

                    $this->excel->getActiveSheet()->getStyle("$firstTotalColumn$row:$lastTotalColumn$row")->getFill()->applyFromArray(array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'startcolor' => array(
                            'rgb' => 'FAC090'
                        )
                    ));
                    //Set Background color for header columns
                    $this->excel->getActiveSheet()->getStyle($firstTotalColumn . "1:" . $lastTotalColumn . "1")->getFill()->applyFromArray(array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'startcolor' => array(
                            'rgb' => 'FAC090'
                        )
                    ));

                    if($percentageFlag == 1){
                        $this->excel->getActiveSheet()->getStyle("C2:$lastTotalColumn$row")->getNumberFormat()->applyFromArray( 
                            array( 
                                'code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE
                            )
                        );
                    }
                    $row++;
                }


                $percentText = (($percentageFlag == 1) ? "IN_Percentage_" : "IN_Hours_");
                $filename = 'ReportWithCappedData_' . $percentText . "$startDate - $endDate" . '.xlsx'; //save our workbook as this file name
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); //mime type
                header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
                
                header('Cache-Control: max-age=0'); //no cache

                $objWriter = new PHPExcel_Writer_Excel2007($this->excel);
                $objWriter->setOffice2003Compatibility(true);
                $objWriter->save('php://output');
                exit;
            }
        } else {
            redirect("login");
        }
    }

    /*
     * function name :withoutCorpReport
     *  Shows user report without corporate data page.
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    public function withoutCorpReport() {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Report without Corp & Bench"
            );

            $data['locations'] = $this->location_model->getActiveLocationData();
            $data['departments'] = $this->dept_model->getActiveDeptData();

            $this->load->view('reports/userWithoutCorpData', $data);
        } else {
            redirect("login");
        }
    }

    /*
     * function name :userWithoutCorpData
     *  Shows the user Report without corp data by hours.
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    public function userWithoutCorpData() {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Reports"
            );

            if ($_POST) {


                $reportDept = $this->input->post("optReportDepartment");
                $reportCategory = $this->input->post("optReportCategory");
                $reportLocation = $this->input->post("optReportLocation");
                $dateRange = $this->input->post("txtReportDateRange");
                $percentageFlag = $this->input->post("rdoPercentageFlag");
                $dateArr = explode(" - ", $dateRange);

                $startDate = "";
                $endDate = "";
                if (!empty($dateArr)) {
                    $startDate = $dateArr[0];
                    $endDate = $dateArr[1];
                }

                //Fetch total working hours in a month
                $workingHours = $this->getTotalWorkingHrs($startDate, $endDate);


                $empData = $this->reports_model->getEmployeeData($reportDept);
                $codeData = $this->reports_model->getCodeDataWithoutCorp($reportLocation);
                //Set active worksheet
                $this->excel->setActiveSheetIndex(0);

                //name the worksheet
                $this->excel->getActiveSheet()->setTitle('Without Corporate Data');

                //set font size and font effect as bold for header columns
                $this->excel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(8);

                //set header row height
                $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);


                //Freeze Header and 2 columns
                $this->excel->getActiveSheet()->freezePane("AH2");
                $this->excel->getActiveSheet()->freezePane("C2");


                $headerRow = 1;
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $headerRow, "Name");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $headerRow, "Dept");
                $headerCol = 2;

                foreach ($codeData as $code_key => $code) {

                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol, $headerRow, $code['pCode']);
                    
                    $HeaderColumn = PHPExcel_Cell::stringFromColumnIndex($headerCol);
                    $this->excel->getActiveSheet()->getColumnDimension("$HeaderColumn")->setAutoSize(false);
                    $this->excel->getActiveSheet()->getColumnDimension("$HeaderColumn")->setWidth("13");
                    $headerCol++;
                }

                //Header for capped data
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol, $headerRow, "PROJ-\nVIDEO");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 1, $headerRow, "PROJ-\nTECH");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 2, $headerRow, "PROJ-\nMEDIA");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 3, $headerRow, "PROJ-\nSTRAT");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 4, $headerRow, "RETA-\nRETAINER");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 5, $headerRow, "SUPPORT");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 6, $headerRow, "MGMT");
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($headerCol + 7, $headerRow, "TOTAL\nTIME");

                $row = 2;
                foreach ($empData as $emp_key => $emp) {

                    //Set default values for variables.
                    $techMinutes = $techHours = 0;
                    $videoMinutes = $videoHours = 0;
                    $mediaMinutes = $mediaHours = 0;
                    $stratMinutes = $stratHours = 0;
                    $retMinutes = $retHours = 0;
                    $freetimeMinutes = $freetimeHours = 0;
                    $internalMinutes = $internalHours = 0;
                    $trainingMinutes = $trainingHours = 0;
                    $pitchMinutes = $pitchHours = 0;
                    $corpTotal = $corpIntTotal = $corpFreeTotal = $corpPitchTotal = $corpTrainTotal = $projectVidTotal = $projectTechTotal = $projectMedTotal = $projectStratTotal = $retRetTotal = 0;
                    $hrs = 0;
                    $finalHrs = 0;
                    $totalTime = 0;
                    $empMinutes = 0;
                    $empHours = 0;
                    $totalEmpHours = 0;
                    $projectTechFinalTotal = $projectMedFinalTotal = $projectVidFinalTotal = $projectStratFinalTotal = $retRetFinalTotal = 0;
                    



                    $col = 0;
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $emp['first_name'] . " " . $emp['last_name']);
                    $col++;
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $emp['dept_name']);
                    $col++;
                    $reportData = $this->reports_model->getUserTimesheetReportByEmp($emp['id'], $startDate, $endDate);

                    if (!empty($reportData)) {


                        //Iterate Loop for calculating total minutes and hours for each Code category.
                        foreach ($reportData as $key => $task) {
                            $empMinutes += $this->explode_min($task['task_duration']);
                            $empHours += $this->explode_hrs($task['task_duration']);


                            if ($task['type'] == 'corporate' && $task['subtype'] == 'freetime') {

                                $freetimeMinutes += $this->explode_min($task['task_duration']);
                                $freetimeHours += $this->explode_hrs($task['task_duration']);
                            } else if ($task['type'] == 'corporate' && $task['subtype'] == 'internal') {

                                $internalMinutes += $this->explode_min($task['task_duration']);
                                $internalHours += $this->explode_hrs($task['task_duration']);
                            } else if ($task['type'] == 'corporate' && $task['subtype'] == 'training') {

                                $trainingMinutes += $this->explode_min($task['task_duration']);
                                $trainingHours += $this->explode_hrs($task['task_duration']);
                            } else if ($task['type'] == 'corporate' && $task['subtype'] == 'pitch') {

                                $pitchMinutes += $this->explode_min($task['task_duration']);
                                $pitchHours += $this->explode_hrs($task['task_duration']);
                            }
                        }
                        $totalEmpHours = round($this->calculateHrs($empMinutes, $empHours));

                        $corpFreeTotal = round($this->calculateHrs($freetimeMinutes, $freetimeHours));
                        $corpIntTotal = round($this->calculateHrs($internalMinutes, $internalHours));
                        $corpPitchTotal = round($this->calculateHrs($pitchMinutes, $pitchHours));
                        $corpTrainTotal = round($this->calculateHrs($trainingMinutes, $trainingHours));

                        $corpTotal = $corpFreeTotal + $corpIntTotal + $corpPitchTotal + $corpTrainTotal;
                    }

                    // echo $corpTotal. "<br>";
                    // echo $totalEmpHours;
                    foreach ($codeData as $cd_key => $codeVal) {

                        $minutes = 0;
                        $hours = 0;
                        $hrs = 0;
                        $finalHrs = 0;

                        $totalHours = 0;
                        $calculatedTime = 0;
                        $finalTaskTime = 0;
                        $suppTotalTime = $mgmtTotalTime = $workingHours;


                        $reportCodeData = $this->reports_model->getUserTimesheetReport($codeVal['id'], $emp['id'], $startDate, $endDate);

                        if (!empty($reportCodeData)) {


                            foreach ($reportCodeData as $key => $task) {

                                $minutes += $this->explode_min($task['task_duration']);
                                $hours += $this->explode_hrs($task['task_duration']);


                                if ($task['type'] == 'project' && $task['subtype'] == 'tech') {

                                    $techMinutes += $this->explode_min($task['task_duration']);
                                    $techHours += $this->explode_hrs($task['task_duration']);
                                } else if ($task['type'] == 'project' && $task['subtype'] == 'video') {

                                    $videoMinutes += $this->explode_min($task['task_duration']);
                                    $videoHours += $this->explode_hrs($task['task_duration']);
                                } else if ($task['type'] == 'project' && $task['subtype'] == 'mediabuying') {

                                    $mediaMinutes += $this->explode_min($task['task_duration']);
                                    $mediaHours += $this->explode_hrs($task['task_duration']);
                                } else if ($task['type'] == 'project' && $task['subtype'] == 'strategy') {

                                    $stratMinutes += $this->explode_min($task['task_duration']);
                                    $stratHours += $this->explode_hrs($task['task_duration']);
                                } else if ($task['type'] == 'retainer' && $task['subtype'] == 'retainer') {

                                    $retMinutes += $this->explode_min($task['task_duration']);
                                    $retHours += $this->explode_hrs($task['task_duration']);
                                }
                            }

                            $totalHours = round($this->calculateHrs($minutes, $hours));

                            if ($totalEmpHours != 0) {
                                $calculatedTime = round(($corpTotal * $totalHours / ($totalEmpHours - $corpTotal)) + $totalHours, 2);
                                $finalTaskTime = round(($calculatedTime / $totalEmpHours * $workingHours), 2);

                            } else {
                                $finalTaskTime = $totalHours;
                            }
                            // echo "calculated   $calculatedTime <br/>";
                            // echo "Final   $finalTaskTime <br/>";

                            // exit;

                            //Calculate Total hours for each code category
                            $projectTechTotal = round((($corpTotal * round($this->calculateHrs($techMinutes, $techHours)) / ($totalEmpHours - $corpTotal) ) + round($this->calculateHrs($techMinutes, $techHours))), 2);
                            $projectTechFinalTotal = round(($projectTechTotal / $totalEmpHours * $workingHours), 2);

                            $projectMedTotal = round((($corpTotal * round($this->calculateHrs($mediaMinutes, $mediaHours)) / ($totalEmpHours - $corpTotal) ) + round($this->calculateHrs($mediaMinutes, $mediaHours))), 2);
                            $projectMedFinalTotal = round(($projectMedTotal / $totalEmpHours * $workingHours), 2);

                            $projectVidTotal = round((($corpTotal * round($this->calculateHrs($videoMinutes, $videoHours)) / ($totalEmpHours - $corpTotal) ) + round($this->calculateHrs($videoMinutes, $videoHours))), 2);
                            $projectVidFinalTotal = round(($projectVidTotal / $totalEmpHours * $workingHours), 2);

                            $projectStratTotal = round((($corpTotal * round($this->calculateHrs($stratMinutes, $stratHours)) / ($totalEmpHours - $corpTotal) ) + round($this->calculateHrs($stratMinutes, $stratHours))), 2);
                            $projectStratFinalTotal = round(($projectStratTotal / $totalEmpHours * $workingHours), 2);

                            $retRetTotal = round((($corpTotal * round($this->calculateHrs($retMinutes, $retHours)) / ($totalEmpHours - $corpTotal) ) + round($this->calculateHrs($retMinutes, $retHours))), 2);
                            $retRetFinalTotal = round(($retRetTotal / $totalEmpHours * $workingHours), 2);

                            //calculate totals in percentage
                            if($percentageFlag == 1){
                                $finalTaskTime = round(($finalTaskTime / $workingHours * 100), 2);
                            }
                        }
                        
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $finalTaskTime);

                        $col++;
                    }
                    //calculate totals in percentage
                    if($percentageFlag == 1){

                        $projectTechFinalTotal = round(($projectTechFinalTotal / $workingHours * 100), 2);
                        $projectMedFinalTotal = round(($projectMedFinalTotal / $workingHours * 100), 2);
                        $projectVidFinalTotal = round(($projectVidFinalTotal / $workingHours * 100), 2);
                        $projectStratFinalTotal = round(($projectStratFinalTotal / $workingHours * 100), 2);
                        $retRetFinalTotal = round(($retRetFinalTotal / $workingHours * 100), 2);
                        $suppTotalTime = round(($workingHours / $workingHours * 100));
                        $mgmtTotalTime = round(($workingHours / $workingHours * 100));
                    }


                    $totalTime = round($projectTechFinalTotal + $projectMedFinalTotal + $projectVidFinalTotal + 
                        $projectStratFinalTotal + $retRetFinalTotal);

                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $projectVidFinalTotal);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 1, $row, $projectTechFinalTotal);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 2, $row, $projectMedFinalTotal);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 3, $row, $projectStratFinalTotal);
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 4, $row, $retRetFinalTotal);


                    if ($emp['dept_name'] == 'Supp') {
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 5, $row, $suppTotalTime);
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 6, $row, 0);
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 7, $row, "$suppTotalTime");
                    } else if ($emp['dept_name'] == 'Mgmt') {
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 5, $row, 0);
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 6, $row, $mgmtTotalTime);
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 7, $row, "$mgmtTotalTime");
                    } else {
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 5, $row, 0);
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 6, $row, 0);
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col + 7, $row, $totalTime);
                    }

                    $firstTotalColumn = PHPExcel_Cell::stringFromColumnIndex($col);
                    $lastTotalColumn = PHPExcel_Cell::stringFromColumnIndex($col + 7);

                    $this->excel->getActiveSheet()->getStyle("$firstTotalColumn$row:$lastTotalColumn$row")->getFill()->applyFromArray(array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'startcolor' => array(
                            'rgb' => 'FAC090'
                        )
                    ));
                    //Set Background color for header columns
                    $this->excel->getActiveSheet()->getStyle($firstTotalColumn . "1:" . $lastTotalColumn . "1")->getFill()->applyFromArray(array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'startcolor' => array(
                            'rgb' => 'FAC090'
                        )
                    ));

                    $row++;
                }
                // if($percentageFlag == 1){

                //     $this->excel->getActiveSheet()->getStyle("C1:$lastTotalColumn$row")->getNumberFormat()->applyFromArray( 
                //         array( 
                //             'code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE
                //         )
                //     );
                // }
                $percentText = (($percentageFlag == 1) ? "IN_Percentage_" : "IN_Hours_");
                $filename = 'ReportWithoutCorpBench_' . $percentText . "$startDate - $endDate" . '.xlsx'; //save our workbook as this file name
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); //mime type
                header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache

                $objWriter = new PHPExcel_Writer_Excel2007($this->excel);
                $objWriter->setOffice2003Compatibility(true);
                $objWriter->save('php://output');
                exit;
            }
        } else {
            redirect("login");
        }
    }

    function getTotalWorkingHrs($startDate, $endDate) {
        $holidayCounts = $this->reports_model->getTotalWorkingDays($startDate, $endDate);

        $start = strtotime($startDate);
        $end = strtotime($endDate . '+1 day');

        $datediff = $end - $start;
        $totalDays = floor($datediff / (60 * 60 * 24));

        $days = $totalDays;

        for ($i = $startDate; $i <= $endDate; $i++) {
            $curr = date("D", strtotime($i));

            // substract if Saturday or Sunday
            if ($curr == 'Sat' || $curr == 'Sun') {
                $days--;
            }
        }
        $days -= $holidayCounts;

        $workinghrs = 8 * $days;

        return $workinghrs;
    }

    function calculateHrs($totalMinutes, $totalHrs) {
        $mins = $this->minutesToStr($totalMinutes);

        $minArr = explode("-", $mins);
        $hrs = $totalHrs + $minArr[0];
        $finalHrs = (($minArr[1] + ($hrs * 60)) / 60);

        return $finalHrs;
    }

    function explode_min($time) { //explode time and convert into minutes
        $time = explode(':', $time);
        $time = ($time[1]);
        return $time;
    }

    function explode_hrs($time) { //explode time and convert into hours
        $time = explode(':', $time);
        $time = ($time[0]);
        return $time;
    }

    function minutesToStr($minutes) { //convert minutes to hours-minutes format
        $hours = floor($minutes / 60);
        $minutes = floor($minutes % 60);

        return $hours . '-' . $minutes;
    }

    function updateCode(){
        $codeData = $this->code_model->getCodeData();

        foreach ($codeData as $key => $value) {

            $product_code = ucwords(str_replace(" ", "", $value['client_name'])) . "-". ucwords($value['location_name']) . "-". ucwords(substr($value['type'], 0, 4)) . "-". ucwords($value['subtype']) . "-". ucwords(str_replace(" ", "_", $value['suffix']));

            $code['product_code'] = $product_code;

            $this->reports_model->updateCode($code, $value['id']);
        }
        exit;
        echo "<pre>";
        print_r($codeData);exit;
    }

}

/* End of file reports.php */
/* Location: ./application/controllers/reports.php */