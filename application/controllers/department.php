<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Department extends CI_Controller {
    /*
     * function name :__construct
     *  Counstructor for Department controller 
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('datatables');
        $this->load->library('form_validation');
        $this->load->model('dept_model');
    }

    /*
     * function name :index
     *  Shows the Department listing page.
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    public function index() {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Department"
            );

            // $data['dept_results'] = $this->dept_model->getDeptData();

            $this->load->view('department/deptList', $data);
        } else {
            redirect("login");
        }
    }

    public function datatable() {

        $column = '<a class="" href="'. base_url("department/edit/$1") .'"><i class="fa fa-edit fa-lg"></i></a>&nbsp;&nbsp;
         <a data-href="'. base_url("department/delete/$1") .'" data-toggle="modal" data-target="#deleteModal" href="#"><i class="fa fa-trash fa-lg"></i></a>';

        $this->datatables->select("id, dept_name, added_date, case status  when '1' then 'Active'  when '0' then 'Inactive' end as status", FALSE)
                ->from('dept_data')
                ->edit_column('id', '<input type="checkbox"  name="check_clients[]" value="$1"  class="multi_check" />', 'id')
                ->add_column('edit', "$column", 'id');

                echo $this->datatables->generate();
    }

    /*
     * function name : getDeptDesignations
     * 
     * Get Department Designations.
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : -
     * @return : json $data
     */

    public function getDeptDesignations() {

        if ($this->session->userdata('userid')) {

            $deptId = $this->input->post("deptId");

            $data = $this->dept_model->getDeptDesignations($deptId);

            echo json_encode($data);
            exit;
        } else {
            redirect("login");
        }
    }

    /*
     * function name :add
     *  Add Department Details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    public function add() {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Add Department"
            );

            if ($_POST) {

                $this->form_validation->set_rules('txtDeptName', 'Department Name', 'required');
                $this->form_validation->set_rules('optDeptStatus', 'Status', 'required');

                if ($this->form_validation->run() == true) {

                    $insertData = array(
                        "dept_name" => $this->input->post("txtDeptName"),
                        "status" => $this->input->post("optDeptStatus"),
                        "added_date" => date("Y-m-d H:i:s")
                    );
                    $result = $this->dept_model->addDepartment($insertData);

                    if ($result) {
                        $this->session->set_flashdata("success_msg", "Department Saved Successfully.");
                        redirect("department");
                    } else {
//                        $this->session->set_flashdata("login_msg", "Invalid Username/Password!!!");
                        redirect("department/add");
                    }
                }
            }

            $this->load->view('department/addDept', $data);
        } else {
            redirect("login");
        }
    }

    /*
     * function name : edit
     *  Update Department details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : Int $deptId
     * @return : void
     */

    public function edit($deptId = NULL) {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Edit Department"
            );

            if ($_POST) {

                $this->form_validation->set_rules('txtDeptName', 'Client Name', 'required');
                $this->form_validation->set_rules('optDeptStatus', 'Status', 'required');

                if ($this->form_validation->run() == true) {


                    $deptId = $this->input->post("txtDeptId");

                    $updateData = array(
                        "dept_name" => $this->input->post("txtDeptName"),
                        "status" => $this->input->post("optDeptStatus")
                    );
                    $result = $this->dept_model->editDepartment($updateData, $deptId);

                    if ($result) {
                        $this->session->set_flashdata("success_msg", "Department Updated Successfully.");
                        redirect("department");
                    } else {
                        redirect("department/edit/$deptId");
                    }
                }
            }

            $data['dept'] = $this->dept_model->getDeptDetails($deptId);

            $this->load->view('department/editDept', $data);
        } else {
            redirect("login");
        }
    }

    /*
     * function name : delete
     *  Delete Department details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : Int $deptId
     * @return : void
     */

    public function delete($deptId = NULL) {

        if ($this->session->userdata('userid')) {

            if ($deptId) {

                $this->dept_model->deleteDepartment($deptId);
            } else if ($_POST) {

                $deptIds = $this->input->post("check_depts");

                foreach ($deptIds as $key => $value) {
                    $this->dept_model->deleteDepartment($value);
                }
            }
            $this->session->set_flashdata("success_msg", "Department Deleted Successfully.");
            redirect("department");
        } else {
            redirect("login");
        }
    }

}

/* End of file department.php */
/* Location: ./application/controllers/department.php */