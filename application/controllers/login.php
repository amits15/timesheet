<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {
    /*
     * function name :__construct
     *  Counstructor for Login controller 
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('login_model');
        $this->load->helper('cookie');
    }

    public function index() {
        $data = array(
            "title" => "Login"
        );
        if ($this->session->userdata('userid')) {
            redirect('dashboard');
        } else {
            $this->load->view('login/login', $data);
        }
    }

    /*
     * function name : loginProcess
     * 
     * Process the login request submitted by user.
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    public function loginProcess() {


        if ($_POST) {
// echo "test";
            //validate form input
            $this->form_validation->set_rules('identity', 'Email', 'required|email');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == true) {


                $result = $this->login_model->loginProcess($_POST);

                if ($result) {
                    $remember = $this->input->post("glitch_remember");
                    if ($remember) {
//                        echo "cookie";exit;
                        $cookie = array(
                            'name' => 'remember_me_token',
                            'value' => '1',
                            'expire' => '1209600' // Two weeks
                        );

                        set_cookie($cookie);
                        
                        $emailCookie = array(
                            'name' => 'remember_name',
                            'value' => $this->input->post('identity'),
                            'expire' => '1209600' // Two weeks
                        );
                        set_cookie($emailCookie);
                        
                        $pwdCookie = array(
                            'name' => 'remember_key',
                            'value' => $this->input->post('password'),
                            'expire' => '1209600' // Two weeks
                        );
                        set_cookie($pwdCookie);
                        
                    }else{
                        $cookie = array(
                            'name' => 'remember_me_token',
                            'value' => '',
                            'expire' => '-1' // Two weeks
                        );

                        set_cookie($cookie);
                        
                        $emailCookie = array(
                            'name' => 'remember_name',
                            'value' => '',
                            'expire' => '-1' // Two weeks
                        );
                        set_cookie($emailCookie);
                        
                        $pwdCookie = array(
                            'name' => 'remember_key',
                            'value' => '',
                            'expire' => '-1' // Two weeks
                        );
                        set_cookie($pwdCookie);
                    }
                    redirect("dashboard");
                } else {
                    $this->session->set_flashdata("login_msg", "Invalid Username/Password!!!");
                    redirect("login");
                }
            }
            $this->load->view('login/login');
        }
    }

    public function logout() {
        $data = array(
            "userid" => '',
            "email_id" => '',
            "first_name" => '',
            "last_name" => '',
            "employee_type" => '',
            "designation" => '',
            "department" => '',
            "profile_pic" => ''
        );

        $this->session->unset_userdata($data);        
        $this->session->sess_destroy();
        redirect("login");
    }

}

/* End of file login.php */
/* Location: ./application/controllers/login.php */