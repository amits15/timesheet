// Client Add and Update Form validation
$("#frmLogin").validate({
    errorClass: "form-error",
    errorElement: "span",
    rules: {
        identity: {
            required: true,
            email: true
        },
        password: {
            required: true
        }
    },
    messages: {
        identity: {
            required: "Username/Email is required!!"
        },
        password: {
            required: "Password is required!!"
        }
    }
});


// Client Add and Update Form validation
$("#frmClient").validate({
    errorClass: "form-error",
    errorElement: "span",
    rules: {
        txtClientName: {
            required: true
        },
        optClientStatus: {
            required: true
        }
    },
    messages: {
        txtClientName: {
            required: "Client Name is Required!!"
        }
    }
});

// Location Add and Update Form validation
$("#frmLocation").validate({
    errorClass: "form-error",
    errorElement: "span",
    rules: {
        txtLocationName: {
            required: true
        },
        optLocationStatus: {
            required: true
        }
    },
    messages: {
        txtLocationName: {
            required: "Location Name is Required!!"
        }
    }
});

$.validator.setDefaults({ignore: ":hidden:not(select)"});
// Code Add and Update Form validation
$("#frmCode").validate({
    errorClass: "form-error",
    errorElement: "span",
    rules: {
        txtJobCode:{
            required: true,
            remote: {
                url: "checkJobCode",
                type: "post"
            }
        },
        optCodeClientName: {
            required: true
        },
        optCodeClientType: {
            required: true
        },
        optCodeClientSubType: {
            required: true
        },
        txtCodeSuffix: {
            required: true
        },
        optCodeLocation: {
            required: true
        },
        txtProductCode:{
            remote: {
                url: "checkProductCode",
                type: "post"
            }
        }

    },
    messages: {
        txtJobCode:{
            required: "Job Code is Required!!",
            remote: "Job Code already exsist"
        },
        optCodeClientName: {
            required: "Client Name is Required!!"
        },
        optCodeClientType: {
            required: "Type is Required!!"
        },
        optCodeClientSubType: {
            required: "Sub Type is Required!!"
        },
        txtCodeSuffix: {
            required: "Suffix is Required!!"
        },
        optCodeLocation: {
            required: "Location is Required!!"
        },
        txtProductCode:{
            remote: "Product code already exsist"
        }
    },
    errorPlacement: function (error, element) {
        if (element.attr("name") == "optCodeClientName") {
            error.insertAfter($("#optCodeClientName_chosen"));
        } else {
            error.insertAfter(element);
        }
    }
});

$("#frmEditCode").validate({
    errorClass: "form-error",
    errorElement: "span",
    rules: {
        txtJobCode:{
            required: true,
            remote: {
                url: baseURL + "code/checkJobCode",
                type: "post",
                data: {
                    codeId: function(){
                        return $("#txtCodeId").val();
                    }
                }
            }
        },
        optCodeClientName: {
            required: true
        },
        optCodeClientType: {
            required: true
        },
        optCodeClientSubType: {
            required: true
        },
        txtCodeSuffix: {
            required: true
        },
        optCodeLocation: {
            required: true
        }

    },
    messages: {
        txtJobCode:{
            required: "Job Code is Required!!",
            remote: "Job Code already exsist for Other code!!"
        },
        optCodeClientName: {
            required: "Client Name is Required!!"
        },
        optCodeClientType: {
            required: "Type is Required!!"
        },
        optCodeClientSubType: {
            required: "Sub Type is Required!!"
        },
        txtCodeSuffix: {
            required: "Suffix is Required!!"
        },
        optCodeLocation: {
            required: "Location is Required!!"
        }
    },
    errorPlacement: function (error, element) {
        if (element.attr("name") == "optCodeClientName") {
            error.insertAfter($("#optCodeClientName_chosen"));
        } else {
            error.insertAfter(element);
        }
    }
});

// Employee Add and Update Form validation
$("#frmEmployee").validate({
    errorClass: "form-error",
    errorElement: "span",
    rules: {
        txtEmpCode:{
            required: true,
            remote: {
                url: "checkEmpCode",
                type: "post"
            }
        },
        txtEmpFirstName: {
            required: true
        },
        txtEmpLastName: {
            required: true
        },
        txtEmpEmail: {
            required: true,
            email: true
        },
        txtEmpPassword: {
            required: true,
            minlength: 4
        },
        rdoEmpGender: {
            required: true
        },
        /*txtEmpDOB: {
            required: {
                depends: function(element) {
                    return $('#chkEmpStatus').is(':checked');
                }
            },
        },
        txtEmpPhone: {
            required: {
                depends: function(element) {
                    return $('#chkEmpStatus').is(':checked');
                }
            },            
            digits: true,
            maxlength: 10
        },*/
        optEmpDept: {
            required: true
        },
        txtEmpDesignation: {
            required: true
        },
        txtEmpSalary: {
            required: true
        },
        optEmpLocation: {
            required: true
        },
        optEmpType: {
            required: true
        },
        txtEmpDOJ: {
            required: true
        },
        txtEmpLWD: {
            required: {
                depends: function(element) {
                    return !$('#chkEmpStatus').is(':checked');
                }
            },
        }
    },
    messages: {
        txtEmpCode:{
            required: "Emp code is Required!!",
            remote: "Emp code already exsist"
        },
        txtEmpFirstName: {
            required: "First Name is Required!!"
        },
        txtEmpLastName: {
            required: "Last Name is Required!!"
        },
        txtEmpEmail: {
            required: "Email is Required!!"
        },
        txtEmpPassword: {
            required: "Password is Required!!",
            minlength: "Password must be of at least 4 characters!!"
        },
        rdoEmpGender: {
            required: "Gender is Required!!"
        },
        /*txtEmpDOB: {
            required: "DOB is Required!!"
        },
        txtEmpPhone: {
            required: "Phone number is Required!!",
            
            maxlength: "Phone must be of 10 Digits only!!"
        },*/
        optEmpDept: {
            required: "Department is Required!!"
        },
        txtEmpDesignation: {
            required: "Designation is Required!!"
        },
        txtEmpSalary: {
            required: "Salary is Required!!"
        },
        optEmpLocation: {
            required: "Location is Required!!"
        },
        optEmpType: {
            required: "Employee Type is Required!!"
        },
        txtEmpDOJ: {
            required: "Employee DOJ is Required!!"
        },
        txtEmpLWD: {
            required: "Employee LWD is Required!!"
        }
    },
    errorPlacement: function (error, element) {
        if (element.attr("name") == "rdoEmpGender") {
            error.insertAfter($("#gender_div"));
        } else if (element.attr("name") == "txtEmpDOB") {
            error.insertAfter($(".empDob"));
        } else if (element.attr("name") == "txtEmpDOJ") {
            error.insertAfter($(".empDOJ"));
        } else if (element.attr("name") == "txtEmpLWD") {
            error.insertAfter($(".empLWD"));
        } else {
            error.insertAfter(element);
        }
    }
});

$("#frmEditEmployee").validate({
    errorClass: "form-error",
    errorElement: "span",
    rules: {
        txtEmpCode:{
            required: true,
            remote: {
                url: baseURL + "employee/checkEmpCode",
                type: "post",
                data: {
                    empId: function(){
                        return $("#txtEmpId").val();
                    }
                }
            }
        },
        txtEmpFirstName: {
            required: true
        },
        txtEmpLastName: {
            required: true
        },
        txtEmpEmail: {
            required: true,
            email: true
        },
        rdoEmpGender: {
            required: true
        },
        /*txtEmpDOB: {
            required: {
                depends: function(element) {
                    return $('#chkEmpStatus').is(':checked');
                }
            },
        },
        txtEmpPhone: {
            required: {
                depends: function(element) {
                    return $('#chkEmpStatus').is(':checked');
                }
            },            
            digits: true,
            maxlength: 10
        },*/
        optEmpDept: {
            required: true
        },
        txtEmpDesignation: {
            required: true
        },
        txtEmpSalary: {
            required: true
        },
        optEmpLocation: {
            required: true
        },
        optEmpType: {
            required: true
        },
        txtEmpDOJ: {
            required: true
        },
        txtEmpLWD: {
            required: {
                depends: function(element) {
                    return !$('#chkEmpStatus').is(':checked');
                }
            },
        }
    },
    messages: {
        txtEmpCode:{
            required: "Emp code is Required!!",
            remote: "Emp code already exsist"
        },
        txtEmpFirstName: {
            required: "First Name is Required!!"
        },
        txtEmpLastName: {
            required: "Last Name is Required!!"
        },
        txtEmpEmail: {
            required: "Email is Required!!"
        },
        rdoEmpGender: {
            required: "Gender is Required!!"
        },
        /*txtEmpDOB: {
            required: "DOB is Required!!"
        },
        txtEmpPhone: {
            required: "Phone number is Required!!",
            number: "Phone must contain only numbers!!",
            maxlength: "Phone must be of 10 Digits only!!"
        },*/
        optEmpDept: {
            required: "Department is Required!!"
        },
        txtEmpDesignation: {
            required: "Designation is Required!!"
        },
        txtEmpSalary: {
            required: "Salary is Required!!"
        },
        optEmpLocation: {
            required: "Location is Required!!"
        },
        optEmpType: {
            required: "Employee Type is Required!!"
        },
        txtEmpDOJ: {
            required: "Employee DOJ is Required!!"
        },
        txtEmpLWD: {
            required: "Employee LWD is Required!!"
        }
    },
    errorPlacement: function (error, element) {
        if (element.attr("name") == "rdoEmpGender") {
            error.insertAfter($("#gender_div"));
        } else if (element.attr("name") == "txtEmpDOB") {
            error.insertAfter($(".empDob"));
        } else if (element.attr("name") == "txtEmpDOJ") {
            error.insertAfter($(".empDOJ"));
        } else if (element.attr("name") == "txtEmpLWD") {
            error.insertAfter($(".empLWD"));
        } else {
            error.insertAfter(element);
        }
    }
});


// User profile Change password Form validation
$("#frmChangePassword").validate({
    errorClass: "form-error",
    errorElement: "span",
    rules: {
        txtUserOldPwd: {
            required: true,
            remote: {
                url: baseURL + "profile/checkOldPwd",
                type: "post",
                data: {
                    userId: function(){
                        return $("#txtUserId").val();
                    }
                }
            }
        },
        txtUserNewPwd: {
            required: true
        },
        txtUserConfirmPwd: {
            required: true,
            equalTo: "#txtUserNewPwd"
        }
    },
    messages: {
        txtUserOldPwd: {
            required: "Old Password is Required!!",
            remote: "Password not matched with current password!!"
        },
        txtUserNewPwd: {
            required: "New Password is Required!!"
        },
        txtUserConfirmPwd: {
            required: "Confirm Password is Required!!",
            equalTo: "Confirm password must be same as new password!!"
        }
    }
});



// User edit profile Form validation
$("#frmUserProfile").validate({
    errorClass: "form-error",
    errorElement: "span",
    rules: {
        txtUserPhone: {
            digits: true,
            maxlength: 10
        }
    },
    messages: {
        txtUserPhone: {
            maxlength: "Phone must be of 10 Digits only!!"
        }
    }
});


// User edit profile Form validation
$("#frmUserReports").validate({
    errorClass: "form-error",
    errorElement: "span",
    rules: {
        txtReportDateRange: {
            required: true
        }
    },
    messages: {
        txtReportDateRange: {
            required: "Please Select Date!!"
        }
    },
    errorPlacement: function (error, element) {
        if (element.attr("name") == "txtReportDateRange") {
            error.insertAfter($(".taskDate"));
        } else {
            error.insertAfter(element);
        }
    }
});

var baseURL = "";


$("#frmEditUserTimesheet").validate({
    errorClass: "form-error",
    errorElement: "span",
    rules: {
        optEditTimeSheetClient: {
            required: true
        },
        optEditTimeSheetCode: {
            required: true
        },
        optEditTimeSheetActivity: {
            required: true
        },
        txtEditTimeSheetDate: {
            required: true
        },
        optEditTimeSheetTime: {
            required: true
        }
    },
    messages: {
        optEditTimeSheetClient: {
            required: "Client Name is required!!"
        },
        optEditTimeSheetCode: {
            required: "Code is required!!"
        },
        optEditTimeSheetActivity: {
            required: "Activity is required!!"
        },
        txtEditTimeSheetDate: {
            required: "Date is required!!"
        },
        optEditTimeSheetTime: {
            required: "Time Duration is required!!"
        }
    },
    errorPlacement: function (error, element) {
        if (element.attr("name") == "optEditTimeSheetClient") {
            error.insertAfter($("#optEditTimeSheetClient_chosen"));
        } else if (element.attr("name") == "optEditTimeSheetCode") {
            error.insertAfter($("#optEditTimeSheetCode_chosen"));
        } else if (element.attr("name") == "optEditTimeSheetActivity") {
            error.insertAfter($("#optEditTimeSheetActivity_chosen"));
        } else if (element.attr("name") == "txtEditTimeSheetDate") {
            error.insertAfter($(".taskEditDate"));
        } else if (element.attr("name") == "optEditTimeSheetTime") {
            error.insertAfter($("#optEditTimeSheetTime_chosen"));
        } else {
            error.insertAfter(element);
        }
    }
});

$("#frmDept").validate({
    errorClass: "form-error",
    errorElement: "span",
    rules: {
        txtDeptName: {
            required: true
        },
        optDeptStatus: {
            required: true
        }
    },
    messages: {
        txtDeptName: {
            required: "Department Name is required!!"
        },
        optDeptStatus: {
            required: "Status is required!!"
        }
    }
});

$("#frmDesig").validate({
    errorClass: "form-error",
    errorElement: "span",
    rules: {
        optDesigDept: {
            required: true
        },
        txtDesigName: {
            required: true
        },
        optDesigStatus: {
            required: true
        }
    },
    messages: {
        optDesigDept: {
            required: "Department is required!!"
        },
        txtDesigName: {
            required: "Designation is required!!"
        },
        optDesigStatus: {
            required: "Status is required!!"
        }
    }
});

$("#frmHolidays").validate({
    errorClass: "form-error",
    errorElement: "span",
    rules: {
        txtHolidayDate: {
            required: true
        },
        txtHolidayDesc: {
            required: true
        }
    },
    messages: {
        txtHolidayDate: {
            required: "Holiday Date is required!!"
        },
        txtHolidayDesc: {
            required: "Description is required!!"
        }
    },
    errorPlacement: function (error, element) {
        if (element.attr("name") == "txtHolidayDate") {
            error.insertAfter($(".holidayDate"));
        } else {
            error.insertAfter(element);
        }
    }
});

