<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Client extends CI_Controller {
    /*
     * function name :__construct
     *  Counstructor for Client controller 
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('datatables');
        $this->load->library('form_validation');
        $this->load->model('client_model');
    }

    /*
     * function name :index
     *  Shows the client listing page.
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    public function index() {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Client"
            );

            // $data['client_results'] = $this->client_model->getClientData();

            $this->load->view('client/clientList', $data);
        } else {
            redirect("login");
        }
    }

    public function datatable() {

        $column = '<a class="" href="'. base_url("client/edit/$1") .'"><i class="fa fa-edit fa-lg"></i></a>&nbsp;&nbsp;
         <a data-href="'. base_url("client/delete/$1") .'" data-toggle="modal" data-target="#deleteModal" href="#"><i class="fa fa-trash fa-lg"></i></a>';

        $this->datatables->select("id, client_name, created, case status  when '1' then 'Active'  when '0' then 'Inactive' end as status", FALSE)
                ->from('client_data')
                ->edit_column('id', '<input type="checkbox"  name="check_clients[]" value="$1"  class="multi_check" />', 'id')
                ->add_column('edit', "$column", 'id');

                echo $this->datatables->generate();
    }


    /*
     * function name :add
     *  Add Client Details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    public function add() {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Add Client"
            );

            if ($_POST) {

                $this->form_validation->set_rules('txtClientName', 'Client Name', 'required');
                $this->form_validation->set_rules('optClientStatus', 'Status', 'required');

                if ($this->form_validation->run() == true) {

                    $insertData = array(
                        "client_name" => $this->input->post("txtClientName"),
                        "status" => $this->input->post("optClientStatus"),
                        "created" => date("Y-m-d H:i:s")
                    );
                    $result = $this->client_model->addClient($insertData);

                    if ($result) {
                        redirect("client");
                    } else {
//                        $this->session->set_flashdata("login_msg", "Invalid Username/Password!!!");
                        redirect("client/add");
                    }
                }
            }

            $this->load->view('client/addClient', $data);
        } else {
            redirect("login");
        }
    }

    /*
     * function name : edit
     *  Update client details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : Int $clientId
     * @return : void
     */

    public function edit($clientId = NULL) {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Edit Client"
            );

            if ($_POST) {

                $this->form_validation->set_rules('txtClientName', 'Client Name', 'required');
                $this->form_validation->set_rules('optClientStatus', 'Status', 'required');

                if ($this->form_validation->run() == true) {


                    $clientId = $this->input->post("txtClientId");
//                    echo "$clientId";exit;

                    $updateData = array(
                        "client_name" => $this->input->post("txtClientName"),
                        "status" => $this->input->post("optClientStatus")
                    );
                    $result = $this->client_model->editClient($updateData, $clientId);

                    if ($result) {
                        redirect("client");
                    } else {
                        redirect("client/edit/$clientId");
                    }
                }
            }

            $data['client'] = $this->client_model->getClientDetails($clientId);
//            print_r($data);exit;

            $this->load->view('client/editClient', $data);
        } else {
            redirect("login");
        }
    }

    /*
     * function name : delete
     *  Delete client details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : Int $clientId
     * @return : void
     */

    public function delete($clientId = NULL) {

        if ($this->session->userdata('userid')) {

            if ($clientId) {

                $this->client_model->deleteClient($clientId);
            } else if ($_POST) {

                $clientIds = $this->input->post("check_clients");

                foreach ($clientIds as $key => $value) {
                    $this->client_model->deleteClient($value);
                }
            }
            redirect("client");
        } else {
            redirect("login");
        }
    }

}

/* End of file client.php */
/* Location: ./application/controllers/client.php */