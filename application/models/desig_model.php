<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Desig_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /*
     * function name : getDesignationData
     * 
     * Fetch Designation details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : none
     * @return : Array $data
     */

    public function getDesignationData() {

        $this->db->select("ds.*, dp.dept_name");
        $this->db->join('dept_data As dp', 'dp.id=ds.dept_id', 'LEFT');
        $this->db->order_by('desig_name');
        $query = $this->db->get('designation_data As ds');

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    /*
     * function name : getActiveDeptData
     * 
     * Fetch Active Department details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : none
     * @return : Array $data
     */

    public function getActiveDeptData() {

        $this->db->where("status", "1");
        $this->db->order_by('dept_name');
        $query = $this->db->get('designation_data');

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    /*
     * function name : designation_data
     * 
     * Fetch particular Designation details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : int $desigId
     * @return : Array $data
     */

    public function getDesigDetails($desigId) {

        $this->db->where("id", $desigId);
        $query = $this->db->get('designation_data');

        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    /*
     * function name : addDesignation
     * 
     * Save Designation details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : array $insertData
     * @return : bool true|false
     */

    public function addDesignation($insertData) {

        $this->db->insert('designation_data', $insertData);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * function name : editDesignation
     * 
     * update Designation details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : array $updateData, int $desigId
     * @return : bool true|false
     */

    public function editDesignation($updateData, $desigId) {


        $this->db->update('designation_data', $updateData, array("id" => $desigId));

        return true;
    }
    
    
    /*
     * function name : deleteDesignation
     * 
     * Delete Designation details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : int $desigId
     * @return : bool true|false
     */

    public function deleteDesignation($desigId) {

        $this->db->delete('designation_data', array("id" => $desigId));
        return true;
    }

}
