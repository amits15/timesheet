$(document).ready(function () {
    $(".grp_check").change(function () {
        $(".multi_check").prop('checked', $(this).prop("checked"));
    });
    $(".multi_check").change(function () {
        if ($(".multi_check:not(:checked)").length == 0) {
            $(".grp_check").prop('checked', true);
        } else {
            $(".grp_check").prop('checked', '');
        }

    });
    
    //Bootstrap delete modal href Action. Redirecting to specified href on modal Yes click.
    $('#deleteModal').on('show.bs.modal', function (e) {
        $(this).find('.btn-danger').attr('href', $(e.relatedTarget).data('href'));
    });

    $("#multiDelete").click(function () {
        var i = 0;
        $(".multi_check").each(function () {
            if ($(this).is(":checked")) {
                i++;
            }
        });

        if (i == 0) {
            alert("Please select records to delete.");
            return false;
        } else {
            $("#multiDelete").attr("data-href", "#multiDeleteModal");
            $('#multiDeleteModal').modal('show');
        }

    });
});