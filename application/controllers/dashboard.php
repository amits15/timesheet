<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    /*
     * function name :__construct
     *  Counstructor for Dashboard controller 
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('code_model');
        $this->load->model('client_model');
        $this->load->model('dashboard_model');
    }

    /*
     * function name : index
     * 
     * Show dashboard page with data
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : -
     * @return : array $data
     */


    public function index() {

        if ($this->session->userdata('userid')) {

            $data = array(
                "title" => "Dashboard"
            );

            $userId = $this->session->userdata('userid');

//            $data['tasks'] = $this->client_model->getUserTasks($userId);
            $data['clients'] = $this->client_model->getActiveClientData();
            $data['activities'] = $this->dashboard_model->getActivityData();
            $data['quickListData'] = $this->dashboard_model->getQuickListData($userId);
            $this->load->view('dashboard', $data);
        } else {
            redirect("login");
        }
    }
    
    /*
     * function name : getHolidays
     * 
     * Get holidays List
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : -
     * @return : json $data
     */

    public function getHolidays() {
        $holidays = $this->dashboard_model->getHolidays();

        $dates = array();
        if($holidays){
            foreach ($holidays as $holiday) {
                $dates[] = $holiday['holiday_date'];
            }
        }
        echo json_encode($dates);
        exit;
    }


    /*
     * function name : getUserTasks
     * 
     * Get User tasks between Start and end dates
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : -
     * @return : json $data
     */

    public function getUserTasks() {

        if ($this->session->userdata('userid')) {

            $userId = $this->session->userdata('userid');

            $startDate = $this->input->post("start");
            $endDate = $this->input->post("end");

            $data = $this->dashboard_model->getUserTasks($userId, $startDate, $endDate);


            $timesheetData = array();
            if ($data) {
                foreach ($data as $key => $value) {
                    $start = date('Y-m-d H:i:s', strtotime($value['task_date'] . " " . $value['task_start_time']));
                    $end = date('Y-m-d H:i:s', strtotime($value['task_date'] . " " . $value['task_end_time']));

                    //Adding data into array
                    $timesheetData[] = array(
                        "allDay" => false,
                        "id" => $value['taskId'],
                        "title" => $value['pCode'],
                        "start" => $start,
                        "end" => $end,
                        "body" => $value['description'],
                        "backgroundColor" => $value['background_color']
                    );
                }
            }

            echo json_encode($timesheetData);
            exit;
        } else {
            redirect("login");
        }
    }


    /*
     * function name : getEditTaskDetails
     * 
     * Get User task details for Edit task
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : -
     * @return : json $data
     */

    public function getEditTaskDetails() {

        if ($this->session->userdata('userid')) {

            $userId = $this->session->userdata('userid');

            $taskId = $this->input->post("taskId");

            $data = array();

            $taskData = $this->dashboard_model->getEditTaskDetails($taskId);

            if ($taskData) {
                $data['codeData'] = $this->dashboard_model->getClientCodes($taskData['clientId']);
            }
            //prepairing timesheet data
            if ($taskData) {
                $data['timesheetData'] = array(
                    "taskId" => $taskData['taskId'],
                    "codeId" => $taskData['codeId'],
                    "code" => $taskData['pCode'],
                    "clientId" => $taskData['clientId'],
                    "client" => $taskData['client_name'],
                    "taskDate" => $taskData['task_date'],
                    "taskStartTime" => $taskData['task_start_time'],
                    "taskEndTime" => $taskData['task_end_time'],
                    "taskDuration" => $taskData['task_duration'],
                    "body" => $taskData['description'],
                    "activityId" => $taskData['activityId'],
                    "activity" => $taskData['activity'],
                    "taskDescription" => $taskData['description'],
                );
            }

            echo json_encode($data);
            exit;
        } else {
            redirect("login");
        }
    }


    /*
     * function name : getClientCodes
     * 
     * Get Client Codes listing.
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : -
     * @return : json $data
     */

    public function getClientCodes() {

        if ($this->session->userdata('userid')) {

            $clientId = $this->input->post("clientId");

            $data = $this->dashboard_model->getClientCodes($clientId);

            echo json_encode($data);
            exit;
        } else {
            redirect("login");
        }
    }


    /*
     * function name : saveTask
     * 
     * Save user Tasks in to db.
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : -
     * @return : void
     */

    public function saveTask() {

        if ($this->session->userdata('userid')) {

            if ($_POST) {

                $this->form_validation->set_rules('optTimeSheetClient', 'Client Name', 'required');
                $this->form_validation->set_rules('optTimeSheetCode', 'Code', 'required');
                $this->form_validation->set_rules('optTimeSheetActivity', 'Activity', 'required');
                $this->form_validation->set_rules('txtTimeSheetDate', 'Date', 'required');
                $this->form_validation->set_rules('optTimeSheetTime', 'Time', 'required');
                // $this->form_validation->set_rules('rtTimeSheetDescription', 'Description', 'required');

                //check validation
                if ($this->form_validation->run() == true) {

                    //prepare insert data
                    $insertData = array(
                        "employee_id" => $this->session->userdata('userid'),
                        "client_id" => $this->input->post("optTimeSheetClient"),
                        "code_id" => $this->input->post("optTimeSheetCode"),
                        "activity_id" => $this->input->post("optTimeSheetActivity"),
                        "description" => $this->input->post("rtTimeSheetDescription"),
                        "task_date" => $this->input->post("txtTimeSheetDate"),
                        "task_duration" => $this->input->post("optTimeSheetTime"),
                        "created_date" => date("Y-m-d")
                    );
                    $taskCodeId = $this->input->post("optTimeSheetCode");

                    $codeDetails = $this->code_model->getCodeDetails($taskCodeId);


                    //assigining background color based on code category.
                    if ($codeDetails['type'] == "project") {
                        $insertData['background_color'] = "#fa5858";
                    } else if ($codeDetails['type'] == "retainer") {
                        $insertData['background_color'] = "#c317b4";
                    } else if ($codeDetails['type'] == "corporate") {
                        $insertData['background_color'] = "#00a7e5";
                    }

                    $TaskArr = $this->dashboard_model->getUserLatestFilledTask($this->input->post("txtTimeSheetDate"), $this->session->userdata('userid'));

                    //User input time calculation in seconds
                    $taskDuration = $this->input->post("optTimeSheetTime");

                    $timeDuration = explode(":", $taskDuration);

                    //Calculating time duration in seconds
                    $taskTimeSeconds = (3600 * $timeDuration[0]) + (60 * $timeDuration[1]);

                    if ($TaskArr) {
                        $timestamp = strtotime($TaskArr['task_end']) + $taskTimeSeconds;

                        $taskStartTime = $TaskArr['task_end'];
                        $taskEndTime = date('H:i', $timestamp);

                        $insertData['task_start_time'] = $taskStartTime;
                        $insertData['task_end_time'] = $taskEndTime;
                    } else {

                        $timestamp = strtotime("10:00:00") + $taskTimeSeconds;

                        $taskStartTime = "10:00:00";
                        $taskEndTime = date('H:i', $timestamp);

                        $insertData['task_start_time'] = $taskStartTime;
                        $insertData['task_end_time'] = $taskEndTime;
                    }


                    $result = $this->dashboard_model->saveTask($insertData);
                    echo $result;
                    exit;
                    // if ($result) {
                    //     redirect("dashboard");
                    // }
                }
            }
        } else {
            redirect("login");
        }
    }


    /*
     * function name : updateTask
     * 
     * Update user Tasks details.
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : -
     * @return : void
     */

    public function updateTask() {

        if ($this->session->userdata('userid')) {

            if ($_POST) {

                $this->form_validation->set_rules('optEditTimeSheetClient', 'Client Name', 'required');
                $this->form_validation->set_rules('optEditTimeSheetCode', 'Code', 'required');
                $this->form_validation->set_rules('optEditTimeSheetActivity', 'Activity', 'required');
                $this->form_validation->set_rules('txtEditTimeSheetDate', 'Date', 'required');
                $this->form_validation->set_rules('optEditTimeSheetTime', 'Time', 'required');
                // $this->form_validation->set_rules('rtEditTimeSheetDescription', 'Description', 'required');

                //Check form validation
                if ($this->form_validation->run() == true) {

                    $taskId = $this->input->post("txtEditTimeSheetId");

                    //prepare update data
                    $updateData = array(
                        "employee_id" => $this->session->userdata('userid'),
                        "client_id" => $this->input->post("optEditTimeSheetClient"),
                        "code_id" => $this->input->post("optEditTimeSheetCode"),
                        "activity_id" => $this->input->post("optEditTimeSheetActivity"),
                        "description" => $this->input->post("rtEditTimeSheetDescription"),
                        "task_date" => $this->input->post("txtEditTimeSheetDate"),
                        "task_duration" => $this->input->post("optEditTimeSheetTime"),
                        "modified_date" => date("Y-m-d")
                    );

                    $taskCodeId = $this->input->post("optEditTimeSheetCode");
                    $codeDetails = $this->code_model->getCodeDetails($taskCodeId);


                    //code for background color of task
                    if ($codeDetails['type'] == "project") {
                        $updateData['background_color'] = "#fa5858";
                    } else if ($codeDetails['type'] == "retainer") {
                        $updateData['background_color'] = "#c317b4";
                    } else if ($codeDetails['type'] == "corporate") {
                        $updateData['background_color'] = "#00a7e5";
                    }


                    //User input time calculation in seconds
                    $taskDuration = $this->input->post("optEditTimeSheetTime");

                    $timeDuration = explode(":", $taskDuration);

                    //Calculating time duration in seconds
                    $taskTimeSeconds = (3600 * $timeDuration[0]) + (60 * $timeDuration[1]);

                    $timestamp = strtotime($this->input->post("txtEditTimeSheetStart")) + $taskTimeSeconds;

                    $taskStartTime = $this->input->post("txtEditTimeSheetStart");
                    $taskEndTime = date('H:i', $timestamp);

                    $updateData['task_start_time'] = $taskStartTime;
                    $updateData['task_end_time'] = $taskEndTime;

                    $result = $this->dashboard_model->updateTask($updateData, $taskId);

                    echo $result;
                    exit;
                }
            }
        } else {
            redirect("login");
        }
    }


    /*
     * function name : updateTaskTiming
     * 
     * Update user Tasks Timings.
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : -
     * @return : void
     */

    public function updateTaskTiming() {

        if ($this->session->userdata('userid')) {

            if ($_POST) {
                
                $taskStartTime = $this->input->post("taskStartTime");
                $taskEndTime = $this->input->post("taskEndTime");
                $taskDate = $this->input->post("taskDate");
                $taskId = $this->input->post("taskId");

                $updateData = array(
                    "task_date" => $taskDate,
                    "task_start_time" => $taskStartTime,
                    "task_end_time" => $taskEndTime,
                    "modified_date" => date("Y-m-d")
                );

                


                //User input time calculation in seconds
                
                $taskDurationSec = strtotime($taskEndTime) - strtotime($taskStartTime);
                
                $minutes = ($taskDurationSec / 60) % 60;
                $hours = floor($taskDurationSec / (60 * 60));

                //append 0 if hours less than 9
                if($hours < 9){
                    $hours = "0$hours";
                }else{
                    $hours = "$hours";
                }

                //append 0 if Minutes less than 9
                if($minutes == 0){
                    $minutes = "0$minutes";
                }
                
                $tasDuration = "$hours:$minutes";
                
                $updateData['task_duration'] = $tasDuration;

                $result = $this->dashboard_model->updateTaskTiming($updateData, $taskId);
                echo $result;
                exit;
            }
        } else {
            redirect("login");
        }
    }

    /*
     * function name : deleteTask
     * 
     * Delete user Tasks details.
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : -
     * @return : void
     */

    public function deleteTask() {

        if ($this->session->userdata('userid')) {

            $userId = $this->session->userdata('userid');

            $taskId = $this->input->post("taskId");

            $data = $this->dashboard_model->deleteTask($taskId);
            echo $data;
            exit;
        } else {
            redirect("login");
        }
    }

    /*
     * function name : addToQuickList
     * 
     * Add code and client to quick list.
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : -
     * @return : void
     */

    public function addToQuickList() {

        if ($this->session->userdata('userid')) {

            $userId = $this->session->userdata('userid');

            $clientId = $this->input->post("clientId");
            $codeId = $this->input->post("codeId");

            $quickListCode = $clientId . "_". $codeId;

            $data = $this->dashboard_model->addToQuickList($userId, $quickListCode);
            echo $data;
            exit;
        } else {
            redirect("login");
        }
    }

    /*
     * function name : saveQuickTask
     * 
     * Save user Tasks in to db.
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : -
     * @return : void
     */

    public function saveQuickTask() {

        if ($this->session->userdata('userid')) {

            if ($_POST) {

                $calendarView = $this->input->post("calendarView");

                

                //prepare insert data
                $insertData = array(
                    "employee_id" => $this->session->userdata('userid'),
                    "client_id" => $this->input->post("quickClientId"),
                    "code_id" => $this->input->post("quickCodeId"),
                    "activity_id" => $this->input->post("quickActivity"),
                    "task_date" => $this->input->post("quickTaskDate"),
                    "task_duration" => $this->input->post("quickTaskDuration"),
                    "task_start_time" => $this->input->post("quickTaskStartTime"),
                    "task_end_time" => $this->input->post("quickTaskEndTime"),
                    "background_color" => $this->input->post("quickTaskColor"),
                    "created_date" => date("Y-m-d")
                );

                if($calendarView){

                    $TaskArr = $this->dashboard_model->getUserLatestFilledTask($this->input->post("quickTaskDate"), $this->session->userdata('userid'));

                    if ($TaskArr) {
                        $timestamp = strtotime($TaskArr['task_end']) + 3600;

                        $taskStartTime = $TaskArr['task_end'];
                        $taskEndTime = date('H:i', $timestamp);

                        $insertData['task_start_time'] = $taskStartTime;
                        $insertData['task_end_time'] = $taskEndTime;
                    }
                }
                // print_r($insertData);exit;
                
                $result = $this->dashboard_model->saveTask($insertData);
                echo $result;
                exit;
                
            }
        } else {
            redirect("login");
        }
    }

    /*
     * function name : getQuickListData
     * 
     * get user quick list codes details.
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : -
     * @return : json $data
     */

    public function getQuickListData() {

        if ($this->session->userdata('userid')) {

            $userId = $this->session->userdata('userid');
            $quickListData = $this->dashboard_model->getQuickListData($userId);
            

            echo json_encode($quickListData);
            exit;
           
        } else {
            redirect("login");
        }
    }

    /*
     * function name : deleteQuickList
     * 
     * Delete user quick list code.
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : -
     * @return : void
     */

    public function deleteQuickList() {

        if ($this->session->userdata('userid')) {

            $userId = $this->session->userdata('userid');

            $quickListId = $this->input->post("quickListId");

            $quickListData = $this->dashboard_model->getQuickListCodesByUser($userId);

            $str = preg_replace("/$quickListId/", '', $quickListData['quick_list']);
            $quickCode = str_replace(',,', ',', trim($str, ','));

            $data = $this->dashboard_model->updateQuickList($userId, $quickCode);
            echo $data;
            exit;
        } else {
            redirect("login");
        }
    }

}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */