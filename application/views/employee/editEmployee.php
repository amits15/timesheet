<?php echo $this->load->view("common/header", $title); ?>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/minimal/_all.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Employee
        </h1>
        <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Master</li>
            <li><a href="#"></i> Employee</a></li>          
            <li><a href="#"></i> Add</a></li>          
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                        <a href="javascript:history.back()" class="btn btn-link pull-right"><i class="fa fa-arrow-left" ></i> Back</a>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="<?php echo base_url("employee/edit"); ?>" method="POST" id="frmEditEmployee">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Emp Code <span class="text-red">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" name="txtEmpCode" id="txtEmpCode" class="form-control" value="<?php echo $emp['emp_code']; ?>" >
                                            <?php echo form_error('txtEmpCode', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">First Name <span class="text-red">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="hidden" name="txtEmpId" id="txtEmpId" class="form-control" value="<?php echo $emp['id']; ?>">
                                            <input type="text" name="txtEmpFirstName" id="txtEmpFirstName" class="form-control" value="<?php echo $emp['first_name']; ?>" >
                                            <?php echo form_error('txtEmpFirstName', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Last Name <span class="text-red">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" name="txtEmpLastName" id="txtEmpLastName" class="form-control" value="<?php echo $emp['last_name']; ?>">
                                            <?php echo form_error('txtEmpLastName', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Email <span class="text-red">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" name="txtEmpEmail" id="txtEmpEmail" class="form-control" value="<?php echo $emp['email_id']; ?>">
                                            <?php echo form_error('txtEmpEmail', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Gender <span class="text-red">*</span></label>
                                        <div class="col-sm-6">
                                            <div id="gender_div">
                                                Male &nbsp;<input type="radio" name="rdoEmpGender" id="rdoEmpGenderMale" class="form-control" value="M" <?php if($emp['gender'] == "M") echo "checked"; ?> > &nbsp;&nbsp;&nbsp;
                                                Female &nbsp;<input type="radio" name="rdoEmpGender" id="rdoEmpGenderFemale" class="form-control" value="F" <?php if($emp['gender'] == "F") echo "checked"; ?> >
                                            </div>
                                            <?php echo form_error('rdoEmpGender', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">DOB</label>
                                        <div class="col-sm-6">
                                            <div class="input-group date empDob">
                                                <input type="text" name="txtEmpDOB" id="txtEmpDOB" class="form-control" value="<?php if($emp['date_of_birth'] != "0000-00-00") echo $emp['date_of_birth']; ?>">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                            </div>
                                            <?php echo form_error('txtEmpDOB', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Phone</label>
                                        <div class="col-sm-6">
                                            <input type="text" name="txtEmpPhone" id="txtEmpPhone" class="form-control" value="<?php echo $emp['phone']; ?>">
                                            <?php echo form_error('txtEmpPhone', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-6">
                                	<div class="form-group">
                                        <label class="col-sm-4 control-label">Department <span class="text-red">*</span></label>
                                        <div class="col-sm-6">
                                        	<select id="optEmpDept" name="optEmpDept" class="form-control">
		                                        <option value="">Select Department</option>
		                                        <?php if(isset($dept_results)){
		                                            foreach ($dept_results as $key => $dept) { ?>
		                                                <option value="<?php echo $dept['id'] ?>" <?php if($emp['department'] == $dept['id']) echo "selected"; ?> ><?php echo $dept['dept_name'] ?></option>          
		                                        <?php }
		                                            } ?>
		                                    </select>
                                            <?php echo form_error('optEmpDept', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Designation <span class="text-red">*</span></label>
                                        <div class="col-sm-6">
                                        	<select id="optEmpDesignation" name="optEmpDesignation" class="form-control">
		                                        <option value="">Select Designation</option>
		                                        <?php if(isset($desig_results)){
		                                            foreach ($desig_results as $key => $desig) { ?>
		                                                <option value="<?php echo $desig['id'] ?>" <?php if($emp['designation'] == $desig['id']) echo "selected"; ?> ><?php echo $desig['desig_name'] ?></option>          
		                                        <?php }
		                                            } ?>
		                                    </select>
                                            <?php echo form_error('optEmpDesignation', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <?php if ($this->session->userdata('employee_type') == 'super_admin') {?>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Salary <span class="text-red">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" name="txtEmpSalary" id="txtEmpSalary" class="form-control" value="<?php echo $emp['salary']; ?>" >
                                            <?php echo form_error('txtEmpSalary', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="inputEmail3">Location <span class="text-red">*</span></label>
                                        <div class="col-sm-6">
                                            <select class="form-control col-sm-2" id="optEmpLocation" name="optEmpLocation">
                                                <option value="">Select Location</option>
                                                <?php foreach ($locations as $key => $location) { ?>
                                                    <option value="<?php echo $location['id'] ?>" <?php if($emp['location'] == $location['id']) echo "selected"; ?> ><?php echo $location['location_name'] ?></option>
                                                <?php } ?>
                                            </select>
                                            <?php echo form_error('optEmpLocation', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Type <span class="text-red">*</span></label>
                                        <div class="col-sm-6">
                                            <select class="form-control col-sm-2" id="optEmpType" name="optEmpType">
                                                <option value="">Select Type</option>
                                                <option value="employee" <?php if($emp['employee_type'] == "employee") echo "selected"; ?> >Employee</option>
                                                <option value="admin" <?php if($emp['employee_type'] == "admin") echo "selected"; ?> >Admin</option>
                                                <option value="super_admin" <?php if($emp['employee_type'] == "super_admin") echo "selected"; ?> >Super Admin</option>

                                            </select>
                                            <?php echo form_error('optEmpType', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">DOJ <span class="text-red">*</span></label>
                                        <div class="col-sm-6">
                                            <div class="input-group date empDOJ">
                                                <input type="text" name="txtEmpDOJ" id="txtEmpDOJ" class="form-control" value="<?php echo $emp['joining_date']; ?>">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                            </div>
                                            <?php echo form_error('txtEmpDOJ', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group lwdClass">
                                        <label class="col-sm-4 control-label">LWD </label>
                                        <div class="col-sm-6">
                                            <div class="input-group date empLWD">
                                                <input type="text" name="txtEmpLWD" id="txtEmpLWD" class="form-control" value="<?php if($emp['leaving_date'] != "0000-00-00") echo $emp['leaving_date']; ?>">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                            </div>
                                            <?php echo form_error('txtEmpLWD', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Active</label>
                                        <div class="col-sm-6">
                                            <input class="minimal" type="checkbox" name="chkEmpStatus" id="chkEmpStatus" value="1" <?php if($emp['status'] == "1") echo "checked"; ?> /> 
                                            <?php echo form_error('chkEmpStatus', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div><!-- /.box -->

            </div><!--/.col (left) -->

        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php echo $this->load->view("common/footer"); ?>
<script>
    var baseURL = "<?php echo base_url() ?>";
</script>
<script src="<?php echo base_url(); ?>assets/plugins/jValidate/formValidations.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js"></script>
<script>
    $('.input-group.date').datepicker({
        format: "yyyy-mm-dd"
    });

    $(function () {

        if($('#chkEmpStatus').is(':checked')){
            $(".lwdClass").hide();
        }else{
            $(".lwdClass").show();
        }

        $('input').on('ifUnchecked', function(event){
            $("#txtEmpLWD").val("");
            $(".lwdClass").show();
        });
        $('input').on('ifChecked', function(event){
            $("#txtEmpLWD").val("");
            $(".lwdClass").hide();
        });


        $('input').iCheck({
            radioClass: 'icheckbox_minimal-blue',
            checkboxClass: 'icheckbox_minimal-blue'
        });

		$("#optEmpDept").change(function () {
	        $.ajax({
	            url: '<?php echo base_url('department/getDeptDesignations'); ?>',
	            type: "POST",
	            dataType: "JSON",
	            data: {"deptId": $("#optEmpDept").val()},
	            beforeSend: function() { 
	                $('#loader').css("display", "block");
	            },
	            success: function (response) {
	                $('#loader').css("display", "none");
	                $("#optEmpDesignation").empty();
	                $("#optEmpDesignation").append("<option value=''>Select Designation</option>");
	                if (response) {

	                    $.each(response, function (index, itemData) {

	                        $("#optEmpDesignation").append("<option value='" + itemData.id + "'>" + itemData.desig_name + "</option>");

	                    });
	                }
	            }
	        });
	    });

    });
</script>