<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Emp_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /*
     * function name : getEmployeeData
     * 
     * Fetch Employee details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : none
     * @return : Array $data
     */

    public function getEmployeeData() {

        $this->db->select('l.location_name, u.*, dp.dept_name, ds.desig_name');
        $this->db->from('user_data as u');
        $this->db->join('location_data as l', 'u.location = l.id', 'LEFT');
        $this->db->join('dept_data as dp', 'u.department = dp.id', 'LEFT');
        $this->db->join('designation_data as ds', 'u.designation = ds.id', 'LEFT');
        $this->db->order_by("u.id");
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    /*
     * function name : getEmpDetails
     * 
     * Fetch Particular Employee details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : int $empId
     * @return : Array $data
     */

    public function getEmpDetails($empId) {

        $this->db->where("id", $empId);
        $query = $this->db->get('user_data');

        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }


    /*
     * function name : getEmpDesignations
     * 
     * Fetch Distinct designation of employees
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : int $empId
     * @return : Array $data
     */

    public function getEmpDesignations() {

        $this->db->distinct();
        $this->db->select('designation');
        $query = $this->db->get('user_data');
        // echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    /*
     * function name : checkEmpCode
     * 
     * Checks Emp code is exsist or not
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : $empCode, $empId
     * @return : Array $data
     */

    public function checkEmpCode($empCode, $empId) {

        $this->db->where("emp_code", $empCode);
        $this->db->where("id !=", $empId);
        $query = $this->db->get('user_data');

        if ($query->num_rows() == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    
    /*
     * function name : addEmp
     * 
     * Save Employee details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : array $insertData
     * @return : bool true|false
     */

    public function addEmp($insertData) {

        $this->db->insert('user_data', $insertData);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    /*
     * function name : editEmp
     * 
     * Update Employee details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : array $updateData, int $empId
     * @return : bool true|false
     */

    public function editEmp($updateData, $empId) {


        $this->db->update('user_data', $updateData, array("id" => $empId));

        return true;
    }
    
    /*
     * function name : deleteEmp
     * 
     * Delete Employee details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : int $empId
     * @return : bool true|false
     */

    public function deleteEmp($empId) {

        $this->db->delete('user_data', array("id" => $empId));
        return true;
    }


    public function updatePassword($password, $empId) {


        $this->db->update('user_data', array("password" => $password ), array("id" => $empId));

        return true;
    }

}
