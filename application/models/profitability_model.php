<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profitability_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /*
     * function name : getEmpSalary
     * 
     * Fetch Employee Salary details by Dept and designation wise
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : int $deptId, int $desigId
     * @return : Array $data
     */

    public function getEmpSalary($deptId, $desigId) {

        $this->db->select('max(u.salary) as maxSal');
        $this->db->from('user_data as u');
        $this->db->where('u.department', $deptId);
        $this->db->where('u.designation', $desigId);
        $query = $this->db->get();
// echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }
    

}
