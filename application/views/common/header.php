<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Glitch | <?php echo $title ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <link href="<?php echo base_url(); ?>assets/dist/img/fevicon.png" type="image/gif" rel="icon">

        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/ionicons.min.css">-->
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.css">

        <!-- Jquery Chosen-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/chosen.css">

        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">

    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="index2.html" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><img style="width: 50px" src="<?php echo base_url("assets/dist/img/logo.png"); ?>" /></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><img style="width: 110px" src="<?php echo base_url("assets/dist/img/logo.png"); ?>" /></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <?php if ($this->session->userdata("profile_pic")) { ?>
                                        <img class="user-image" src="<?php echo base_url() ?>uploads/profile/<?php echo $this->session->userdata("profile_pic") ?>" alt="User profile picture">
                                    <?php } else { ?>
                                        <img class="user-image" src="<?php echo base_url() ?>assets/dist/img/blank_profile.png" alt="User profile picture">
                                    <?php } ?>
                                    <span class="hidden-xs"><?php echo $this->session->userdata("first_name") . " " . $this->session->userdata("last_name"); ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <?php if ($this->session->userdata("profile_pic")) { ?>
                                            <img class="img-circle" src="<?php echo base_url() ?>uploads/profile/<?php echo $this->session->userdata("profile_pic") ?>" alt="User profile picture">
                                        <?php } else { ?>
                                            <img class="img-circle" src="<?php echo base_url() ?>assets/dist/img/blank_profile.png" alt="User profile picture">
                                        <?php } ?>
                                        <p>
                                            <?php echo $this->session->userdata("first_name") . " " . $this->session->userdata("last_name") . " - " . $this->session->userdata("designation"); ?>
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="<?php echo base_url("profile/index") . "/" . $this->session->userdata("userid") ?>" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="<?php echo base_url("login/logout") ?>" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <?php echo $this->load->view("common/sidebar"); ?>