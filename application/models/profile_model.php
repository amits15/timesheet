<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profile_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /*
     * function name : getUserProfile
     * 
     * Fetch User Profile details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : $userId
     * @return : Array $data
     */

    public function getUserProfile($userId) {

        $this->db->where("id", $userId);
        $query = $this->db->get('user_data');

        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    /*
     * function name : checkOldPwd
     * 
     * Check user old password is match with entered value
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : $userId, $oldPwd
     * @return : Array $data
     */

    public function checkOldPwd($userId, $oldPwd) {

        $this->db->where("id", $userId);
        $this->db->where("password", md5($oldPwd));
        $query = $this->db->get('user_data');

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * function name : updateProfile
     * 
     * Update User Profile details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : array $updateData, int $userId
     * @return : bool true|false
     */

    public function updateProfile($updateData, $userId) {


        $this->db->update('user_data', $updateData, array("id" => $userId));

        $data = array(
            "first_name" => $updateData['first_name'],
            "last_name" => $updateData['last_name']
        );
        $this->session->set_userdata($data);

        return true;
    }

    /*
     * function name : changePassword
     * 
     * Update User Password
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : array $updateData, int $userId
     * @return : bool true|false
     */

    public function changePassword($updateData, $userId) {


        $this->db->update('user_data', $updateData, array("id" => $userId));

        return true;
    }

    /*
     * function name : changePhoto
     * 
     * Update User photo
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : array $updateData, int $userId
     * @return : bool true|false
     */

    public function changePhoto($updateData, $userId) {


        $this->db->update('user_data', $updateData, array("id" => $userId));

        $data = array(
            "profile_pic" => $updateData['profile_image']
        );
        $this->session->set_userdata($data);

        return true;
    }

}
