<?php echo $this->load->view("common/header", $title); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/datatables/dataTables.bootstrap.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Holidays
        </h1>
        <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Master</li>
            <li><a href="#"></i> Holidays</a></li>          
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <form action="<?php echo base_url("holidays/showHolidays") ?>" method="POST" id="frmHolidaysList" >
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Holidays List</h3>                            
                        </div><!-- /.box-header -->                        
                        <div class="box-body">
                            <div class="form-group pull-right">
                                <label class="col-sm-3 control-label">Year</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="optYear" id="optYear">
                                        <?php 
                                            foreach ($years as $key => $year) { ?>
                                                <option value="<?php echo $year['year'] ?>" <?php if($selectedYear == $year['year']) echo "selected" ?> ><?php echo $year['year'] ?></option>
                                            <?php }
                                         ?>
                                    </select>
                                </div>
                            </div>
                            <br/><br/>
                            <table id="" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Sr No.</th>
                                        <th>Date</th>
                                        <th>Day</th>
                                        <th>Holiday</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($holidays) {
                                        foreach ($holidays as $key => $holiday) {
                                            ?>
                                            <tr>
                                                <td><?php echo $key+1 ?> </td>
                                                <td><?php echo date("d M, Y", strtotime($holiday['holiday_date'])) ?></td>
                                                <td><?php echo date("l", strtotime($holiday['holiday_date'])) ?></td>
                                                <td><?php echo $holiday['holiday_desc'] ?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <!-- Modal -->
                    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Delete Holiday</h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure you want to delete Holiday?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                    <a class="btn btn-danger">Yes</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="multiDeleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Delete Holiday</h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure you want to delete selected Holiday(s)?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                    <button type="submit" class="btn btn-danger">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->



<?php echo $this->load->view("common/footer"); ?>
<!-- DataTables -->
<script src="<?php echo base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>

    
    $(function () {

        $('#optYear').change(function(){
            window.location.href = '<?php echo base_url() ?>holidays/showHolidays/'+ $(this).val();
        });
    });
</script>