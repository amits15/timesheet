<?php echo $this->load->view("common/header", $title); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/datatables/dataTables.bootstrap.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Department Master
        </h1>
        <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Master</li>
            <li><a href="#"></i> Department</a></li>          
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <form action="<?php echo base_url("department/delete") ?>" method="POST" id="frmDeptList" >
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Department List</h3>

                            <?php if($this->session->flashdata('success_msg')) { ?>
                                <div class="alert alert-success">
                                    <?php echo $this->session->flashdata('success_msg'); ?>
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                </div>
                            <?php } ?>
                            
                        </div><!-- /.box-header -->
                        &nbsp;&nbsp;&nbsp;<a class="btn btn-primary" href="<?php echo base_url("department/add") ?>"><i class="fa fa-plus"></i> Add</a>
                        &nbsp;&nbsp;&nbsp;<a class="btn btn-danger" data-toggle="modal" id="multiDelete" href="javascript:void(0)"><i class="fa fa-trash"></i> Delete</a>
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th style="width: 5%" ><input type="checkbox" class="grp_check" name="check_all" /></th>
                                        <!--<th >Sr No.</th>-->
                                        <th>Department Name</th>
                                        <th>Added Date</th>
                                        <th>Status</th>
                                        <th style="width: 10%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    /*if ($dept_results) {
                                        foreach ($dept_results as $dept) {
                                            ?>
                                            <tr>
                                                <td><input type="checkbox" name="check_depts[]" class="multi_check" value="<?php echo $dept['id'] ?>" /></td>
                                                <td><?php echo $dept['dept_name'] ?></td>
                                                <td><?php echo date("d M, Y h:i A", strtotime($dept['added_date'])) ?></td>
                                                <td>
                                                    <?php
                                                    if ($dept['status'] == 1) {
                                                        echo "Active";
                                                    } else {
                                                        echo "Inactive";
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <a class="" href="<?php echo base_url("department/edit/$dept[id]") ?>"><i class="fa fa-edit fa-lg"></i></a>&nbsp;&nbsp;
                                                    <a data-href="<?php echo base_url("department/delete/$dept[id]") ?>" data-toggle="modal" data-target="#deleteModal" href="#"><i class="fa fa-trash fa-lg"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }*/
                                    ?>
                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <!-- Modal -->
                    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Delete Department</h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure you want to delete Department?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                    <a class="btn btn-danger">Yes</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="multiDeleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Delete Department</h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure you want to delete selected Department(s)?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                    <button type="submit" class="btn btn-danger">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->



<?php echo $this->load->view("common/footer"); ?>
<!-- DataTables -->
<script src="<?php echo base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>

    
    $(function () {

        $('#example1').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "aoColumns": 
            [
                { "bSortable": false, "searchable": false },
                null,
                null,
                null,
                { "bSortable": false, "searchable": false },
            ],
            "serverSide": true,
            "processing": true,
            "language": {
                "processing": "Loading data..." //add a loading image,simply putting <img src="loader.gif" /> tag.
            },
            "ajax": {
                url: '<?php echo base_url(); ?>department/datatable',
                type: 'POST'
            }
        });
    });
</script>