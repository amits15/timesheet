<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Designation extends CI_Controller {
    /*
     * function name :__construct
     *  Counstructor for Designation controller 
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('datatables');
        $this->load->library('form_validation');
        $this->load->model('desig_model');
        $this->load->model('dept_model');
    }

    /*
     * function name :index
     *  Shows the Designation listing page.
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    public function index() {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Designation"
            );

            // $data['desig_results'] = $this->desig_model->getDesignationData();

            $this->load->view('designation/designationList', $data);
        } else {
            redirect("login");
        }
    }


    public function datatable() {

        $column = '<a class="" href="'. base_url("designation/edit/$1") .'"><i class="fa fa-edit fa-lg"></i></a>&nbsp;&nbsp;
         <a data-href="'. base_url("designation/delete/$1") .'" data-toggle="modal" data-target="#deleteModal" href="#"><i class="fa fa-trash fa-lg"></i></a>';

        $this->datatables->select("ds.id, dp.dept_name, ds.desig_name, DATE_FORMAT(ds.added_date, '%d %b, %Y') as added_date, case ds.status  when '1' then 'Active'  when '0' then 'Inactive' end as status", FALSE)
                ->from('designation_data as ds')
                ->join('dept_data as dp', 'dp.id = ds.dept_id', 'LEFT')
                ->edit_column('id', '<input type="checkbox"  name="check_desigs[]" value="$1"  class="multi_check" />', 'id')
                ->add_column('edit', "$column", 'id');

                echo $this->datatables->generate();
    }

    /*
     * function name :add
     *  Add Department Details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    public function add() {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Add Designation"
            );

            if ($_POST) {

                $this->form_validation->set_rules('optDesigDept', 'Department Name', 'required');
                $this->form_validation->set_rules('txtDesigName', 'Designation Name', 'required');
                $this->form_validation->set_rules('optDesigStatus', 'Status', 'required');

                if ($this->form_validation->run() == true) {

                    $insertData = array(
                        "dept_id" => $this->input->post("optDesigDept"),
                        "desig_name" => $this->input->post("txtDesigName"),
                        "status" => $this->input->post("optDesigStatus"),
                        "added_date" => date("Y-m-d H:i:s")
                    );
                    $result = $this->desig_model->addDesignation($insertData);

                    if ($result) {
                        $this->session->set_flashdata("success_msg", "Designation Saved Successfully.");
                        redirect("designation");
                    } else {
                        redirect("designation/add");
                    }
                }
            }

            $data['dept_results'] = $this->dept_model->getActiveDeptData();

            $this->load->view('designation/addDesignation', $data);
        } else {
            redirect("login");
        }
    }

    /*
     * function name : edit
     *  Update Designation details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : Int $deptId
     * @return : void
     */

    public function edit($desigId = NULL) {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Edit Designation"
            );

            if ($_POST) {

                $this->form_validation->set_rules('optDesigDept', 'Department Name', 'required');
                $this->form_validation->set_rules('txtDesigName', 'Designation Name', 'required');
                $this->form_validation->set_rules('optDesigStatus', 'Status', 'required');

                if ($this->form_validation->run() == true) {


                    $desigId = $this->input->post("txtDesigId");

                    $updateData = array(
                        "dept_id" => $this->input->post("optDesigDept"),
                        "desig_name" => $this->input->post("txtDesigName"),
                        "status" => $this->input->post("optDesigStatus")
                    );
                    $result = $this->desig_model->editDesignation($updateData, $desigId);

                    if ($result) {
                        $this->session->set_flashdata("success_msg", "Designation Updated Successfully.");
                        redirect("designation");
                    } else {
                        redirect("designation/edit/$desigId");
                    }
                }
            }

            $data['dept_results'] = $this->dept_model->getActiveDeptData();
            $data['designation'] = $this->desig_model->getDesigDetails($desigId);

            $this->load->view('designation/editDesignation', $data);
        } else {
            redirect("login");
        }
    }

    /*
     * function name : delete
     *  Delete Designation details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : Int $deptId
     * @return : void
     */

    public function delete($desigId = NULL) {

        if ($this->session->userdata('userid')) {

            if ($desigId) {

                $this->desig_model->deleteDesignation($desigId);
            } else if ($_POST) {

                $desigIds = $this->input->post("check_desigs");

                foreach ($desigIds as $key => $value) {
                    $this->desig_model->deleteDesignation($value);
                }
            }
            $this->session->set_flashdata("success_msg", "Designation Deleted Successfully.");
            redirect("designation");
        } else {
            redirect("login");
        }
    }

}

/* End of file department.php */
/* Location: ./application/controllers/department.php */