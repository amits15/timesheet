<?php echo $this->load->view("common/header", $title); ?>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/minimal/_all.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User Profile
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">User profile</li>  
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-4">
                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <?php if ($user['profile_image']) { ?>
                            <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url() ?>uploads/profile/<?php echo $user['profile_image'] ?>" alt="User profile picture">
                        <?php } else { ?>
                            <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url() ?>assets/dist/img/blank_profile.png" alt="User profile picture">
                        <?php } ?>


                        <h3 class="profile-username text-center"><?php echo $user['first_name'] . " " . $user['last_name'] ?></h3>
                        <p class="text-muted text-center"><?php echo $user['designation'] ?></p>
                        <span class="text-red">
                            <?php
                            if ($this->session->flashdata("upload_err")) {
                                echo $this->session->flashdata("upload_err");
                            }
                            ?>
                        </span>

                        <form name="frmProfilePhoto" id="frmProfilePhoto" method="POST" action="<?php echo base_url("profile/changePhoto") ?>" enctype="multipart/form-data" >
                            <input type="hidden" name="userId" id="userId" value="<?php echo $user['id'] ?>" />
                            <input type="file" name="userPhoto" id="userPhoto" style="visibility: hidden" />
                        </form>
                        <a href="javascript:void(0)" id="btnPhoto" class="btn btn-primary btn-block"><b>Change Photo</b></a>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

                <!-- Change Password Box -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Change Password</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <span class="text-aqua">
                            <?php
                            if ($this->session->flashdata("success_msg")) {
                                echo $this->session->flashdata("success_msg");
                                header("refresh: 5; url=" . base_url("login/logout"));
                            }
                            ?>
                        </span>
                        <form name="frmChangePassword" id="frmChangePassword" method="POST" action="<?php echo base_url("profile/changePassword") ?>" >
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Old Password  <span class="text-red">*</span></label>
                                    <input type="hidden" name="txtUserId" id="txtUserId" value="<?php echo $user['id'] ?>" class="form-control">
                                    <input type="password" placeholder="Old Password" name="txtUserOldPwd" id="txtUserOldPwd" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>New Password  <span class="text-red">*</span></label>
                                    <input type="password" placeholder="New Password" name="txtUserNewPwd" id="txtUserNewPwd" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password  <span class="text-red">*</span></label>
                                    <input type="password" placeholder="Confirm Password" name="txtUserConfirmPwd" id="txtUserConfirmPwd" class="form-control">
                                </div>
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit" name="pwdSub" id="pwdSub">Submit</button>
                            </div>
                        </form>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-8">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#settings" data-toggle="tab">Edit Profile</a></li>
                    </ul>
                    <div class="tab-content">
                        <span class="text-aqua">
                            <?php
                            if ($this->session->flashdata("profile_msg")) {
                                echo $this->session->flashdata("profile_msg");
                            }
                            ?>
                        </span>
                        <div class="active tab-pane" id="settings">
                            <form class="form-horizontal" name="frmUserProfile" id="frmUserProfile" method="POSt" action="<?php echo base_url("profile/updateProfile"); ?>">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">First Name</label>
                                    <div class="col-sm-6">
                                        <input type="hidden" name="txtUserProfileId" id="txtUserProfileId" class="form-control" value="<?php echo $user['id']; ?>">
                                        <input type="text" name="txtUserFirstName" id="txtUserFirstName" class="form-control" value="<?php echo $user['first_name']; ?>" >
                                        <?php echo form_error('txtUserFirstName', '<div class="form-error">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Last Name</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="txtUserLastName" id="txtUserLastName" class="form-control" value="<?php echo $user['last_name']; ?>">
                                        <?php echo form_error('txtUserLastName', '<div class="form-error">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Email </label>
                                    <div class="col-sm-6">
                                        <input type="text" readonly name="txtUserEmail" id="txtUserEmail" class="form-control" value="<?php echo $user['email_id']; ?>">
                                        <?php echo form_error('txtUserEmail', '<div class="form-error">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Gender</label>
                                    <div class="col-sm-6">
                                        <div id="gender_div">
                                            Male &nbsp;<input type="radio" name="rdoUserGender" id="rdoUserGenderMale" class="form-control" value="M" <?php if ($user['gender'] == "M") echo "checked"; ?> > &nbsp;&nbsp;&nbsp;
                                            Female &nbsp;<input type="radio" name="rdoUserGender" id="rdoUserGenderFemale" class="form-control" value="F" <?php if ($user['gender'] == "F") echo "checked"; ?> >
                                        </div>
                                        <?php echo form_error('rdoUserGender', '<div class="form-error">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">DOB</label>
                                    <div class="col-sm-6">
                                        <div class="input-group date empDob">
                                            <input type="text" name="txtUserDOB" id="txtUserDOB" class="form-control" value="<?php if ($user['date_of_birth'] != "0000-00-00") echo $user['date_of_birth']; ?>">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                        </div>
                                        <?php echo form_error('txtUserDOB', '<div class="form-error">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Phone</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="txtUserPhone" id="txtUserPhone" class="form-control" value="<?php echo $user['phone']; ?>">
                                        <?php echo form_error('txtUserPhone', '<div class="form-error">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Department</label>
                                    <div class="col-sm-6">
                                        <input type="text" readonly name="txtUserDept" id="txtUserDept" class="form-control" value="<?php echo $user['department']; ?>">
                                        <?php echo form_error('txtUserDept', '<div class="form-error">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Designation</label>
                                    <div class="col-sm-6">
                                        <input type="text" readonly name="txtUserDesignation" id="txtUserDesignation" class="form-control" value="<?php echo $user['designation']; ?>">
                                        <?php echo form_error('txtUserDesignation', '<div class="form-error">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Location</label>
                                    <div class="col-sm-6">
                                        <select disabled="" class="form-control col-sm-2" id="optUserLocation" name="optUserLocation">
                                            <option value="">Select Location</option>
                                            <?php foreach ($locations as $key => $location) { ?>
                                                <option value="<?php echo $location['id'] ?>" <?php if ($user['location'] == $location['id']) echo "selected"; ?> ><?php echo $location['location_name'] ?></option>
                                            <?php } ?>
                                        </select>
                                        <?php echo form_error('optUserLocation', '<div class="form-error">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Type</label>
                                    <div class="col-sm-6">
                                        <select disabled="" class="form-control col-sm-2" id="optUserType" name="optUserType">
                                            <option value="">Select Type</option>
                                            <option value="employee" <?php if ($user['employee_type'] == "employee") echo "selected"; ?> >Employee</option>
                                            <option value="admin" <?php if ($user['employee_type'] == "admin") echo "selected"; ?> >Admin</option>
                                            <option value="superadmin" <?php if ($user['employee_type'] == "superadmin") echo "selected"; ?> >Super Admin</option>

                                        </select>
                                        <?php echo form_error('optUserType', '<div class="form-error">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">DOJ</label>
                                    <div class="col-sm-6">
                                        <!--<div class="input-group date empDOJ">-->
                                        <input type="text" readonly name="txtUserDOJ" id="txtUserDOJ" class="form-control" value="<?php echo $user['joining_date']; ?>">
                                        <!--<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>-->
                                        <!--</div>-->
                                        <?php echo form_error('txtUserDOJ', '<div class="form-error">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" id="profileSub" name="profileSub" class="btn btn-danger">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div><!-- /.tab-pane -->
                    </div><!-- /.tab-content -->
                </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php echo $this->load->view("common/footer"); ?>
<script>
    var baseURL = "<?php echo base_url() ?>";
</script>
<script src="<?php echo base_url(); ?>assets/plugins/jValidate/formValidations.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js"></script>
<script>
    $('.input-group.date').datepicker({
        format: "yyyy-mm-dd"
    });

    $(function () {
        $('input').iCheck({
            radioClass: 'icheckbox_minimal-blue'
        });
    });

    $('#btnPhoto').click(function () {
        $('#userPhoto').click();
    });
    $('#userPhoto').change(function () {
        $('#frmProfilePhoto').submit();
    });
</script>