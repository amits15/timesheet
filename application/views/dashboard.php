<?php echo $this->load->view("common/header", $title); ?>
<!-- fullCalendar 2.2.5-->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fullcalendar/fullcalendar.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fullcalendar/fullcalendar.print.css" media="print">

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/minimal/_all.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">

<style>
    .fc-content {
        cursor: pointer;
    }
    .datepicker{z-index:1151 !important;}
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-4">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h4 class="box-title">Add Task</h4>
                        <div class="box-tools pull-right">
                            <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <form name="frmUserTimesheet" id="frmUserTimesheet" method="POST" >
                            <div class="box-body">
                                    <div class="form-group">
                                        <label>Client <span class="text-red">*</span></label>
                                        <select class="form-control col-sm-2" id="optTimeSheetClient" name="optTimeSheetClient">
                                            <option value="">Select Client</option>
                                            <?php foreach ($clients as $key => $client) { ?>
                                                <option value="<?php echo $client['id'] ?>" ><?php echo $client['client_name'] ?></option>
                                            <?php } ?>
                                        </select>
                                        <?php echo form_error('optTimeSheetClient', '<div class="form-error">', '</div>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Code <span class="text-red">*</span> <span> &nbsp;&nbsp;&nbsp; <img id="loader" class="pull-right" style="display:none;" src="<?php echo base_url("assets/dist/img/spin.svg") ?>" /> </span> </label>
                                        <select class="form-control col-sm-2" id="optTimeSheetCode" name="optTimeSheetCode">
                                            <option value="">Select Code</option>
                                        </select>
                                        <?php echo form_error('optTimeSheetCode', '<div class="form-error">', '</div>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Activity  <span class="text-red">*</span></label>
                                        <select class="form-control col-sm-2" id="optTimeSheetActivity" name="optTimeSheetActivity">
                                            <option value="">Select Activity</option>
                                            <?php foreach ($activities as $key => $activity) { ?>
                                                <option value="<?php echo $activity['id'] ?>" ><?php echo $activity['activity'] ?></option>
                                            <?php } ?>
                                        </select>
                                        <?php echo form_error('optTimeSheetActivity', '<div class="form-error">', '</div>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Date  <span class="text-red">*</span></label>
                                        <div class="input-group date taskDate">
                                            <input type="text" readonly="" name="txtTimeSheetDate" id="txtTimeSheetDate" class="form-control">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                        </div>
                                        <?php echo form_error('txtTimeSheetDate', '<div class="form-error">', '</div>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Time <span class="text-red">*</span></label>
                                        <select class="form-control " name="optTimeSheetTime" id="optTimeSheetTime">
                                            <option value="">HR:MM</option>
                                            <option value="00:15">00:15</option>
                                            <option value="00:30">00:30</option>
                                            <option value="00:45">00:45</option>
                                            <option value="01:00">01:00</option>
                                            <option value="01:15">01:15</option>
                                            <option value="01:30">01:30</option>
                                            <option value="01:45">01:45</option>
                                            <option value="02:00">02:00</option>
                                            <option value="02:15">02:15</option>
                                            <option value="02:30">02:30</option>
                                            <option value="02:45">02:45</option>
                                            <option value="03:00">03:00</option>
                                            <option value="03:15">03:15</option>
                                            <option value="03:30">03:30</option>
                                            <option value="03:45">03:45</option>
                                            <option value="04:00">04:00</option>
                                            <option value="04:15">04:15</option>
                                            <option value="04:30">04:30</option>
                                            <option value="04:45">04:45</option>
                                            <option value="05:00">05:00</option>
                                            <option value="05:15">05:15</option>
                                            <option value="05:30">05:30</option>
                                            <option value="05:45">05:45</option>
                                            <option value="06:00">06:00</option>
                                            <option value="06:15">06:15</option>
                                            <option value="06:30">06:30</option>
                                            <option value="06:45">06:45</option>
                                            <option value="07:00">07:00</option>
                                            <option value="07:15">07:15</option>
                                            <option value="07:30">07:30</option>
                                            <option value="07:45">07:45</option>
                                            <option value="08:00">08:00</option>
                                        </select>
                                        <?php echo form_error('optTimeSheetTime', '<div class="form-error">', '</div>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea class="form-control" name="rtTimeSheetDescription" id="rtTimeSheetDescription"></textarea>
                                        <?php echo form_error('rtTimeSheetDescription', '<div class="form-error">', '</div>'); ?>
                                    </div>
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                <button class="btn btn-primary pull-left" type="submit" name="btnTimesheet" id="btnTimesheet">Submit</button>
                                <span class="pull-left"> &nbsp;&nbsp;&nbsp; <img id="taskLoader" style="display:none;" src="<?php echo base_url("assets/dist/img/spin.svg") ?>" /> </span>
                                <button class="btn btn-info pull-right" type="submit" name="" id="btnQuickList">Add to QuickList</button>
                                <span class="pull-right"> <img id="addQuickLoader" style="display:none;" src="<?php echo base_url("assets/dist/img/spin.svg") ?>" />&nbsp;&nbsp;&nbsp; </span>
                            </div>
                        </form>
                    </div><!-- /.box-body -->
                </div><!-- /. box -->
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h4 class="box-title">Quick List</h4>
                        <span> &nbsp;&nbsp;&nbsp; <img id="quickLoader" class="pull-right" style="display:none;" src="<?php echo base_url("assets/dist/img/spin.svg") ?>" /> </span>
                    </div>
                    <div class="box-body">
                        <div id="external-events">
                            <?php if($quickListData){
                                foreach ($quickListData as $key => $quickList) { 
                                    if ($quickList['codeType'] == "project") {
                                        $background_color = "#fa5858";
                                    } else if ($quickList['codeType'] == "retainer") {
                                        $background_color = "#c317b4";
                                    } else if ($quickList['codeType'] == "corporate") {
                                        $background_color = "#00a7e5";
                                    }
                                    ?>
                                    <div class="external-event" data-color="<?php echo $background_color; ?>" data-client="<?php echo $quickList['clientId']; ?>" data="<?php echo $quickList['codeId']; ?>" style="color:#fff; background-color: <?php echo $background_color; ?>" ><?php echo $quickList['productCode']; ?> <a class="pull-right" style="color:#000" onclick="deleteQuickList('<?php echo $quickList['clientId'] . "_" .$quickList['codeId']; ?>')" href='javascript:void(0)'>X</a></div>
                            <?php }
                            } ?>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /. box -->
            </div><!-- /.col -->            
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body no-padding">
                        <!-- THE CALENDAR -->
                        <div id="calendar"></div>
                    </div><!-- /.box-body -->
                </div><!-- /. box -->
            </div><!-- /.col -->
            <div class="col-md-4">
                
            </div>
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" name="frmEditUserTimesheet" id="frmEditUserTimesheet" method="POST" action="<?php echo base_url("dashboard/updateTask") ?>" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Task</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Client <span class="text-red">*</span></label>
                            <div class="col-sm-7">
                                <input type="hidden" name="txtEditTimeSheetId" id="txtEditTimeSheetId" />
                                <select class="form-control col-sm-2" id="optEditTimeSheetClient" name="optEditTimeSheetClient">
                                    <option value="">Select Client</option>
                                    <?php foreach ($clients as $key => $client) { ?>
                                        <option value="<?php echo $client['id'] ?>" ><?php echo $client['client_name'] ?></option>
                                    <?php } ?>
                                </select>
                                <?php echo form_error('optEditTimeSheetClient', '<div class="form-error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label lblCode">Code <span class="text-red">*</span> <img id="loader" style="display:none;" src="<?php echo base_url("assets/dist/img/spin.svg") ?>" /></label>
                            <div class="col-sm-7">
                                <select class="form-control col-sm-2" id="optEditTimeSheetCode" name="optEditTimeSheetCode">
                                    <option value="">Select Code</option>
                                </select>
                                <?php echo form_error('optEditTimeSheetCode', '<div class="form-error">', '</div>'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Activity  <span class="text-red">*</span></label>
                            <div class="col-sm-7">
                                <select class="form-control col-sm-2" id="optEditTimeSheetActivity" name="optEditTimeSheetActivity">
                                    <option value="">Select Activity</option>
                                    <?php foreach ($activities as $key => $activity) { ?>
                                        <option value="<?php echo $activity['id'] ?>" ><?php echo $activity['activity'] ?></option>
                                    <?php } ?>
                                </select>
                                <?php echo form_error('optEditTimeSheetActivity', '<div class="form-error">', '</div>'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Date  <span class="text-red">*</span></label>
                            <div class="col-sm-7">
                                <div class="input-group date taskEditDate">
                                    <input type="text" readonly="" name="txtEditTimeSheetDate" id="txtEditTimeSheetDate" class="form-control">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                </div>
                                <?php echo form_error('txtEditTimeSheetDate', '<div class="form-error">', '</div>'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Time <span class="text-red">*</span></label>
                            <div class="col-sm-7">
                                <input type="hidden" name="txtEditTimeSheetStart" id="txtEditTimeSheetStart" class="form-control">
                                <input type="hidden" name="txtEditTimeSheetEnd" id="txtEditTimeSheetEnd" class="form-control">
                                <select class="form-control " name="optEditTimeSheetTime" id="optEditTimeSheetTime">
                                    <option value="">HR:MM</option>
                                    <option value="00:15">00:15</option>
                                    <option value="00:30">00:30</option>
                                    <option value="00:45">00:45</option>
                                    <option value="01:00">01:00</option>
                                    <option value="01:15">01:15</option>
                                    <option value="01:30">01:30</option>
                                    <option value="01:45">01:45</option>
                                    <option value="02:00">02:00</option>
                                    <option value="02:15">02:15</option>
                                    <option value="02:30">02:30</option>
                                    <option value="02:45">02:45</option>
                                    <option value="03:00">03:00</option>
                                    <option value="03:15">03:15</option>
                                    <option value="03:30">03:30</option>
                                    <option value="03:45">03:45</option>
                                    <option value="04:00">04:00</option>
                                    <option value="04:15">04:15</option>
                                    <option value="04:30">04:30</option>
                                    <option value="04:45">04:45</option>
                                    <option value="05:00">05:00</option>
                                    <option value="05:15">05:15</option>
                                    <option value="05:30">05:30</option>
                                    <option value="05:45">05:45</option>
                                    <option value="06:00">06:00</option>
                                    <option value="06:15">06:15</option>
                                    <option value="06:30">06:30</option>
                                    <option value="06:45">06:45</option>
                                    <option value="07:00">07:00</option>
                                    <option value="07:15">07:15</option>
                                    <option value="07:30">07:30</option>
                                    <option value="07:45">07:45</option>
                                    <option value="08:00">08:00</option>
                                </select>
                                <?php echo form_error('optEditTimeSheetTime', '<div class="form-error">', '</div>'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Description</label>
                            <div class="col-sm-7">
                                <textarea class="form-control" name="rtEditTimeSheetDescription" id="rtEditTimeSheetDescription"></textarea>
                                <?php echo form_error('rtEditTimeSheetDescription', '<div class="form-error">', '</div>'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <span> &nbsp;&nbsp;&nbsp; <img id="editTaskLoader" style="display:none;" src="<?php echo base_url("assets/dist/img/spin.svg") ?>" /> </span>
                    <button type="submit" id="taskUpdate" class="btn btn-primary">Save</button>
                    <button type="button" id="taskDelete" data-id="" class="btn btn-danger">Delete</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php echo $this->load->view("common/footer"); ?>
<!-- fullCalendar 2.2.5 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/jValidate/formValidations.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/fullcalendar/fullcalendar.min.js"></script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script> -->
<script src="<?php echo base_url(); ?>assets/dist/js/jquery.ui.touch-punch.js"></script>

<script>
    
    function ini_events(ele) {
          ele.each(function () {

            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
              title: $.trim($(this).text()) // use the element's text as the event title
            };

            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);
            $(this).addTouch();
            // make the event draggable using jQuery UI
            $(this).draggable({
              zIndex: 1070,
              revert: true, // will cause the event to go back to its
              revertDuration: 0  //  original position after the drag
            });

          });
        }

    $(function () {
//        var taskResponse = "";

        var response = '';

        $("#optTimeSheetClient").chosen({
            search_contains: true
        });
        $("#optTimeSheetCode").chosen({
            search_contains: true
        });
        $("#optTimeSheetActivity").chosen({
            search_contains: true
        });
        $("#optTimeSheetTime").chosen({
            search_contains: true
        });

        $.ajax({
            url: '<?php echo base_url('dashboard/getHolidays'); ?>',
            type: "POST",
            async: true,
            dataType: "JSON",
            success: function (response) {
                // alert(response);.
                setDatePickerDates(response)
            }
        });

        function setDatePickerDates(forbidden) {

            $('.input-group.date').datepicker({
                format: "yyyy-mm-dd",
                beforeShowDay: function (Date) {
                    var curr_day = Date.getDate();
                    var curr_month = Date.getMonth() + 1;
                    var curr_year = Date.getFullYear();

                    if(curr_day <= 9){
                        curr_day = "0"+ curr_day;
                    }

                    if(curr_month <= 9){
                        curr_month = "0"+ curr_month;
                    }

                    var curr_date = curr_year + '-' + curr_month + '-' + curr_day;
                    // alert(curr_date);
                    if (forbidden.indexOf(curr_date) > -1)
                        return false;
                }
            });

        }
        $("#optTimeSheetClient").change(function () {
            $.ajax({
                url: '<?php echo base_url('dashboard/getClientCodes'); ?>',
                type: "POST",
                dataType: "JSON",
                data: {"clientId": $("#optTimeSheetClient").val()},
                beforeSend: function() { 
                    $('#loader').css("display", "block");
                },
                success: function (response) {
                    $('#loader').css("display", "none");
                    $("#optTimeSheetCode").empty();
                    $("#optTimeSheetCode").append("<option value=''>Select Code</option>");
                    if (response) {


                        $.each(response, function (index, itemData) {

                            $("#optTimeSheetCode").append("<option value='" + itemData.id + "'>" + itemData.pCode + "</option>");

                        });

                    }
                    $('#optTimeSheetCode').trigger("chosen:updated");

                }

            });
        });

        $(document).on("change", "#optEditTimeSheetClient", function () { //        alert("client");
            $.ajax({
                url: '<?php echo base_url('dashboard/getClientCodes'); ?>',
                type: "POST",
                dataType: "JSON",
                data: {"clientId": $("#optEditTimeSheetClient").val()},
                beforeSend: function() { 
                    $('#loader').css("display", "block");
                },
                success: function (response) {
                    $('#loader').css("display", "none");
                    $("#optEditTimeSheetCode").empty();
                    $("#optEditTimeSheetCode").append("<option value=''>Select Code</option>");
                    if (response) {
                        $.each(response, function (index, itemData) {

                            $("#optEditTimeSheetCode").append("<option value='" + itemData.id + "'>" + itemData.pCode + "</option>");

                        });
                    }
                    $('#optEditTimeSheetCode').trigger("chosen:updated");
                }

            });
        });

        /* initialize the external events
         -----------------------------------------------------------------*/
        
        ini_events($('#external-events div.external-event'));


        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date();
        var d = date.getDate(),
                m = date.getMonth(),
                y = date.getFullYear();
        $('#calendar').fullCalendar({
            firstDay: "1",
            defaultView: 'agendaWeek',
            minTime: "08:00:00",
            slotDuration: '00:15:00',
            timeFormat: 'hh(:mm) A',
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            //Fetch events dynamically
            eventSources: [
                {
                    url: '<?php echo base_url("dashboard/getUserTasks"); ?>',
                    type: 'POST',
                    error: function () {
                        alert('there was an error while fetching events!');
                    }
                }
            ],
            eventConstraint: {
                start: '0:00',
                end: '24:00'
            },
            editable: true,
            eventRender: function(event, element) {
                $(element).addTouch();
            },
            droppable: true, // this allows things to be dropped onto the calendar !!!
            drop: function (date, allDay) { // this function is called when something is dropped

                //get calendar view type
                var calendarView = $('#calendar').fullCalendar('getView');
                
                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject');

                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);
                
                
                
                var sDate, startDate;                
                var endDate, edate;
                var view = "";
                if(calendarView.name == "month"){
                    view = "month";
                    sDate = $.fullCalendar.moment(date.format());
                    startDate = new Date(sDate);
                    startDate.setTime(startDate.getTime()+ (210 * 60 * 1000));

                    copiedEventObject.start = startDate;

                    endDate = new Date(startDate);                
                    endDate.setHours(startDate.getHours()+1);
                }else{
                    view = "week";
                    copiedEventObject.start = date;

                    edate = $.fullCalendar.moment(date.format());
                    endDate = new Date(edate);                
                    endDate.setHours(endDate.getHours()+1);
                }
                
                copiedEventObject.end = endDate;
                copiedEventObject.allDay = false;  //< -- only change
                copiedEventObject.backgroundColor = $(this).css("background-color");
                
                var sDate = date.format().split("T");
                var taskDate, taskStartTime = "";

                if(calendarView.name == "month"){
                    
                    taskDate = sDate[0];
                    taskStartTime = startDate.getHours() + ":" + startDate.getMinutes();
                    
                }else{
                    taskDate = sDate[0];
                    taskStartTime = sDate[1];                    
                }
                var eDate = endDate.getHours() + ":" + endDate.getMinutes();
                
                //$('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                $.ajax({
                    url: '<?php echo base_url('dashboard/saveQuickTask'); ?>',
                    type: "POST",
                    dataType: "JSON",
                    data: {"quickClientId": $(this).attr('data-client'), "quickCodeId": $(this).attr('data'),
                            "quickActivity": 1 , "quickTaskDate": taskDate, "calendarView" : view,
                            "quickTaskDuration": "01:00", "quickTaskStartTime": taskStartTime,
                            "quickTaskEndTime": eDate, "quickTaskColor":  $(this).attr('data-color')
                            },
                    success: function (response) {
                        // copiedEventObject.id = response;
                        $('#calendar').fullCalendar('refetchEvents');
                        // alert("done");
                    }

                });

                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                

            },
            eventResize: function (event, delta, revertFunc) {

                var eventEndArr = event.end.format().split("T");
                var endDate = eventEndArr[0];
                var endTime = eventEndArr[1];

                var eventStartArr = event.start.format().split("T");
                var startDate = eventStartArr[0];
                var startTime = eventStartArr[1];

                $.ajax({
                    url: '<?php echo base_url('dashboard/updateTaskTiming'); ?>',
                    type: "POST",
                    dataType: "JSON",
                    data: {"taskId": event.id, "taskStartTime": startTime, "taskEndTime": endTime, "taskDate": startDate},
                    success: function (response) {
                        
                    }

                });

            },
            eventDrop: function (event, jsEvent, ui, view) {

                var eventEndArr = event.end.format().split("T");
                var endDate = eventEndArr[0];
                var endTime = eventEndArr[1];

                var eventStartArr = event.start.format().split("T");
                var startDate = eventStartArr[0];
                var startTime = eventStartArr[1];

                $.ajax({
                    url: '<?php echo base_url('dashboard/updateTaskTiming'); ?>',
                    type: "POST",
                    dataType: "JSON",
                    data: {"taskId": event.id, "taskStartTime": startTime, "taskEndTime": endTime, "taskDate": startDate},
                    success: function (response) {
                        // alert("Done");
                    }

                });
            },
            dayRender: function (date, cell) {
                var day = date.format();

                if (day == "2015-11-27") {
                    $(cell).addClass('disabled');
                }
            },
            eventClick: function (event, jsEvent, view) {

                $.ajax({
                    url: '<?php echo base_url('dashboard/getEditTaskDetails'); ?>',
                    type: "POST",
                    dataType: "JSON",
                    data: {"taskId": event.id},
                    success: function (response) {

                        if (response) {
                            //Display Edit Task modal
                            $('#deleteModal').modal();

                            //Apply chosen for select options on modal
                            $('#deleteModal').on('shown.bs.modal', function () {
                                $('#optEditTimeSheetCode, #optEditTimeSheetTime, #optEditTimeSheetClient, #optEditTimeSheetActivity', this).chosen();
                            });
                            //Adding values to form fields
                            $("#optEditTimeSheetCode").empty();
                            $("#optEditTimeSheetCode").append("<option value=''>Select Code</option>");
                            $.each(response.codeData, function (index, itemData) {
                                var codeSelected = "";
                                if (itemData.id == response.timesheetData.codeId) {
                                    codeSelected = "selected=selected";
                                }

                                $("#optEditTimeSheetCode").append("<option value='" + itemData.id + "' " + codeSelected + ">" + itemData.pCode + "</option>");

                            });
                            $('#optEditTimeSheetCode').trigger("chosen:updated");


                            $('#txtEditTimeSheetId').val(response.timesheetData.taskId);
                            $('#taskDelete').attr("data-id", response.timesheetData.taskId);
                            $('#txtEditTimeSheetStart').val(response.timesheetData.taskStartTime);
                            $('#txtEditTimeSheetEnd').val(response.timesheetData.taskEndTime);
                            $('#txtEditTimeSheetDate').val(response.timesheetData.taskDate);
                            $('#rtEditTimeSheetDescription').text(response.timesheetData.taskDescription);

                            $("#optEditTimeSheetClient option").filter(function () {

                                return $(this).val() == response.timesheetData.clientId;
                            }).prop('selected', true);

                            $("#optEditTimeSheetActivity option").filter(function () {

                                return $(this).val() == response.timesheetData.activityId;
                            }).prop('selected', true);

                            $("#optEditTimeSheetTime option").filter(function () {

                                return $(this).val() == response.timesheetData.taskDuration;
                            }).prop('selected', true);

                            $('#optEditTimeSheetClient').trigger("chosen:updated");
                            $('#optEditTimeSheetActivity').trigger("chosen:updated");
                            $('#optEditTimeSheetTime').trigger("chosen:updated");

                        }
                    }

                });
            }
        });

        //Delete Task Code
        $("#taskDelete").click(function () {
            $.ajax({
                url: '<?php echo base_url('dashboard/deleteTask'); ?>',
                type: "POST",
                dataType: "JSON",
                data: {"taskId": $("#taskDelete").attr("data-id")},
                beforeSend: function() { 
                    $('#editTaskLoader').css("display", "inline-block");                        
                },
                success: function (response) {
                    if (response) {
                        $('#editTaskLoader').css("display", "none");
                        $('#deleteModal').modal('toggle');
                        $('#calendar').fullCalendar('refetchEvents');
                    }
                }

            });
        });


        //Edit Task Details
        $("#frmEditUserTimesheet").submit(function () {
            if($("#frmEditUserTimesheet").valid()){
                $.ajax({
                    url: '<?php echo base_url('dashboard/updateTask'); ?>',
                    type: "POST",
                    dataType: "JSON",
                    data: $("#frmEditUserTimesheet").serialize(),
                    beforeSend: function() { 
                        $('#editTaskLoader').css("display", "inline-block");                        
                    },
                    success: function (response) {
                        if (response) {
                             $('#editTaskLoader').css("display", "none");
                            $('#deleteModal').modal('toggle');
                            $('#calendar').fullCalendar('refetchEvents');
                        }
                    }

                });
                return false;
            }
        });

        $("#frmUserTimesheet").validate({
            errorClass: "form-error",
            errorElement: "span",
            rules: {
                optTimeSheetClient: {
                    required: true
                },
                optTimeSheetCode: {
                    required: true
                }
            },
            messages: {
                optTimeSheetClient: {
                    required: "Client Name is required!!"
                },
                optTimeSheetCode: {
                    required: "Code is required!!"
                },
                optTimeSheetActivity: {
                    required: "Activity is required!!"
                },
                txtTimeSheetDate: {
                    required: "Date is required!!"
                },
                optTimeSheetTime: {
                    required: "Time Duration is required!!"
                }
            },
            errorPlacement: function (error, element) {
                if (element.attr("name") == "optTimeSheetClient") {
                    error.insertAfter($("#optTimeSheetClient_chosen"));
                } else if (element.attr("name") == "optTimeSheetCode") {
                    error.insertAfter($("#optTimeSheetCode_chosen"));
                } else if (element.attr("name") == "optTimeSheetActivity") {
                    error.insertAfter($("#optTimeSheetActivity_chosen"));
                } else if (element.attr("name") == "txtTimeSheetDate") {
                    error.insertAfter($(".taskDate"));
                } else if (element.attr("name") == "optTimeSheetTime") {
                    error.insertAfter($("#optTimeSheetTime_chosen"));
                } else {
                    error.insertAfter(element);
                }
            }
        });


         $("#btnTimesheet").click(function () {
            
            $("[name='optTimeSheetActivity'], [name='txtTimeSheetDate'], [name='optTimeSheetTime']").each(function () {
                $(this).rules('add', {
                    required: true
                });
            });

            if($("#frmUserTimesheet").valid()){
                $.ajax({
                    url: '<?php echo base_url('dashboard/saveTask'); ?>',
                    type: "POST",
                    dataType: "JSON",
                    data: $("#frmUserTimesheet").serialize(),
                    beforeSend: function() { 
                        $('#taskLoader').css("display", "block");
                    },
                    success: function (response) {
                        if (response) {
                            $('#taskLoader').css("display", "none");
                            $('#calendar').fullCalendar('refetchEvents');
                            $('#frmUserTimesheet')[0].reset();
                            $('#optTimeSheetClient, #optTimeSheetCode, #optTimeSheetActivity, #optTimeSheetTime').trigger("chosen:updated");
                        }
                    }
                });
                return false;
            }
        });

        


        //Add Quick list codes
        $("#btnQuickList").click(function () {
            
            $("[name='optTimeSheetActivity'], [name='txtTimeSheetDate'], [name='optTimeSheetTime']").each(function () {
                $(this).rules('remove');
            });
            if($("#frmUserTimesheet").valid()){
                $.ajax({
                    url: '<?php echo base_url('dashboard/addToQuickList'); ?>',
                    type: "POST",
                    dataType: "JSON",
                    data: {"clientId": $("#optTimeSheetClient").val() , "codeId" : $("#optTimeSheetCode").val() },
                    beforeSend: function() { 
                        $('#addQuickLoader').css("display", "block");
                    },
                    success: function (response) {
                        $.ajax({
                            url: '<?php echo base_url('dashboard/getQuickListData'); ?>',
                            type: "POST",
                            dataType: "JSON",
                            success: function (response) {
                                $('#addQuickLoader').css("display", "none");
                                var txt = "";

                                $.each(response, function (index, itemData) {
                                    if (itemData.codeType == "project") {
                                        background_color = "#fa5858";
                                    } else if (itemData.codeType == "retainer") {
                                        background_color = "#c317b4";
                                    } else if (itemData.codeType == "corporate") {
                                        background_color = "#00a7e5";
                                    }
                                    txt += '<div class="external-event" data-color="'+ background_color +'" data-client="'+ itemData.clientId +'" data="'+itemData.codeId+'" style="color:#fff; background-color:'+ background_color +'" >'+ itemData.productCode +'<a class="pull-right" style="color:#000" onclick="deleteQuickList(\''+ itemData.clientId + "_" + itemData.codeId  +'\')" href="javascript:void(0)">X</a> </div>';
                                });
                                $("#external-events").html(txt);
                                ini_events($('#external-events div.external-event'));

                            }
                        });
                        return false;
                    }
                });
                return false;
            }


        });

        

       



    });

    function deleteQuickList(quickListId){                
        $.ajax({
            url: '<?php echo base_url('dashboard/deleteQuickList'); ?>',
            type: "POST",
            dataType: "JSON",
            data: {"quickListId": quickListId },
            beforeSend: function() { 
                $('#quickLoader').css("display", "block");
            },
            success: function (response) {
                $.ajax({
                    url: '<?php echo base_url('dashboard/getQuickListData'); ?>',
                    type: "POST",
                    dataType: "JSON",
                    success: function (response) {
                        $('#quickLoader').css("display", "none");
                        var txt = "";

                        $.each(response, function (index, itemData) {
                            if (itemData.codeType == "project") {
                                background_color = "#fa5858";
                            } else if (itemData.codeType == "retainer") {
                                background_color = "#c317b4";
                            } else if (itemData.codeType == "corporate") {
                                background_color = "#00a7e5";
                            }
                            txt += '<div class="external-event" data-color="'+ background_color +'" data-client="'+ itemData.clientId +'" data="'+itemData.codeId+'" style="color:#fff; background-color:'+ background_color +'" >'+ itemData.productCode +'<a class="pull-right" style="color:#000" onclick="deleteQuickList(\''+ itemData.clientId + "_" + itemData.codeId  +'\')" href="javascript:void(0)">X</a> </div>';
                        });
                        $("#external-events").html(txt);
                        ini_events($('#external-events div.external-event'));
                    }
                });
            }
        });
        return false;
    }

</script>