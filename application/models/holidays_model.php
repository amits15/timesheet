<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Holidays_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /*
     * function name : getHolidaysData
     * 
     * Fetch Holidays details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : none
     * @return : Array $data
     */

    public function getHolidaysData() {

        $this->db->order_by('holiday_date', 'desc');
        $query = $this->db->get('holidays_data');

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    /*
     * function name : getYearlyHolidays
     * 
     * Fetch Yearly Holidays list
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : string $year
     * @return : Array $data
     */

    public function getYearlyHolidays($year) {

        if($year){
            $this->db->where("YEAR(holiday_date)", $year);
        }else{
            $year = date("Y");
            $this->db->where("YEAR(holiday_date)", $year);
        }

        $query = $this->db->get('holidays_data');
        // echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    /*
     * function name : getAllHolidayYears
     * 
     * Fetch Holidays Years list
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : 
     * @return : Array $data
     */

    public function getAllHolidayYears() {

        $this->db->select("DISTINCT(YEAR(holiday_date)) as year", false);
        $this->db->order_by('YEAR(holiday_date)', 'desc');
        $query = $this->db->get('holidays_data');
        // echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    

    /*
     * function name : getHolidayDetails
     * 
     * Fetch particular Holiday details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : int $holidayId
     * @return : Array $data
     */

    public function getHolidayDetails($holidayId) {

        $this->db->where("id", $holidayId);
        $query = $this->db->get('holidays_data');

        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    /*
     * function name : addHoliday
     * 
     * Save Holiday details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : array $insertData
     * @return : bool true|false
     */

    public function addHoliday($insertData) {

        $this->db->insert('holidays_data', $insertData);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * function name : editHoliday
     * 
     * update Holiday details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : array $updateData, int $holidayId
     * @return : bool true|false
     */

    public function editHoliday($updateData, $holidayId) {


        $this->db->update('holidays_data', $updateData, array("id" => $holidayId));
        // echo $this->db->last_query();exit;

        return true;
    }
    
    
    /*
     * function name : deleteHoliday
     * 
     * Delete Holidays details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : int $holidayId
     * @return : bool true|false
     */

    public function deleteHoliday($holidayId) {

        $this->db->delete('holidays_data', array("id" => $holidayId));
        return true;
    }

}
