<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Location_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /*
     * function name : getLocationData
     * 
     * Fetch Locations details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : none
     * @return : Array $data
     */

    public function getLocationData() {

        $query = $this->db->get('location_data');

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    /*
     * function name : getActiveClientData
     * 
     * Fetch Active Location details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : none
     * @return : Array $data
     */

    public function getActiveLocationData() {

        $this->db->where("status", "1");
        $query = $this->db->get('location_data');

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    /*
     * function name : getLocationDetails
     * 
     * Fetch particular Location details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : int $locationId
     * @return : Array $data
     */

    public function getLocationDetails($locationId) {

        $this->db->where("id", $locationId);
        $query = $this->db->get('location_data');

        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    /*
     * function name : addLocation
     * 
     * Save Location details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : array $insertData
     * @return : bool true|false
     */

    public function addLocation($insertData) {

        $this->db->insert('location_data', $insertData);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * function name : editLocation
     * 
     * update Location details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : array $updateData, int $locationId
     * @return : bool true|false
     */

    public function editLocation($updateData, $locationId) {


        $this->db->update('location_data', $updateData, array("id" => $locationId));

        return true;
    }
    
    
    /*
     * function name : deleteLocation
     * 
     * Delete Location details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : int $locationId
     * @return : bool true|false
     */

    public function deleteLocation($locationId) {

        $this->db->delete('location_data', array("id" => $locationId));
        return true;
    }

}
