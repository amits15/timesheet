<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /*
     * function name : getTotalWorkingDays
     * 
     * Fetch total working days based on holidays.
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : string $startDate, string $endDate
     * @return : Array $data
     */

    public function getTotalWorkingDays($startDate, $endDate) {

        $this->db->where("holiday_date >=", $startDate);
        $this->db->where("holiday_date <=", $endDate);
        $query = $this->db->get("holidays_data");
//echo $this->db->last_query();exit;
        return $query->num_rows();
    }

    /*
     * function name : getEmployeeData
     * 
     * Fetch Employees based on department.
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : string $reportDept
     * @return : Array $data
     */

    public function getEmployeeData($reportDept) {

        $this->db->select("u.*, dp.dept_name, dp.id as dept_id");
        $this->db->join('dept_data as dp', 'dp.id = u.department', 'LEFT');
        if ($reportDept != "all" && $reportDept != "") {
            $this->db->where("dp.id", $reportDept);
        }
        $this->db->where("u.status", "1");
        // $this->db->limit(10);
        $query = $this->db->get("user_data as u");

        // echo "<pre>";
        // print_r($query->result_array());
        // exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    /*
     * function name : getCodeData
     * 
     * Fetch Code based on category.
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : string $reportCategory, string $reportLocation
     * @return : Array $data
     */

    public function getCodeData($reportCategory, $reportLocation) {


        $this->db->select("cd.*, cl.client_name, cd.product_code as pCode", FALSE);
        $this->db->from("code_data cd");
        $this->db->join("client_data cl", "cd.client_id = cl.id", 'Left');
        $this->db->join("location_data l", "cd.location = l.id", 'Left');
        if ($reportCategory != "all" && $reportCategory != "") {
            $this->db->where("type", $reportCategory);
        }
        if ($reportLocation != "all" && $reportLocation != "") {
            $this->db->where("cd.location", $reportLocation);
        }
        $this->db->order_by("cd.type, cd.subtype");
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    /*
     * function name : getCodeDataWithoutCorp
     * 
     * Fetch Code based on category.
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : string $reportCategory, string $reportLocation
     * @return : Array $data
     */

    public function getCodeDataWithoutCorp($reportLocation) {


        $this->db->select("cd.*, cl.client_name, cd.product_code as pCode", FALSE);
        $this->db->from("code_data cd");
        $this->db->join("client_data cl", "cd.client_id = cl.id", 'Left');
        $this->db->join("location_data l", "cd.location = l.id", 'Left');
        $this->db->where("type != ", "corporate");
        if ($reportLocation != "all" && $reportLocation != "") {
            $this->db->where("cd.location", $reportLocation);
        }
        $this->db->order_by("cd.type, cd.subtype");
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    /*
     * function name : getUserTimesheetReport
     * 
     * Fetch Users Tasks details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : $codeId, $userId, $startDate, $endDate
     * @return : Array $data
     */

    public function getUserTimesheetReport($codeId, $userId, $startDate, $endDate) {


        $this->db->select("td.*,LOWER(cd.type) as type , LOWER(cd.subtype) as subtype, cd.product_code as pCode", FALSE);
        $this->db->from("time_data td");
        $this->db->join("code_data cd", "td.code_id = cd.id", 'Left');
        $this->db->join("activity_data ad", "td.activity_id = ad.id", 'Left');
        $this->db->join("location_data l", "cd.location = l.id", 'Left');
        $this->db->join("client_data cl", "td.client_id = cl.id", 'Left');
        $this->db->join("user_data ud", "td.employee_id = ud.id", 'Left');
        $this->db->where("td.employee_id", $userId);
        $this->db->where("td.code_id", $codeId);
        $this->db->where("td.task_date >=", "$startDate");
        $this->db->where("td.task_date <=", "$endDate");
        $query = $this->db->get();

//        echo $this->db->last_query() ."<br>";
//        exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    /*
     * function name : getUserTimesheetReportByEmp
     * 
     * Fetch Users Tasks details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : $userId, $startDate, $endDate
     * @return : Array $data
     */

    public function getUserTimesheetReportByEmp($userId, $startDate, $endDate) {


        $this->db->select("td.*,LOWER(cd.type) as type , LOWER(cd.subtype) as subtype, cd.product_code as pCode", FALSE);
        $this->db->from("time_data td");
        $this->db->join("code_data cd", "td.code_id = cd.id", 'Left');
        $this->db->join("activity_data ad", "td.activity_id = ad.id", 'Left');
        $this->db->join("location_data l", "cd.location = l.id", 'Left');
        $this->db->join("client_data cl", "td.client_id = cl.id", 'Left');
        $this->db->join("user_data ud", "td.employee_id = ud.id", 'Left');
        $this->db->where("td.employee_id", $userId);
        $this->db->where("td.task_date >=", "$startDate");
        $this->db->where("td.task_date <=", "$endDate");
        $query = $this->db->get();

//        echo $this->db->last_query() ."<br>";
//        exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function updateCode($code, $codeId){
        $this->db->where("id", $codeId);
        $this->db->update("code_data", $code);
    }

}
