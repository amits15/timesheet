<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profile extends CI_Controller {
    /*
     * function name :__construct
     *  Counstructor for profile controller 
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->model('profile_model');
        $this->load->model('emp_model');
        $this->load->model('location_model');
    }

    /*
     * function name :index
     *  Shows the user profile
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : int $userId
     * @return : void
     */

    public function index($userId = NULL) {

        if ($this->session->userdata('userid')) {

            $data = array(
                "title" => "Profile"
            );

            if ($userId) {
                $data['user'] = $this->profile_model->getUserProfile($userId);
            } else {
                redirect("dashboard");
            }
            $data['locations'] = $this->location_model->getActiveLocationData();

            $this->load->view('profile/userProfile', $data);
        } else {
            redirect("login");
        }
    }

    /*
     * function name :checkOldPwd
     * Checks user old password is matched or not
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : bool true|false
     */

    public function checkOldPwd() {

        if ($this->session->userdata('userid')) {

            $userId = $this->input->post("userId");
            $oldPwd = $this->input->post("txtUserOldPwd");

            $result = $this->profile_model->checkOldPwd($userId, $oldPwd);

            if ($result) {
                echo "true";
            } else {
                echo "false";
            }
            exit;
        } else {
            redirect("login");
        }
    }

    /*
     * function name :changePassword
     *  Change the user Password
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    public function changePassword() {

        if ($this->session->userdata('userid')) {

            if ($_POST) {

                $userId = $this->input->post("txtUserId");
                $oldPwd = $this->input->post("txtUserOldPwd");
                $newPwd = $this->input->post("txtUserNewPwd");
                $confirmPwd = $this->input->post("txtUserConfirmPwd");

                if ($newPwd == $confirmPwd) {
                    $updateData = array(
                        "password" => md5($newPwd)
                    );
                }
                $this->profile_model->changePassword($updateData, $userId);
            }
            $this->session->set_flashdata('success_msg', "Password updated successfully...After 5 sec you will be automatically redirected to login screen.");
            redirect("profile/index/$userId");
        } else {
            redirect("login");
        }
    }

    /*
     * function name :updateProfile
     *  Update user profile details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    public function updateProfile() {

        if ($this->session->userdata('userid')) {

            if ($_POST) {
                
                $userId = $this->input->post("txtUserProfileId");
                

                $updateData = array(
                    "first_name" => $this->input->post("txtUserFirstName"),
                    "last_name" => $this->input->post("txtUserLastName"),
                    "phone" => $this->input->post("txtUserPhone"),
                    "gender" => $this->input->post("rdoUserGender"),
                    "date_of_birth" => $this->input->post("txtUserDOB")
                );
                
                $this->profile_model->updateProfile($updateData, $userId);
            }
            $this->session->set_flashdata('profile_msg', "Profile updated successfully.");
            redirect("profile/index/$userId");
        } else {
            redirect("login");
        }
    }

    /*
     * function name :changePhoto
     *  Change the user profile photo
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    public function changePhoto() {

        if ($this->session->userdata('userid')) {

            if ($_POST) {

                $userId = $this->input->post("userId");
                $data['user'] = $this->profile_model->getUserProfile($userId);



                if ($_FILES) {
                    $file_name = $this->uploadPhoto($_FILES['userPhoto']['name'], 'userPhoto');

                    if ($file_name) {
                        $updateData = array(
                            "profile_image" => $file_name
                        );
                    }
                    $this->profile_model->changePhoto($updateData, $userId);
                    unlink(getcwd() . "/uploads/profile/" . $data['user']['profile_image']);
                }
            }

            redirect("profile/index/$userId");
        } else {
            redirect("login");
        }
    }

    /*
     * function name :uploadPhoto
     *  Upload user profile pic
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : $fileName, $elementName, $userId
     * @return : void
     */

    public function uploadPhoto($fileName, $elementName) {

        if ($this->session->userdata('userid')) {

            if ($fileName) {
                $ext = end((explode(".", $fileName)));
                $config['upload_path'] = './uploads/profile/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|GIf|JPF|PNG|JPEG';
                $config['file_name'] = time() . ".$ext";

                $this->upload->initialize($config);

                if (!$this->upload->do_upload($elementName)) {

                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('upload_err', $error);
                    $userId = $this->input->post("userId");
                    redirect("profile/index/$userId");
                } else {
                    $data = $this->upload->data();
                    chmod($data['full_path'], 0777);
//                    echo "<pre>";
//                    print_r($data);exit;
                    return $data['file_name'];
                }
            }
        } else {
            redirect("login");
        }
    }

}

/* End of file login.php */
/* Location: ./application/controllers/login.php */