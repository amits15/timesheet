<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Client_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /*
     * function name : getClientData
     * 
     * Fetch Clients details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : none
     * @return : Array $data
     */

    public function getClientData() {

        $this->db->select('client_data.*');
        // $this->db->join('(SELECT @cnt := 0) AS dummy', 'true');
        $this->db->order_by('client_name');
        $query = $this->db->get('client_data');

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    /*
     * function name : getActiveClientData
     * 
     * Fetch Active Clients details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : none
     * @return : Array $data
     */

    public function getActiveClientData() {

        $this->db->where("status", "1");
        $this->db->order_by('client_name');
        $query = $this->db->get('client_data');

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    /*
     * function name : getClientDetails
     * 
     * Fetch particular Client details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : int $clientId
     * @return : Array $data
     */

    public function getClientDetails($clientId) {

        $this->db->where("id", $clientId);
        $query = $this->db->get('client_data');

        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    /*
     * function name : addClient
     * 
     * Save Clients details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : array $insertData
     * @return : bool true|false
     */

    public function addClient($insertData) {

        $this->db->insert('client_data', $insertData);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * function name : editClient
     * 
     * update Clients details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : array $updateData, int $clientId
     * @return : bool true|false
     */

    public function editClient($updateData, $clientId) {


        $this->db->update('client_data', $updateData, array("id" => $clientId));

        return true;
    }
    
    
    /*
     * function name : deleteClient
     * 
     * Delete Clients details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : int $clientId
     * @return : bool true|false
     */

    public function deleteClient($clientId) {

        $this->db->delete('client_data', array("id" => $clientId));
        return true;
    }

}
