<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /*
     * function name : getActivityData
     * 
     * Fetch Activity details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : none
     * @return : Array $data
     */

    public function getActivityData() {

        $query = $this->db->get('activity_data');

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    /*
     * function name : getHolidays
     * 
     * Fetch Holidays details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : none
     * @return : Array $data
     */

    public function getHolidays() {

        $this->db->select('holiday_date');
        $query = $this->db->get('holidays_data');

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    /*
     * function name : getClientCodes
     * 
     * Fetch Client code details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : none
     * @return : Array $data
     */

    public function getClientCodes($clientId) {


        $this->db->select("cd.*, cl.client_name, cd.product_code as pCode", FALSE);
        $this->db->from("code_data cd");
        $this->db->join("client_data cl", "cd.client_id = cl.id", 'Left');
        $this->db->join("location_data l", "cd.location = l.id", 'Left');
        $this->db->where("cd.client_id", $clientId);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    /*
     * function name : saveTask
     * 
     * Save the user task details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : array $insertData
     * @return : bool true | false
     */

    public function saveTask($insertData) {

        $query = $this->db->insert("time_data", $insertData);

        if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }
    
    /*
     * function name : updateTask
     * 
     * Update the user task details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : array $updateData, int $taskId
     * @return : bool true | false
     */

    public function updateTask($updateData, $taskId) {

        $this->db->where("id", $taskId);
        $query = $this->db->update("time_data", $updateData);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    /*
     * function name : updateTaskTiming
     * 
     * Update the user task timings.
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : array $updateData, int $taskId
     * @return : bool true | false
     */

    public function updateTaskTiming($updateData, $taskId) {

        $this->db->where("id", $taskId);
        $query = $this->db->update("time_data", $updateData);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    
    /*
     * function name : deleteTask
     * 
     * Delete the user task details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : int $taskId
     * @return : bool true | false
     */

    public function deleteTask($taskId) {

        $this->db->where("id", $taskId);
        $query = $this->db->delete("time_data");

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * function name : addToQuickList
     * 
     * Add Code and client to user quick list.
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : int $userId, String $quickListCode
     * @return : bool true | false
     */

    public function addToQuickList($userId, $quickListCode) {

        $this->db->where("id", $userId);
        $selQuery = $this->db->get("user_data");
        $selRow = $selQuery->row_array();
        if($selRow['quick_list']){

            $quickListCode  = $selRow['quick_list'] . ",$quickListCode";
        }else{
            
            $quickListCode  = $quickListCode;
        }

        $this->db->where("id", $userId);
        $query = $this->db->update("user_data", array("quick_list" => $quickListCode) );
        // echo $this->db->last_query();exit;
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * function name : updateQuickList
     * 
     * Add Code and client to user quick list.
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : int $userId, String $quickListCode
     * @return : bool true | false
     */

    public function updateQuickList($userId, $quickListCode) {

        $this->db->where("id", $userId);
        $query = $this->db->update("user_data", array("quick_list" => $quickListCode) );
        // echo $this->db->last_query();exit;
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * function name : getQuickListData
     * 
     * Get user quick list codes
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : int $userId
     * @return : array $data
     */

    public function getQuickListData($userId) {

        $this->db->where("id", $userId);
        $selQuery = $this->db->get("user_data");
        $selRow = $selQuery->row_array();
        $quickListArray = array();
        if($selRow['quick_list']){
            

            $arrQuickList = explode(",", $selRow['quick_list']);

            
            foreach ($arrQuickList as $key => $quickVal) {
                $quickClientCode = explode("_", $quickVal);


                $clientQuery = $this->db->get_where("client_data", array("id" => $quickClientCode[0]));
                $clientResult = $clientQuery->row_array();

                $codeQuery = $this->db->get_where("code_data", array("id" => $quickClientCode[1]));
                $codeResult = $codeQuery->row_array();

                $quickListArray[] = array(
                    "codeId" => $codeResult['id'],
                    "productCode" => $codeResult['product_code'],
                    "clientId" => $clientResult['id'],
                    "clientName" => $clientResult['client_name'],
                    "codeType" => $codeResult['type']

                ); 

            }
        }
        
        
        return $quickListArray;
    }


    /*
     * function name : getQuickListCodesByUser
     * 
     * Get user quick list codes
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : int $userId
     * @return : array $data
     */

    public function getQuickListCodesByUser($userId) {

        $this->db->where("id", $userId);
        $selQuery = $this->db->get("user_data");
        $selRow = $selQuery->row_array();
        
        return $selRow;
    }
    
    
    
    /*
     * function name : getUserTasks
     * 
     * Fetch Users Tasks details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : none
     * @return : Array $data
     */

    public function getUserTasks($userId, $startDate, $endDate) {


        $this->db->select("ad.*, td.*, td.id as taskId, cd.*, cl.client_name, cd.product_code as pCode", FALSE);
        $this->db->from("time_data td");
        $this->db->join("code_data cd", "td.code_id = cd.id", 'Left');
        $this->db->join("activity_data ad", "td.activity_id = ad.id", 'Left');
        $this->db->join("client_data cl", "td.client_id = cl.id", 'Left');
        $this->db->join("location_data l", "cd.location = l.id", 'Left');
        $this->db->join("user_data ud", "td.employee_id = ud.id", 'Left');
        $this->db->where("ud.id", $userId);
       $this->db->where("td.task_date >=", $startDate);
       $this->db->where("td.task_date <", $endDate);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    
    /*
     * function name : getEditTaskDetails
     * 
     * Fetch Tasks details for update
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : int $taskId
     * @return : array $data
     */

    public function getEditTaskDetails($taskId) {


        $this->db->select("ad.activity, ad.id as activityId, td.*, td.id as taskId, cl.client_name, cl.id as clientId, cd.id as codeId, cd.product_code as pCode", FALSE);
        $this->db->from("time_data td");
        $this->db->join("code_data cd", "td.code_id = cd.id", 'Left');
        $this->db->join("activity_data ad", "td.activity_id = ad.id", 'Left');
        $this->db->join("client_data cl", "td.client_id = cl.id", 'Left');
        $this->db->join("location_data l", "cd.location = l.id", 'Left');
        $this->db->join("user_data ud", "td.employee_id = ud.id", 'Left');
        $this->db->where("td.id", $taskId);
        $query = $this->db->get();
        

        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }
    
    
    
    /*
     * function name : getUserLatestFilledTask
     * 
     * Fetch Users Latest Tasks Timing details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : none
     * @return : Array $data
     */

    public function getUserLatestFilledTask($date, $userId) {
        $this->db->select('max(task_end_time) as task_end');
        $this->db->where("employee_id", $userId);
        $this->db->where("task_date", $date);
        $query = $this->db->get('time_data');
        $row = $query->row_array();
//        print_r($query->row_array());exit;
        if ($row['task_end']) {
            return $row;
        } else {
            return false;
        }
    }

}
