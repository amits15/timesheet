<?php echo $this->load->view("common/header", $title); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add Holiday
        </h1>
        <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Master</li>
            <li><a href="#"></i> Holiday</a></li>          
            <li><a href="#"></i> Add</a></li>          
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                        <a href="javascript:history.back()" class="btn btn-link pull-right"><i class="fa fa-arrow-left" ></i> Back</a>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="<?php echo base_url("holidays/add"); ?>" method="POST" name="frmHolidays" id="frmHolidays">
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Date <span class="text-red">*</span></label>
                                <div class="col-sm-3">
                                    <div class="input-group date holidayDate">
                                        <input type="text" name="txtHolidayDate" id="txtHolidayDate" class="form-control">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                    </div>
                                    <?php echo form_error('txtHolidayDate', '<div class="form-error">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="inputEmail3">Description <span class="text-red">*</span></label>
                                <div class="col-sm-4">
                                    <input type="text" placeholder="Description" id="txtHolidayDesc" name="txtHolidayDesc" class="form-control">
                                    <?php echo form_error('txtHolidayDesc', '<div class="form-error">', '</div>'); ?>
                                </div>
                            </div>                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div><!-- /.box -->

            </div><!--/.col (left) -->

        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php echo $this->load->view("common/footer"); ?>
<script src="<?php echo base_url(); ?>assets/plugins/jValidate/formValidations.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>

<script>
    $('.input-group.date').datepicker({
        format: "yyyy-mm-dd"
    });
</script>