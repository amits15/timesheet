<?php echo $this->load->view("common/header", $title); ?>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/minimal/_all.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add Code
        </h1>
        <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Master</li>
            <li><a href="#"></i> Code</a></li>          
            <li><a href="#"></i> Add</a></li>          
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                        <a href="javascript:history.back()" class="btn btn-link pull-right"><i class="fa fa-arrow-left" ></i> Back</a>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="<?php echo base_url("code/add"); ?>" method="POST" id="frmCode">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Job Code <span class="text-red">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" name="txtJobCode" id="txtJobCode" class="form-control">
                                            <?php echo form_error('txtJobCode', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Client Name <span class="text-red">*</span></label>
                                        <div class="col-sm-6">
                                            <select class="form-control col-sm-2" id="optCodeClientName" name="optCodeClientName">
                                                <option value="">Select Client</option>
                                                <?php foreach ($clients as $key => $client) { ?>
                                                    <option value="<?php echo $client['id'] ?>" ><?php echo $client['client_name'] ?></option>
                                                <?php } ?>
                                            </select>
                                            <?php echo form_error('optCodeClientName', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="inputEmail3">Location <span class="text-red">*</span></label>
                                        <div class="col-sm-6">
                                            <select class="form-control col-sm-2" id="optCodeLocation" name="optCodeLocation">
                                                <option value="">Select Location</option>
                                                <?php foreach ($locations as $key => $location) { ?>
                                                    <option value="<?php echo $location['id'] ?>" ><?php echo $location['location_name'] ?></option>
                                                <?php } ?>
                                            </select>
                                            <?php echo form_error('optCodeLocation', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Type <span class="text-red">*</span></label>
                                        <div class="col-sm-6">
                                            <select class="form-control col-sm-2" name="optCodeClientType" id="optCodeClientType">
                                                <option value="">Select Type</option>
                                                <option value="project">Project</option>
                                                <option value="retainer">Retainer</option>
                                                <option value="corporate">Corporate</option>
                                            </select>
                                            <?php echo form_error('optCodeClientType', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Sub Type <span class="text-red">*</span></label>
                                        <div class="col-sm-6">
                                            <select class="form-control col-sm-2" name="optCodeClientSubType" id="optCodeClientSubType">
                                                <option value="">Select Sub Type</option>
                                            </select>
                                            <?php echo form_error('optClientSubType', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Suffix <span class="text-red">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" placeholder="Code Suffix" id="txtCodeSuffix" name="txtCodeSuffix" class="form-control">
                                            <?php echo form_error('txtCodeSuffix', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Product Code</label>
                                        <div class="col-sm-6">
                                            <input type="text" name="txtProductCode" readonly="" id="txtProductCode" class="form-control">
                                            <?php echo form_error('txtProductCode', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>                                    
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Start Date</label>
                                        <div class="col-sm-6">
                                            <div class="input-group date">
                                                <input type="text" name="txtCodeStartDate" id="txtCodeStartDate" class="form-control">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                            </div>
                                            <?php echo form_error('txtCodeStartDate', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>                                    
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">End Date</label>
                                        <div class="col-sm-6">
                                            <div class="input-group date">
                                                <input type="text" name="txtCodeEndDate" id="txtCodeEndDate" class="form-control">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                            </div>
                                            <?php echo form_error('txtCodeEndDate', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Active</label>
                                        <div class="col-sm-6">
                                            <input class="minimal" type="checkbox" name="chkClientStatus" id="chkClientStatus" value="1" checked="" /> 
                                            <?php echo form_error('chkClientStatus', '<div class="form-error">', '</div>'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div><!-- /.box -->

            </div><!--/.col (left) -->

        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php echo $this->load->view("common/footer"); ?>
<script src="<?php echo base_url(); ?>assets/plugins/jValidate/formValidations.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js"></script>
<script>
    $("#optCodeClientName").chosen();

    var productOptions = '<option value="">Select Sub Type</option><option value="video">Video</option><option value="tech">Tech</option><option value="mediabuying">Media Buying</option><option value="strategy">Strategy</option>'
    var retainerOptions = '<option value="">Select Sub Type</option><option value="retainer">Retainer</option>';
    var corpOptions = '<option value="">Select Sub Type</option><option value="freetime">Freetime</option><option value="internal">Internal</option><option value="training">Training</option><option value="pitch">Pitch</option>';

    $("#optCodeClientType").change(function () {

        if ($(this).val() == "project") {
            $("#optCodeClientSubType").html(productOptions);
        } else if ($(this).val() == "retainer") {
            $("#optCodeClientSubType").html(retainerOptions);
        } else if ($(this).val() == "corporate") {
            $("#optCodeClientSubType").html(corpOptions);
        } else {
            $("#optCodeClientSubType").html('<option value="">Select Sub Type</option>');
        }

    });

    $("#optCodeClientName, #optCodeClientType, select[name='optCodeClientSubType'], #optCodeLocation , #txtCodeSuffix").change(function () {
        var clientName = "";
        var type = "";
        var subType = "";
        var suffix = "";
        var location = "";
        if ($("#optCodeClientName option:selected").text() != "Select Client") {
            clientName = $("#optCodeClientName option:selected").text().toLowerCase().split(" ").join("");
        }
        if ($("#optCodeClientType").val() != "") {
            type = $("#optCodeClientType").val().toLowerCase().substring(0, 4);
        }
        if ($("select[name='optCodeClientSubType']").val() != "") {
            subType = $("select[name='optCodeClientSubType']").val().toLowerCase();
        }
        if ($("#txtCodeSuffix").val() != "") {
            suffix = $("#txtCodeSuffix").val().toLowerCase().split(" ").join("_");
        }
        if ($("#optCodeLocation").val() != "") {
            location = $("#optCodeLocation option:selected").text().toLowerCase();
        }

        var pCode = clientName + "-" + location + "-" + type + "-" + subType + "-" + suffix;
        $("#txtProductCode").val(pCode);
    });

    $('.input-group.date').datepicker({
        format: "yyyy-mm-dd"
    });

    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_minimal-blue'
        });
    });
</script>