<?php echo $this->load->view("common/header", $title); ?>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker-bs3.css">


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Report with Original Data
        </h1>
        <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Master</li>
            <li><a href="#"></i> Reports</a></li>          
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <form class="form-horizontal" action="<?php echo base_url("reports/userOriginalData") ?>" method="POST" id="frmUserReports" >
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Filter Report</h3>
                        </div><!-- /.box-header -->

                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label>Department:</label>
                                            <div class="input-group">
                                                <select id="optReportDepartment" name="optReportDepartment" class="form-control">
                                                    <option value="all">All Departments</option>
                                                    <?php if($departments){
                                                        foreach($departments as $department){ ?>
                                                            <option value="<?php echo $department['id'] ?>" ><?php echo $department['dept_name'] ?></option>
                                                       <?php }
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label>Category:</label>
                                            <div class="input-group">
                                                <select id="optReportCategory" name="optReportCategory" class="form-control">
                                                    <option value="all">All Categories</option>
                                                    <option value="project">project</option>	
                                                    <option value="retainer">retainer</option>	
                                                    <option value="corporate">corporate</option>	
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label>Location:</label>
                                            <div class="input-group">
                                                <select id="optReportLocation" name="optReportLocation" class="form-control">
                                                    <option value="">All Locations</option>
                                                    <?php if($locations){
                                                        foreach($locations as $location){ ?>
                                                            <option value="<?php echo $location['id'] ?>" ><?php echo $location['location_name'] ?></option>
                                                       <?php }
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label>Date range:</label>
                                            <div class="input-group col-xs-11 taskDate">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" id="txtReportDateRange" name="txtReportDateRange" class="form-control pull-right">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="form-group">
                                            <label> IN Percent(%) / Hours(HH):</label>
                                            <div class="input-group">
                                                <input type="radio" id="rdoPercentageFlagYes" name="rdoPercentageFlag" class="" value="1">&nbsp; Percent &nbsp;&nbsp;&nbsp;
                                                <input type="radio" id="rdoPercentageFlagNo" checked name="rdoPercentageFlag" class="" value="0">&nbsp; Hours
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="form-group pull-right">
                                            <br/>
                                            <span id="loaderImg" style="display: none;" > <img src="<?php echo base_url("assets/dist/img/Preloader_9.gif") ?>" /> </span>
                                            <button type="submit" class="btn btn-default"><i class="fa fa-download"></i> Download Excel</button>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </form>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->



<?php echo $this->load->view("common/footer"); ?>
<script src="<?php echo base_url(); ?>assets/plugins/jValidate/formValidations.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url() ?>assets/plugins/daterangepicker/moment.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/daterangepicker/daterangepicker.js"></script>

<script>
// $(document).ready(function(){
//     alert("document")
//     $("#loaderImg").css("display", "none");
// });

    $(function () {

        $('#txtReportDateRange').daterangepicker({format: 'YYYY-MM-DD'});       

        // $("#frmUserReports").submit(function(){
        //     $("#loaderImg").css("display", "inline-block");
        // });

    });
</script>