<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dept_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /*
     * function name : getDeptData
     * 
     * Fetch Department details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : none
     * @return : Array $data
     */

    public function getDeptData() {

        $this->db->order_by('dept_name');
        $query = $this->db->get('dept_data');

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    /*
     * function name : getActiveDeptData
     * 
     * Fetch Active Department details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : none
     * @return : Array $data
     */

    public function getActiveDeptData() {

        $this->db->where("status", "1");
        $this->db->order_by('dept_name');
        $query = $this->db->get('dept_data');

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    /*
     * function name : getDeptDetails
     * 
     * Fetch particular Department details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : int $deptId
     * @return : Array $data
     */

    public function getDeptDetails($deptId) {

        $this->db->where("id", $deptId);
        $query = $this->db->get('dept_data');

        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    /*
     * function name : getDeptDesignations
     * 
     * Fetch particular Department designations details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : int $deptId
     * @return : Array $data
     */

    public function getDeptDesignations($deptId) {

        $this->db->where("dept_id", $deptId);
        $query = $this->db->get('designation_data');

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    /*
     * function name : addDepartment
     * 
     * Save Department details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : array $insertData
     * @return : bool true|false
     */

    public function addDepartment($insertData) {

        $this->db->insert('dept_data', $insertData);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * function name : editDepartment
     * 
     * update Department details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : array $updateData, int $deptId
     * @return : bool true|false
     */

    public function editDepartment($updateData, $deptId) {


        $this->db->update('dept_data', $updateData, array("id" => $deptId));

        return true;
    }
    
    
    /*
     * function name : deleteDepartment
     * 
     * Delete Department details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : int $deptId
     * @return : bool true|false
     */

    public function deleteDepartment($deptId) {

        $this->db->delete('dept_data', array("id" => $deptId));
        return true;
    }

}
