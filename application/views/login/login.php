<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Glitch | Log in</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/square/blue.css">

    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <img src="<?php echo base_url("assets/dist/img/logo.png"); ?>" />
            </div><!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg"><strong>Log In</strong> to your account</p>
                <div class="form-error"><?php echo $this->session->flashdata('login_msg'); ?></div>
                <form name="frmLogin" id="frmLogin" action="<?php echo base_url(); ?>login/loginProcess" method="post">
                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" placeholder="Email" name="identity" id="identity" value="<?php if(isset($_COOKIE['remember_name'])) echo $_COOKIE['remember_name'] ?>" >
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        <?php echo form_error('identity', '<div class="form-error">', '</div>'); ?>
                    </div>
                    
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Password" name="password" id="password" value="<?php if(isset($_COOKIE['remember_key'])) echo $_COOKIE['remember_key'] ?>" >
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        <?php echo form_error('password', '<div class="form-error">', '</div>'); ?>
                    </div>
                    
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="checkbox icheck">
                                <label>
                                    <input type="checkbox" name="glitch_remember" id="glitch_remember" value="1" <?php if(isset($_COOKIE['remember_me_token'])) echo "checked" ?> > Remember Me
                                </label>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                        </div><!-- /.col -->
                    </div>
                </form>

            </div><!-- /.login-box-body -->
        </div><!-- /.login-box -->

        <!-- jQuery 2.1.4 -->
        <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/jValidate/jquery.validate.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/jValidate/additional-methods.js"></script>

        <script src="<?php echo base_url(); ?>assets/plugins/jValidate/formValidations.js"></script>

        <!-- Bootstrap 3.3.5 -->
        <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- iCheck -->
        <script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js"></script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '10%' // optional
                });
            });
        </script>
    </body>
</html>