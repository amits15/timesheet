<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Code_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /*
     * function name : getCodeData
     * 
     * Fetch Code details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : none
     * @return : Array $data
     */

    public function getCodeData() {

        $this->db->select('cl.client_name, l.location_name, c.*');
        // $this->db->where('c.id >', '330');
        $this->db->from('code_data as c');
        $this->db->join('client_data as cl', 'c.client_id = cl.id', 'LEFT');
        $this->db->join('location_data as l', 'c.location = l.id', 'LEFT');
        $this->db->order_by('c.id');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }


    /*
     * function name : checkProductCode
     * 
     * Checks product code is exsist or not
     * is_unique[TABLENAME.COLUMNNAME]
     * @author  Amit Salunkhe
     * @access  public
     * @param : $productCode
     * @return : Array $data
     */

    public function checkProductCode($productCode) {

        $this->db->where("product_code", $productCode);
        $query = $this->db->get('code_data');
        if ($query->num_rows() == 0) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * function name : checkJobCode
     * 
     * Checks job code is exsist or not
     * 
     * @author  Amit Salunkhe
     * @access  public
     * @param : $jobCode, $codeId
     * @return : Array $data
     */

    public function checkJobCode($jobCode, $codeId) {

        
        $this->db->where("UPPER(job_code)", strtoupper($jobCode));
        if(!empty($codeId)){
            $this->db->where("id !=", $codeId);
        }
        $query = $this->db->get('code_data');
        // echo $this->db->last_query();exit;

        if ($query->num_rows() == 0) {
            return true;
        } else {
            return false;
        }
    }


    
    /*
     * function name : getCodeDetails
     * 
     * Fetch Particular Code details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : int $codeId
     * @return : Array $data
     */

    public function getCodeDetails($codeId) {

        $this->db->where("id", $codeId);
        $query = $this->db->get('code_data');

        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    
    /*
     * function name : addCode
     * 
     * Save Code details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : array $insertData
     * @return : bool true|false
     */

    public function addCode($insertData) {

        $this->db->insert('code_data', $insertData);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    
    /*
     * function name : editCode
     * 
     * update Code details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : array $updateData, int $codeId
     * @return : bool true|false
     */

    public function editCode($updateData, $codeId) {


        $this->db->update('code_data', $updateData, array("id" => $codeId));

        return true;
    }
    
    /*
     * function name : deleteCode
     * 
     * Delete Code details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : int $codeId
     * @return : bool true|false
     */

    public function deleteCode($codeId) {

        $this->db->delete('code_data', array("id" => $codeId));
        return true;
    }
    
}
