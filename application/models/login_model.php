<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /*
     * function name : loginProcess
     * 
     * Process the login request submitted by user.
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    public function loginProcess($data) {

        $this->db->where("email_id", $data['identity']);
        $this->db->where("password", md5($data['password']));
        $query = $this->db->get('user_data');

        if ($query->num_rows() > 0) {
            $row = $query->row_array();

            $deptQuery = $this->db->get_where('dept_data', array('id' => $row['department'] ));
            $deptRow = $deptQuery->row_array();

            $desigQuery = $this->db->get_where('designation_data', array('id' => $row['designation'] ));
            $desigRow = $desigQuery->row_array();

            $data = array(
                "userid" => $row['id'],
                "email_id" => $row['email_id'],
                "first_name" => $row['first_name'],
                "last_name" => $row['last_name'],
                "employee_type" => $row['employee_type'],
                "designation" => $desigRow['desig_name'],
                "department" => $deptRow['dept_name'],
                "profile_pic" => $row['profile_image']
            );
            $this->session->set_userdata($data);
            return true;
        } else {
            return false;
        }
    }

}
