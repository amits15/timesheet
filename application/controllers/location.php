<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Location extends CI_Controller {
    /*
     * function name :__construct
     *  Counstructor for Location controller 
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('datatables');
        $this->load->library('form_validation');
        $this->load->model('location_model');
    }

    /*
     * function name :index
     *  Shows the Location listing page.
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    public function index() {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Location"
            );

            $data['location_results'] = $this->location_model->getLocationData();

            $this->load->view('location/locationList', $data);
        } else {
            redirect("login");
        }
    }

    public function datatable() {

        $column = '<a class="" href="'. base_url("location/edit/$1") .'"><i class="fa fa-edit fa-lg"></i></a>&nbsp;&nbsp;
         <a data-href="'. base_url("location/delete/$1") .'" data-toggle="modal" data-target="#deleteModal" href="#"><i class="fa fa-trash fa-lg"></i></a>';

        $this->datatables->select("id, location_name, DATE_FORMAT(created, '%D %b, %Y %H:%i %p') as date, case status  when '1' then 'Active'  when '0' then 'Inactive' end as status", FALSE)
                ->from('location_data')
                ->edit_column('id', '<input type="checkbox"  name="check_loc[]" value="$1"  class="multi_check" />', 'id')
                ->add_column('edit', "$column", 'id');

                echo $this->datatables->generate();
    }

    /*
     * function name :add
     *  Add Location Details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : 
     * @return : void
     */

    public function add() {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Add Location"
            );

            if ($_POST) {

                $this->form_validation->set_rules('txtLocationName', 'Location Name', 'required');
                $this->form_validation->set_rules('optLocationStatus', 'Status', 'required');

                if ($this->form_validation->run() == true) {

                    $insertData = array(
                        "location_name" => $this->input->post("txtLocationName"),
                        "status" => $this->input->post("optLocationStatus"),
                        "created" => date("Y-m-d H:i:s")
                    );
                    $result = $this->location_model->addLocation($insertData);

                    if ($result) {
                        redirect("location");
                    } else {
//                        $this->session->set_flashdata("login_msg", "Invalid Username/Password!!!");
                        redirect("location/add");
                    }
                }
            }

            $this->load->view('location/addLocation', $data);
        } else {
            redirect("login");
        }
    }

    /*
     * function name : edit
     *  Update client details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : Int $clientId
     * @return : void
     */

    public function edit($locationId = NULL) {

        if ($this->session->userdata('userid')) {

            //creating header title for header view page;
            $data = array(
                "title" => "Edit Location"
            );

            if ($_POST) {

                $this->form_validation->set_rules('txtLocationName', 'Client Name', 'required');
                $this->form_validation->set_rules('optLocationStatus', 'Status', 'required');

                if ($this->form_validation->run() == true) {


                    $locationId = $this->input->post("txtLocationId");
//                    echo "$clientId";exit;

                    $updateData = array(
                        "location_name" => $this->input->post("txtLocationName"),
                        "status" => $this->input->post("optLocationStatus")
                    );
                    $result = $this->location_model->editLocation($updateData, $locationId);

                    if ($result) {
                        redirect("location");
                    } else {
                        redirect("location/edit/$locationId");
                    }
                }
            }

            $data['location'] = $this->location_model->getLocationDetails($locationId);
//            print_r($data);exit;

            $this->load->view('location/editLocation', $data);
        } else {
            redirect("login");
        }
    }

    /*
     * function name : delete
     *  Delete Location details
     * 
     * @author	Amit Salunkhe
     * @access	public
     * @param : Int $locationId
     * @return : void
     */

    public function delete($locationId = NULL) {

        if ($this->session->userdata('userid')) {

            if ($locationId) {

                $this->location_model->deleteLocation($locationId);
            } else if ($_POST) {

                $locationIds = $this->input->post("check_loc");

                foreach ($locationIds as $key => $value) {
                    $this->location_model->deleteLocation($value);
                }
            }
            redirect("location");
        } else {
            redirect("login");
        }
    }

}

/* End of file location.php */
/* Location: ./application/controllers/location.php */