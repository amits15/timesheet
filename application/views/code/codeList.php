<?php echo $this->load->view("common/header", $title); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/datatables/dataTables.bootstrap.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Code Master
        </h1>
        <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Master</li>
            <li><a href="#"></i> Codes</a></li>            
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <form action="<?php echo base_url("code/delete") ?>" method="POST" id="frmCodeList" >
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Code List</h3>
                        </div><!-- /.box-header -->
                        &nbsp;&nbsp;&nbsp;<a class="btn btn-primary" href="<?php echo base_url("code/add"); ?>"><i class="fa fa-plus"></i> Add</a>
                        &nbsp;&nbsp;&nbsp;<a class="btn btn-danger" data-toggle="modal" id="multiDelete" href="javascript:void(0)"><i class="fa fa-trash"></i> Delete</a>
                        <a class="btn btn-success pull-right" data-toggle="modal" style="margin-right: 10px;" href="<?php echo base_url("code/exportCodes") ?>"><i class="fa fa-file-excel-o"></i> Export to Excel</a>
                        <div class="box-body">
                            <table id="code_tbl" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th style="width: 5%" ><input type="checkbox" class="grp_check" name="check_all" /></th>
                                        <!--<th >Sr No.</th>-->
                                        <th>Client Name</th>
                                        <th>Location</th>
                                        <th>Type</th>
                                        <th>Sub-Type</th>
                                        <th>Suffix</th>
                                        <th>Product Code</th>
                                        <!-- <th>End Date</th> -->
                                        <th>Added Date</th>
                                        <th>Status</th>
                                        <th style="width: 10%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    /*if ($code_results) {
                                        foreach ($code_results as $code) {
                                            ?>
                                            <tr>
                                                <td><input type="checkbox" name="check_codes[]" class="multi_check" value="<?php echo $code['id'] ?>" /></td>
                                                <!--<td><?php echo $code['rowNumber'] ?></td>-->
                                                <td><?php echo $code['client_name'] ?></td>
                                                <td><?php echo $code['location_name'] ?></td>
                                                <td><?php echo $code['type'] ?></td>
                                                <td><?php echo $code['subtype'] ?></td>
                                                <td><?php echo $code['suffix'] ?></td>
                                                <td>
                                                    <?php
                                                    if ($code['start_date'] != NULL && $code['start_date'] != "0000-00-00") {
                                                        echo date("d M, Y", strtotime($code['start_date']));
                                                    } else {
                                                        echo "-";
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    
                                                    if ($code['end_date'] != NULL && $code['end_date'] != "0000-00-00") {
                                                        echo date("d M, Y", strtotime($code['end_date']));
                                                    } else {
                                                        echo "-";
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($code['created'] != NULL && $code['created'] != "0000-00-00") {
                                                        echo date("d M, Y", strtotime($code['created']));
                                                    } else {
                                                        echo "-";
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($code['status'] == 1) {
                                                        echo "Active";
                                                    } else {
                                                        echo "Inactive";
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <a class="" href="<?php echo base_url("code/edit/$code[id]"); ?>"><i class="fa fa-edit fa-lg"></i></a>&nbsp;&nbsp;
                                                    <a href="#" data-href="<?php echo base_url("code/delete/$code[id]") ?>" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash fa-lg"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }*/
                                    ?>
                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->

                    <!-- Modal -->
                    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Delete Code</h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure you want to delete Code?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                    <a class="btn btn-danger">Yes</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="multiDeleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Delete Code</h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure you want to delete selected Code(s)?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                    <button type="submit" class="btn btn-danger">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php echo $this->load->view("common/footer"); ?>
<!-- DataTables -->
<script src="<?php echo base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>
    $(function () {
        $('#code_tbl').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "aoColumns": [
                { "bSortable": false, "searchable": false },
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                { "bSortable": false, "searchable": false },
                ],
            "serverSide": true,
            "processing": true,
            "language": {
                "processing": "Loading data..." //add a loading image,simply putting <img src="loader.gif" /> tag.
            },
            "ajax": {
                url: '<?php echo base_url(); ?>code/datatable',
                type: 'POST'
            }
        });

        
    });
</script>