<?php echo $this->load->view("common/header", $title); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Location
        </h1>
        <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Master</li>
            <li><a href="#"></i> Location</a></li>          
            <li><a href="#"></i> Edit</a></li>          
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                        <a href="javascript:history.back()" class="btn btn-link pull-right"><i class="fa fa-arrow-left" ></i> Back</a>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="<?php echo base_url("location/edit"); ?>" method="POST" id="frmLocation">
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Location Name</label>
                                <div class="col-sm-4">
                                    <input type="hidden" id="txtLocationId" name="txtLocationId" class="form-control" value="<?php echo $location['id'] ?>" >
                                    <input type="text" placeholder="Location Name" id="txtLocationName" name="txtLocationName" class="form-control" value="<?php echo $location['location_name'] ?>" >
                                    <?php echo form_error('txtLocationName', '<div class="form-error">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="inputPassword3">Status</label>
                                <div class="col-sm-3">
                                    <select class="form-control col-sm-2" name="optLocationStatus" id="optLocationStatus">
                                        <option value="1" <?php if($location['status'] == 1) echo "Selected"; ?> >Active</option>
                                        <option value="0" <?php if($location['status'] == 0) echo "Selected"; ?> >Inactive</option>
                                    </select>
                                    <?php echo form_error('optLocationStatus', '<div class="form-error">', '</div>'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div><!-- /.box -->

            </div><!--/.col (left) -->

        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php echo $this->load->view("common/footer"); ?>
<script src="<?php echo base_url(); ?>assets/plugins/jValidate/formValidations.js"></script>